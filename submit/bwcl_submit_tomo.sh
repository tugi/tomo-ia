#!/bin/bash
#MSUB -l nodes=1:ppn=16
#MSUB -l walltime=12:00:00
#MSUB -l mem=40000mb
#MSUB -v EXECUTABLE=./tomo_ia.exe

#MSUB -N tomo_ia

NBIN=$1
mode=$2
morph=$3
NL=$4
survey=$5

export OMP_NUM_THREADS=${MOAB_PROCCOUNT}
echo "Executable ${EXECUTABLE} running on ${MOAB_PROCCOUNT} cores with ${OMP_NUM_THREADS} threads"
startexe="${EXECUTABLE} ${mode} ${morph} ${NL} ${survey}"
echo $startexe
exec $startexe

