#!/bin/bash

# 0 mode_lensing  s2n for lensing
# 1 mode_ebmode e and b mode spectra
# 2 mode_ellipticity correlation functions
# 3 mode_tomography ccoefficients and kappa_i
# 4 mode_fisher fisher matrices
# 5 mode_bias bias vectors
# 6 mode_gravslip gravslip fisher
# 8 mode_derivs d log C / d theta_u
# 14 mode_victor: takes external Pk

#Morph 10: Ellipse
#Morph 20: Spiral
#NL 0: Linear Pk
#NL 1: Non-Linear Pk

NBIN=$1
survey=$2

# Ellipticals
echo "====================="
for mode in 1 3 4 5 8; do
    for morph in 10; do
	for NL in 1; do
	            echo "running MODE=$mode MORPH=$morph NL=$NL..."
		    msub -q singlenode -v EXECUTABLE=./tomo_ia_$NBIN-bin.exe -N tIA$NBIN-$morph$mode ./submit/bwcl_submit_tomo.sh $NBIN $mode $morph $NL $survey
		    echo "====================="
		    done
    done
done

# Spirals
echo "====================="
for mode in 1 3 4 5 8; do
   for morph in 20; do
	for NL in 0; do
	            echo "running MODE=$mode MORPH=$morph NL=$NL..."
		    msub -q singlenode -v EXECUTABLE=./tomo_ia_$NBIN-bin.exe -N tIA$NBIN-$morph$mode bwcl_submit_tomo.sh $NBIN $mode $morph $NL $survey
		    echo "====================="
		    done
   done
done

# Both
echo "====================="
for mode in 5 8; do
    for morph in 30; do
	for NL in 1; do
	            echo "running MODE=$mode MORPH=$morph NL=$NL..."
		    msub -q singlenode -v EXECUTABLE=./tomo_ia_$NBIN-bin.exe -N tIA$NBIN-$morph$mode bwcl_submit_tomo.sh $NBIN $mode $morph $NL $survey
		    echo "====================="
		    done
    done
done
