#!/bin/bash
cd ..

survey=100 # euclid

echo "====================="
echo "Cleaning up logs..."
rm *.log
rm *.out
rm ./tomo_ia_?-bin.exe
echo "Folder housekeeping ..."
today=$(date +"%d-%m-%y_%Hh%M")
mkdir -p ./data_archive
mv output ./data_archive/output_$today
echo "Moved to folder output_$today ..."
mkdir -p output
echo "Making clean ..."
make clean
echo "====================="

for iBIN in $@; do
   echo "compiling with NBIN=$iBIN..."
   make NBIN=$iBIN alpha=0.0 hooke=9.5e5 COMPILERMODE=bwcluster > /dev/null
   mv tomo_ia tomo_ia_$iBIN-bin.exe
   ./submit/bwcl_meta_submit_tomo.sh $iBIN $survey > ./tomo-submit-$iBIN-bin.log
   echo "$iBIN submitted!" 
done
