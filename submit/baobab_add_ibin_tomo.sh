#!/bin/bash

echo "====================="
#echo "Cleaning up logs..."
#rm *.err
#rm *.out
#echo "Folder housekeeping ..."
#today=$(date +"%d-%m-%y_%Hh%M")
#mv output ./data_archive/output_$today
#echo "Moved to folder output_$today ..."
mkdir -p output

for iBIN in $@; do
   echo "compiling with NBIN=$iBIN..."
   make NBIN=$iBIN > /dev/null
   mv tomo_ia tomo_ia_$iBIN-bin
   ./baobab_meta_submit_tomo.sh $iBIN 100 > /dev/null
   echo "$iBIN submitted!" 
done
