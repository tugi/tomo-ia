#!/bin/bash

survey=100 # Euclid

echo "====================="
echo "Cleaning up logs..."
rm *.err
rm *.out
echo "Folder housekeeping ..."
today=$(date +"%d-%m-%y_%Hh%M")
mkdir -p ./data_archive
mv output ./data_archive/output_$today
echo "Moved to folder output_$today ..."
mkdir -p output

for iBIN in $@; do
   echo "compiling with NBIN=$iBIN..."
   make NBIN=$iBIN COMPILERMODE=baobab > /dev/null
   mv tomo_ia tomo_ia_$iBIN-bin
   ./baobab_meta_submit_tomo.sh $iBIN $survey > ./tomo-submit-$iBIN-bin.log
   echo "$iBIN submitted!"
done
