#!/bin/bash

# 0 mode_lensing  s2n ?
# 1 mode_ebmode e and b mode spectra
# 2 mode_ellipticity correlation functions
# 3 mode_tomography ccoefficients and kappa_i
# 4 mode_fisher fisher matrices
# 5 mode_bias bias vectors
# 6 mode_gravslip gravslip fisher
# 7 mode_gravslip_bias gravslip biases
# 8 mode_derivs d log C / d theta_u

#Morph 10: Ellipse
#Morph 20: Spiral
#NL 0: Linear Pk
#NL 1: Non-Linear Pk

NBIN=$1
survey=$2

# ONLY S2N
# echo "====================="
# for mode in 1; do
#     for morph in 10; do
# 	for NL in 1; do
# 	        echo "running MODE=$mode MORPH=$morph NL=$NL..."
# 		sbatch submit_tomo.sh $NBIN $mode $morph $NL $survey
# 		echo "====================="
# 	done
#     done
# done

# FULL RUN
# Ellipticals
echo "====================="
for mode in 1 3 4 5 8; do
    for morph in 10; do
	for NL in 1; do
	        echo "running MODE=$mode MORPH=$morph NL=$NL..."
		sbatch baobab_submit_tomo.sh $NBIN $mode $morph $NL $survey
		echo "====================="
	done
    done
done

# Spirals
echo "====================="
for mode in 1 3 4 5 8; do
    for morph in 20; do
	for NL in 0; do
	        echo "running MODE=$mode MORPH=$morph NL=$NL..."
		sbatch baobab_submit_tomo.sh $NBIN $mode $morph $NL $survey
		echo "====================="
	done
    done
done

# Both biases
echo "====================="
for mode in 5 8; do
    for morph in 30; do
	for NL in 0; do
	        echo "running MODE=$mode MORPH=$morph NL=$NL..."
		sbatch baobab_submit_tomo.sh $NBIN $mode $morph $NL $survey
		echo "====================="
	done
    done
done

