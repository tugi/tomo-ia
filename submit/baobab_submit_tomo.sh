#!/bin/bash

#SBATCH -J tomo_ia
#SBATCH -e LOG_tomo_ia_%j.err
#SBATCH -o LOG_tomo_ia_%j.out
#SBATCH -p shared
#SBATCH -t 12:00:00
#SBATCH --exclusive
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=4000
#SBATCH --extra-node-info=2:8:1
#SBATCH --mail-type=FAIL

NBIN=$1
mode=$2
morph=$3
NL=$4
survey=$5



srun ./tomo_ia_$NBIN-bin $mode $morph $NL $survey