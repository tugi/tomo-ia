# gcc-mp-4.8 -o tomo tomo.c -L. -L/sw/lib -I/sw/include -lgsl -lgslcblas -lcuba -lm -fopenmp
# gcc -o tomo_ia tomo_ia.c -L./libtomo/lib -I./libtomo/inlcude -I./libcuba/include -L./libcuba/lib -lcuba -lm -fopenmp

NBIN=2
alpha=1.97563102367963
hooke=9.5e5

COMPILERMODE=bwcluster

# TOMO DEFINTIONS
LIBT=libtomo.a

TOMODIR=./libtomo
TOMODIRL=$(TOMODIR)/lib
TOMODIRI=$(TOMODIR)/include
TOMODIRS=$(TOMODIR)/src

TOMOC=alignment.c bias.c correlation.c cosmology.c lensing.c output.c input.c
TOMOH=$(TOMOC:.c=.h)
TOMOO=$(TOMOC:.c=.o)

# GCC STUFF
CC=gcc
AR=ar
ARFLAGS=-cvq
CFLAGS= -fopenmp -ltomo -lgsl -lgslcblas -lm
CINCLUDES= -L$(TOMODIRL) -I$(TOMODIRI)
CINCLUDESLIB= -L../../$(TOMODIRL) -I../../$(TOMODIRI)

# DIFFERENT ENVIRONMENTS
ifeq ($(COMPILERMODE),baobab)
CINCLUDES+= -I/opt/gsl/gsl-1.16/include -L/opt/gsl/gsl-1.16/lib
CINCLUDESLIB+= -I/opt/gsl/gsl-1.16/include -L/opt/gsl/gsl-1.16/lib
endif

ifeq ($(COMPILERMODE),cosmofloor)
CINCLUDES+= -I/home/ilion/0/ttugendhat/gsl/include -L/home/ilion/0/ttugendhat/gsl/lib
CINCLUDESLIB+= -I/home/ilion/0/ttugendhat/gsl/include -L/home/ilion/0/ttugendhat/gsl/lib
endif

ifeq ($(COMPILERMODE),bwcluster)
CINCLUDES+= -L/opt/bwhpc/common/numlib/gsl/2.2.1-gnu-4.8/lib -I/opt/bwhpc/common/numlib/gsl/2.2.1-gnu-4.8/include
CINCLUDESLIB+= -L/opt/bwhpc/common/numlib/gsl/2.2.1-gnu-4.8/lib -I/opt/bwhpc/common/numlib/gsl/2.2.1-gnu-4.8/include
endif

ifeq ($(COMPILERMODE),bjoern)
CC=gcc-mp-4.8
CINLCUDES+= -L. -L/sw/lib -I/sw/include
CINLCUDESLIB+= -L. -L/sw/lib -I/sw/include
endif

ifeq ($(COMPILERMODE),tim)
CC=gcc-6
endif

#Include NBIN:
CFLAGS+=-DexternalNBIN=${NBIN}
#Include Alpha:
CFLAGS+=-DexternalAlpha=${alpha}
#Include Hooke:
CFLAGS+=-DexternalHooke=${hooke}

all: clean libtomo.a tomo_ia

default: clean libtomo.a tomo_ia

tomo_ia: tomo_ia.c
	$(CC) -o $@ $@.c $(CINCLUDES) $(CFLAGS) 

libtomo.a: $(TOMODIR)/include/tomo.h 
	cd $(TOMODIRS);$(CC) $(CINCLUDESLIB) $(CFLAGS) -c $(TOMOC)
	cd $(TOMODIRS);$(AR) $(ARFLAGS) $(LIBT) $(TOMOO)
	mv $(TOMODIRS)/libtomo.a $(TOMODIRL)

clean:
	rm -f $(TOMODIRS)/*.o
	rm -f tomo_ia
	rm -f $(TOMODIRL)/libtomo.a

