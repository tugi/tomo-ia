#include <tomo.h>

struct TUGI_CONFIG* read_config(char *filename) 
{
        struct TUGI_CONFIG *configstruct;
        configstruct = (struct TUGI_CONFIG *)calloc(1,sizeof(struct TUGI_CONFIG));
        FILE *file = fopen (filename, "r");
        if (file != NULL)
        { 
                char line[MAXBUF];
                int i = 0;
                printf("!!!\n"); 
                while(fgets(line, sizeof(line), file) != NULL)
                {
                        char *cfline;
                        cfline = strstr((char *)line,TUGI_DELIM);
                        cfline = cfline + strlen(TUGI_DELIM);
    
                        switch(i)
                        {
                        case 0:
                                memcpy(configstruct->mode_c,cfline,strlen(cfline));
                                break;
                        case 1:
                                memcpy(configstruct->morphology_c,cfline,strlen(cfline));
                                break;
                        case 2:
                                memcpy(configstruct->surveytype_c,cfline,strlen(cfline));
                                break;
                        case 3:
                                memcpy(configstruct->nonlinear_c,cfline,strlen(cfline));
                                break;
                        case 4:
                                memcpy(configstruct->baryon_transfer_c,cfline,strlen(cfline));
                                break;
                        }
                        
                        i++;
                } // End while
        } // End if file
                printf("!!!\n"); 
        fclose(file);

// Start cleaning up
        printf("ayay = %s",configstruct->mode_c);

        if(strcmp(configstruct->mode_c,"fisher")==1)
        {
            printf("ayay\n");
            configstruct->mode=mode_fisher;
        }

        
        return configstruct;

}


void CONFIG_TOMO(char *argv[])
{

    tconf->flag_gravslip = off;
    tconf->flag_separation = off;

    switch(atoi(argv[1]))
    {
    case 0:
        tconf->mode=mode_lensing;
        sprintf(tconf->mode_string, "lensing");
    break;

    case 1:
        tconf->mode=mode_ebmode;
        sprintf(tconf->mode_string, "ebmode");
    break;  

    case 2:
        tconf->mode=mode_ellipticity;
        sprintf(tconf->mode_string, "ellipticity");
    break;  

    case 3:
        tconf->mode=mode_tomography;
        sprintf(tconf->mode_string, "tomography");
    break;  
    
    case 4:
        tconf->mode=mode_fisher;
        sprintf(tconf->mode_string, "fisher");
    break;  

    case 5:
        tconf->mode=mode_bias;
        sprintf(tconf->mode_string, "bias");
    break;  

    case 6:
        tconf->mode=mode_gravslip;
        tconf->flag_gravslip=on; 
        tconf->morphology=ellipse;
        tconf->flag_GIs=on;
        sprintf(tconf->mode_string, "gravslip");
    break;

    case 7:
        tconf->mode=mode_hooke;
        tconf->morphology=ellipse;
        tconf->flag_GIs=on;
        sprintf(tconf->mode_string, "hooke/s2n");
    break;

    case 8:
        tconf->mode=mode_derivs;
        sprintf(tconf->mode_string, "log Derivs of C");
    break;

    case 9:
        tconf->mode=mode_moments;
        tconf->morphology=both;
        sprintf(tconf->mode_string, "spectral moments for peak counts");
    break;

    case 10:
        tconf->mode=mode_sep_s2n;
        tconf->flag_separation=on;
        sprintf(tconf->mode_string, "s2n for separation paper");
    break;

    case 11:
        tconf->mode=mode_sep_biases;
        tconf->flag_separation=on;
        sprintf(tconf->mode_string, "bias for separation paper");
    break;

    case 12:
        tconf->mode=mode_sep_dchisq;
        tconf->flag_separation=on;
        sprintf(tconf->mode_string, "chi2 for separation paper");
    break;
    
    case 13:
        tconf->mode=mode_sep_aDfisher;
        tconf->flag_separation=on;
        sprintf(tconf->mode_string, "aD Fisher for Seperation paper");
    break;
    
    case 14:
        tconf->mode=mode_victor;
        tconf->flag_extPk=on;
        sprintf(tconf->mode_string, "Victor External Pk");
    break;
    }

    switch(atoi(argv[2]))
    {
    case 20:
        tconf->morphology=spiral;
        tconf->flag_GIs=off;
        sprintf(tconf->morph_string, "spiral");
    break;

    case 10:
        tconf->morphology=ellipse;
        tconf->flag_GIs=on;
        sprintf(tconf->morph_string, "ellipse");
    break;  

    case 30:
        tconf->morphology=both;
        tconf->flag_GIs=on;
        sprintf(tconf->morph_string, "both");
    break;  

    }

    switch(atoi(argv[3]))
    {
    case 1:
        tconf->nonlinearPk=on;
        sprintf(tconf->Pktype_string, "Using non-linear power spectrum.");
    break;
    case 0:
        tconf->nonlinearPk=off;
        sprintf(tconf->Pktype_string, "Using linear power spectrum");
    break;  
    }

    switch(atoi(argv[4]))
    {
    case euclid:
        tconf->surveytype=euclid;
        sprintf(tconf->survey_string, "euclid");
    break;
    case cfhtlens:
        tconf->surveytype=cfhtlens;
        sprintf(tconf->survey_string, "cfhtlens");
    break;  
    case sunglass:
        tconf->surveytype=sunglass;
        sprintf(tconf->survey_string, "sunglass");
    break;  
    case robert_test:
        tconf->surveytype=robert_test;
        sprintf(tconf->survey_string, "robert_test");
    break;  
    }

}

const char* readfield(char* line, int num)
{
//    printf(" num = %i ...\n", num);

    const char* tok;
    for (tok = strtok(line, ",");
            tok && *tok;
            tok = strtok(NULL, ",\n"))
    {
        if (!--num)
            return tok;
    }
    return NULL;
}
void READIN_EXTPK(struct DATA *data)
{
    int i,j;
    int l = 0;
    double k[N_k];
    double P[2][3][N_k];
    double eta[2][3][N_k];
    double mu[2][3][N_k];

    FILE* stream_PkaB = fopen("ext_data/matter_power_spectrum_change_aB.txt", "r");
    FILE* stream_PkaM = fopen("ext_data/matter_power_spectrum_change_aM.txt", "r");

    char line[1024];
    printf("tomo_ia: input: reading in P(k) from external file(s) ... ");

    fgets(line, 1024, stream_PkaB); // discard first line
    while (fgets(line, 1024, stream_PkaB))
    {
        char* tmp1 = strdup(line);
        // printf(" l = %i ...\n", l);
        k[l] = atof(readfield(tmp1, 1));
        // printf(" k = %f ...\n", k[l]);
        free(tmp1);
        
        tmp1 = strdup(line);
        P[0][1][l] = atof(readfield(tmp1, 2));
        free(tmp1);
        tmp1 = strdup(line);
        P[0][2][l] = atof(readfield(tmp1, 3));
        free(tmp1);
        tmp1 = strdup(line);
        P[0][0][l] = atof(readfield(tmp1, 4));
        free(tmp1);
        
        tmp1 = strdup(line);
        eta[0][1][l] = atof(readfield(tmp1, 5));
        free(tmp1);
        tmp1 = strdup(line);
        eta[0][2][l] = atof(readfield(tmp1, 6));
        free(tmp1);
        tmp1 = strdup(line);
        eta[0][0][l] = atof(readfield(tmp1, 7));
        free(tmp1);

        tmp1 = strdup(line);
        mu[0][1][l] = atof(readfield(tmp1, 8));
        free(tmp1);
        tmp1 = strdup(line);
        mu[0][2][l] = atof(readfield(tmp1, 9));
        free(tmp1);
        tmp1 = strdup(line);
        mu[0][0][l] = atof(readfield(tmp1, 10));
        free(tmp1);
        l++;
    }
    fgets(line, 1024, stream_PkaM); // discrd first line
    l=0;
    while (fgets(line, 1024, stream_PkaM))
    {
        char* tmp2 = strdup(line);
        P[1][1][l] = atof(readfield(tmp2, 2));
        free(tmp2);
        tmp2 = strdup(line);
        P[1][2][l] = atof(readfield(tmp2, 3));
        free(tmp2);
        tmp2 = strdup(line);
        P[1][0][l] = atof(readfield(tmp2, 4));
        free(tmp2);
        
        tmp2 = strdup(line);
        eta[1][1][l] = atof(readfield(tmp2, 5));
        free(tmp2);
        tmp2 = strdup(line);
        eta[1][2][l] = atof(readfield(tmp2, 6));
        free(tmp2);
        tmp2 = strdup(line);
        eta[1][0][l] = atof(readfield(tmp2, 7));
        free(tmp2);
        
        tmp2 = strdup(line);
        mu[1][1][l] = atof(readfield(tmp2, 8));
        free(tmp2);
        tmp2 = strdup(line);
        mu[1][2][l] = atof(readfield(tmp2, 9));
        free(tmp2);
        tmp2 = strdup(line);
        mu[1][0][l] = atof(readfield(tmp2, 10));
        free(tmp2);

        l++;
    }

//    printf("kmin = %f ..., \n", k[0]); 
//    printf("kmax = %f ..., \n", k[N-1]);	

    for(i=0;i<2;i++)
	{
		for(j=0;j<3;j++)
		{
        
        gcosmo->acc_extPk[i][j] = gsl_interp_accel_alloc();
        gcosmo->spline_extPk[i][j] = gsl_spline_alloc(gsl_interp_steffen, N_k);
        gsl_spline_init(gcosmo->spline_extPk[i][j], k, P[i][j], N_k);
        
        gcosmo->acc_exteta[i][j] = gsl_interp_accel_alloc();
        gcosmo->spline_exteta[i][j] = gsl_spline_alloc(gsl_interp_steffen, N_k);
        gsl_spline_init(gcosmo->spline_exteta[i][j], k, eta[i][j], N_k);
        
        gcosmo->acc_extmu[i][j] = gsl_interp_accel_alloc();
        gcosmo->spline_extmu[i][j] = gsl_spline_alloc(gsl_interp_steffen, N_k);
        gsl_spline_init(gcosmo->spline_extmu[i][j], k, mu[i][j], N_k);
        
        }	
	}
    printf("done!\n");

}
void READIN_EXTDPLUS(struct DATA *data)
{
    int i,j;
    int l,m = 0;

    double *za = malloc(N_kdp * N_a * sizeof(double));
    
    double k[N_kdp];
    double dplus[2][3][N_kdp][N_a];
    

    double a[N_a];
    a[0]=0.1;
    a[1]=0.3;
    a[2]=0.6;
    a[3]=0.7;
    a[4]=0.8;
    a[5]=0.9;
    a[6]=1.0;

    READIN_FILE_EXTDPLUS(k, dplus, a[0], 0);   
    READIN_FILE_EXTDPLUS(k, dplus, a[1], 1);  
    READIN_FILE_EXTDPLUS(k, dplus, a[2], 2);   
    READIN_FILE_EXTDPLUS(k, dplus, a[3], 3);   
    READIN_FILE_EXTDPLUS(k, dplus, a[4], 4);
    READIN_FILE_EXTDPLUS(k, dplus, a[5], 5);   
    READIN_FILE_EXTDPLUS(k, dplus, a[6], 6);   

    
    for(i=0;i<2;i++)
    {
        for(j=0;j<3;j++)
        {

            gcosmo->acc_extdplus_k[i][j] = gsl_interp_accel_alloc();
            gcosmo->acc_extdplus_a[i][j] = gsl_interp_accel_alloc();
            gcosmo->spline_extdplus[i][j] = gsl_spline2d_alloc(gsl_interp2d_bicubic, N_kdp, N_a);
            for(l=0;l<N_kdp;l++)
            {
                for (m=0;m<N_a;m++)
                {
                gsl_spline2d_set(gcosmo->spline_extdplus[i][j], za, k[l], a[m], dplus[i][j][l][m]);
                }
            }
            gsl_spline2d_init(gcosmo->spline_extdplus[i][j], k, a, za, N_kdp, N_a);
        }   
    }
}
void READIN_FILE_EXTDPLUS(double karr[], double array[][3][N_kdp][N_a], double anum, int m)
{
    int i,j;
    int l = 0;
//    double dplus[2][3][Nk][Na]; // = *reinterpret_cast<float (*)[2][3][Nk][Na]>(array);
 //   double k[Nk]; // = *reinterpret_cast<float (*)[Nk]>(karr);
    
    char fnameaB[256];
    char fnameaM[256];
    sprintf(fnameaB,"ext_data/D+(k,%.1f)_aB.txt",anum);
    sprintf(fnameaM,"ext_data/D+(k,%.1f)_aM.txt",anum);
    FILE* stream_DpaB = fopen(fnameaB, "r");
    FILE* stream_DpaM = fopen(fnameaM, "r");
    
    char line[1024];
    printf("tomo_ia: input: reading in D+(k,a) from external file %i out of %i ... ", m+1, N_a);
    fgets(line, 1024, stream_DpaB); // discard first line
    
    while (fgets(line, 1024, stream_DpaB))
    {
        char* tmp1 = strdup(line);
        karr[l] = atof(readfield(tmp1, 1));
        free(tmp1);

        tmp1 = strdup(line);
        array[0][1][l][m] = atof(readfield(tmp1, 2));
        free(tmp1);
        
        tmp1 = strdup(line);
        array[0][2][l][m] = atof(readfield(tmp1, 3));
        free(tmp1);
        
        tmp1 = strdup(line);
        array[0][0][l][m] = atof(readfield(tmp1, 4));
        free(tmp1);
        
        l++;
    }
    l=0;
    while (fgets(line, 1024, stream_DpaM))
    {
        char* tmp1 = strdup(line);
        karr[l] = atof(readfield(tmp1, 1));
        free(tmp1);

        tmp1 = strdup(line);
        array[1][1][l][m] = atof(readfield(tmp1, 2));
        free(tmp1);
        
        tmp1 = strdup(line);
        array[1][2][l][m] = atof(readfield(tmp1, 3));
        free(tmp1);
        
        tmp1 = strdup(line);
        array[1][0][l][m] = atof(readfield(tmp1, 4));
        free(tmp1);
        
        l++;
    }

    printf("done!\n");


}