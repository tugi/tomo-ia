#include <tomo.h>

// A nice header
void PRINT_TOMO_HEADER()
{

	printf("============================================================\n"); 
	printf("tomo_ia:\n------------------------------------------------------------\n\t*weak lensing spectra* \n\tw/ *tomography*\n\t*ellipticity spectra* from \n\t*quadradic & linear alignment models*\n");
	printf("by Bjoern Malte Schaefer & Tim M. Tugendhat, Heidelberg\n");
	printf("============================================================\n");
	// printf("NFISHER= %i\n", NFISHER);
	// printf("NFISHER START= %i\n", NFISHER_START);
	// printf("NFISHER END= %i\n", NFISHER_END);
	
	int nthreads, tid;
	#pragma omp parallel private(nthreads, tid)
 	{
		tid = omp_get_thread_num();
	 	if (tid == 0) 
 		{
			nthreads = omp_get_num_threads();
 			printf("Number of threads = %d\n", nthreads);
		}

	}


	switch(tconf->mode)
	{
	case mode_lensing:
		printf("tomo_ia: mode: *LENSING*\n");
		break;
	case mode_ebmode:
		printf("tomo_ia: mode: *$E$- and $B$-modes*\n");
		break;
	case mode_fisher:
		printf("tomo_ia: mode: *FISHER*\n");
		break;
	case mode_bias:
		printf("tomo_ia: mode: *BIAS*\n");
		break;
	case mode_tomography:
		printf("tomo_ia: mode: *TOMOGRAPHY*\n");
		break;
	case mode_ellipticity:
		printf("tomo_ia: mode: *ELLIPTICITY*\n");
		break;
	case mode_gravslip:
		printf("tomo_ia: mode: *GRAVITATIONAL SLIP FISHER*\n");
		break;	
	case mode_hooke:
		printf("tomo_ia: mode: *S2N/HOOKE*\n");
		break;	
	case mode_derivs:
		printf("tomo_ia: mode: *SPECTRUM DERIVATIVES*\n");
		break;
	case mode_sep_s2n:
		printf("tomo_ia: mode: *SEPARATION S2N*\n");
		break;
	case mode_sep_biases:
		printf("tomo_ia: mode: *SEP BIAS*\n");
		break;
	case mode_sep_dchisq:
		printf("tomo_ia: mode: *SEP DCHI2*\n");
		break;
	case mode_sep_aDfisher:
		printf("tomo_ia: mode: *Fisher-Matrix A D (Separation)*\n");
		break;
	case mode_victor:
		printf("tomo_ia: mode: *Victor Stuff*\n");
		break;
	}
	
	switch(tconf->morphology)
	{
	case ellipse:
		printf("tomo_ia: galaxy morphology: *ellipse*\n");
		break;
	case spiral:
		printf("tomo_ia: galaxy morphology: *spiral*\n");
		break;
	case both:
		printf("tomo_ia: galaxy morphology: *both*\n");
		break;
	}
	
	switch(tconf->nonlinearPk)
	{
	case on:
		printf("tomo_ia: P(k): non-linear\n");
		break;
	case off:
		printf("tomo_ia: P(k): linear\n");
		break;
	}
		printf("============================================================\n");
	
}
int PREPARE_OUTPUT(struct DATA *data)
{
	int mypid = getppid();
    int result;
	
	sprintf(tconf->outputdir, "./output/%i-bin", NBIN);
    result = mkdir(tconf->outputdir, 0755);
	
	sprintf(tconf->outputdir, "./output/%i-bin/%s", NBIN, tconf->morph_string);
    result = mkdir(tconf->outputdir, 0755);
    
	return 0;
}
int WRITE_CONFIG2DISK(struct DATA *data)
{
   FILE *fp;
   char fname[256];
   int mypid = getppid();
   tconf->myuid = mypid;

   sprintf(fname,"%s/config_%d.cfg", tconf->outputdir, mypid);
   
   fp = fopen(fname, "w+");
   
   fprintf(fp, "tomo_ia post-output config file\n");
   fprintf(fp, "================================================== \n");
   fprintf(fp, "* Runtime parameters \n");
   fprintf(fp, "NBIN: %i \n", NBIN);   	
   fprintf(fp, "alpha separation: %e \n", tugi_alpha);   	
   fprintf(fp, "Mode: %i (%s) \n", tconf->mode, tconf->mode_string);
   fprintf(fp, "Morphology: %i (%s) \n", tconf->morphology, tconf->morph_string);
   fprintf(fp, "P(k) linear? %s \n", tconf->Pktype_string);
   fprintf(fp, "Survey type: %i (%s)\n", tconf->surveytype, tconf->survey_string);
   fprintf(fp, "Spiral fraction: %f \n", tugi_q);
   fprintf(fp, "Brayon_transfer (0 means on): %i \n", tconf->baryon_transfer);
   fprintf(fp, "================================================== \n");   
   fprintf(fp, "* Cosmology \n");
   fprintf(fp, "Om = %f \n", gcosmo->omega_m);
   fprintf(fp, "Oq = %f \n", gcosmo->omega_q);
   fprintf(fp, "Ob = %f \n", gcosmo->omega_b);
   fprintf(fp, "w = %f \n", gcosmo->w);
   fprintf(fp, "n_s = %f \n", gcosmo->n_s);
   fprintf(fp, "sigma8 = %f \n", gcosmo->sigma8);
   fprintf(fp, "rsmooth = %f \n", gcosmo->rsmooth);
   fprintf(fp, "ellipticity = %f \n", gcosmo->ellipticity);
   fprintf(fp, "rsmoothscale = %f \n", gcosmo->rsmoothscale);
   fprintf(fp, "anorm = %f \n", gcosmo->anorm);
   fprintf(fp, "* Survey \n");
   fprintf(fp, "z mean  = %f \n", gcosmo->zmean);
   fprintf(fp, "beta = %f \n", gcosmo->beta);
   fprintf(fp, "f_sky = %f \n", gcosmo->f_sky);
   fprintf(fp, "nmean = %f \n", gcosmo->nmean);
   fprintf(fp, "================================================== \n");     
   fprintf(fp, "* Alignment: ELLIPSE \n");
   fprintf(fp, "tugi_align = %f \n", gcosmo->tugi_align);
   fprintf(fp, "* Alignment: SPIRAL \n");
   fprintf(fp, "tugi_hooke = %f \n", gcosmo->tugi_hooke);
   fprintf(fp, "hooke = %f \n", hooke);
   fprintf(fp, "cdm_halo_mass in 1e10 Msol/h= %f \n", cdm_halo_mass);
   fprintf(fp, "================================================== \n");   
   fprintf(fp, "* Gravslip \n");
   fprintf(fp, "tugi_eta = %f \n", gcosmo->tugi_eta);
   fprintf(fp, "tugi_mu = %f \n", gcosmo->tugi_mu);
   fprintf(fp, "================================================== \n");   
   fprintf(fp, "* Debug (integration etc.) \n");
   fprintf(fp, "eps_derivative = %f \n", eps_derivative);
   fprintf(fp, "ASTEP = %d \n", ASTEP);
   fprintf(fp, "AMIN = %f \n", AMIN);
   fprintf(fp, "AMAX = %f \n", AMAX);
   fprintf(fp, "CHIMIN = %f \n", CHIMIN);
   fprintf(fp, "CHIMAX = %f \n", CHIMAX);
   fprintf(fp, "CHISTEP = %d \n", CHISTEP);
   fprintf(fp, "LMAX_INT = %d \n", LMAX_INT);
   fprintf(fp, "LMAX = %d \n", LMAX);
   fprintf(fp, "LMIN = %d \n", LMIN);
   fprintf(fp, "LSTEP = %d \n", LSTEP);
   fprintf(fp, "DCHI for ellipticals = %f \n", DCHI_ellipticals);
   fprintf(fp, "DCHI for spirals = %f \n", DCHI_spirals);
   fprintf(fp, "CMIN = %f \n", CMIN);
   fprintf(fp, "CMAX = %f \n", CMAX);
   fprintf(fp, "CSTEP = %d \n", CSTEP);
   fprintf(fp, "TMIN = %f \n", TMIN);
   fprintf(fp, "TMAX = %f \n", TMAX);
   fprintf(fp, "TSTEP = %d \n", TSTEP);
   fprintf(fp, "================================================== \n");   
   fprintf(fp, "* files I wrote: \n");   

   fclose(fp);
	return 0;
}

int WRITE2CONFIG(char * line)
{
   FILE *fp;
   char fname[256];
   int mypid = tconf->myuid;
   sprintf(fname,"%s/config_%d.cfg", tconf->outputdir, mypid);   
 
   fp = fopen(fname, "a");
   fprintf(fp, "%s \n", line);   
 
   fclose(fp);
   return 0;
}
// --- function view_matrix
int view_matrix(gsl_matrix *matrix)
{
	double aux;
	int i,j;

	for(i=0;i<matrix->size1;i++)
	{
		for(j=0;j<matrix->size2;j++)
		{
			aux = gsl_matrix_get(matrix,i,j);
			printf("%e ",aux);
		}
		printf("\n");
	}

	return(0);
}

// ---  ---
int SAVE_LENSING_SPECTRA2DISK(struct DATA *data)
{
	int a,b,l;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: saving noisy spectra to disk....\n");

	sprintf(fname,"%s/kappa_%i.data", tconf->outputdir, NBIN);
	
	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# l c_noise \n");
	for(l=LMIN;l<LMAX;l++)
	{
		fprintf(file,"%i  ",l);
		for(a=0;a<NBIN;a++)
		{
			for(b=a;b<NBIN;b++)
			{
				fprintf(file,"%e  ",data->c_noise[l][a][b]);
			}
		}
		fprintf(file,"\n");
	}

	fclose(file);

	return(0);
}
// ---  ---
int WRITE_DERVIATIVE_2DISK(struct DATA *data, int iFish)
{
	int i,l,a,b;
	FILE *file;
	double aux;
	char fname[256];

	printf("tomo_ia: output: saving 1/C * dC / dtheta_i ....\n");

	sprintf(fname,"%s/logDerivC_%i_%ibin.data",tconf->outputdir, iFish, NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# l 1/C dC/dtheta_i (times nbin)\n");

	for(l=LMIN;l<LMAX;l++)
	{
		fprintf(file,"%i  ",l);
		for(a=0;a<NBIN;a++)
		{
			for(b=a;b<NBIN;b++)
			{
				aux = (1/data->cv[l][a][b]) * data->dc[l][a][b][iFish];
				fprintf(file,"%e  ", aux);
			}//b
		}//a
		fprintf(file,"\n");
	}
	fclose(file);
	return(0);
}
// ---  ---
int SAVE_SIGNIFICANCE2DISK(struct DATA *data)
{
	int i;
	FILE *file;
	char fname[256];
	double l;

	printf("tomo_ia: output: saving significances to disk....\n");

	if(tconf->mode==mode_hooke) sprintf(fname,"%s/s2n_ellipticity_eb_%e.data",tconf->outputdir, hooke);
	else sprintf(fname,"%s/s2n_ellipticity_eb_%i.data",tconf->outputdir, NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# ell ce_s2n cb_s2n cs_s2n cc_s2n cse_s2n csec_s2n \n");
	for(i=LMIN;i<LMAX;i++)
	{
		fprintf(file,"%i  %e  %e  %e  %e  %e  %e\n",
			i,
			data->ce_s2n[i],data->cb_s2n[i],data->cs_s2n[i],
			data->cc_s2n[i],data->cse_s2n[i],data->csec_s2n[i]);
	}
	fclose(file);

	return(0);
}
int SAVE_DCHI22DISK(struct DATA *data)
{
	int i;
	FILE *file;
	char fname[256];
	
	printf("tomo_ia: output: saving Dchi2 to disk....\n");

	sprintf(fname,"%s/sep_Dchi2_%i.data",tconf->outputdir, NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# ell dChi2 diffdChi2\n");
	for(i=LMIN;i<LMAX;i++)
	{
		fprintf(file,"%i  %e  %e\n",
			i, data->Dchisq[i], data->deltaDchisq[i]);
	}
	fclose(file);

	return(0);
}
// ---  ---
int SAVE_SPECTRUM_SIGNIFICANCE2DISK(struct DATA *data)
{
	int i;
	FILE *file;
	char fname[256];
	double l;

	printf("tomo_ia: output: saving significances to disk....\n");

	sprintf(fname,"%s/s2n_ellipticity_%i.data",tconf->outputdir, NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# ell ck_s2n \n");
	for(i=LMIN;i<LMAX;i++)
	{
		fprintf(file,"%i  %e  \n",
			i,
			data->ck_s2n[i]);
	}
	fclose(file);

	return(0);
}
// ---  ---
int SAVE_SIGNIFICANCE2DISK_SEPARATION(struct DATA *data)
{
	int i;
	FILE *file;
	char fname[256];
	double l;

	printf("tomo_ia: output: saving significances to disk....\n");

	sprintf(fname,"%s/s2n_ellipticity_sep_%i.data",tconf->outputdir, NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# ell c_s2n dc_s2n \n");
	for(i=LMIN;i<LMAX;i++)
	{
		fprintf(file,"%i  %e  %e  \n",
			i,
			data->ck_s2n[i],
			data->dck_s2n[i]);
	}
	fclose(file);

	return(0);
}
// ---  ---


// ---  ---
int SAVE_FISHER_MATRIX2DISK(struct DATA *data)
{
	double aux;
	int i,j;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: saving Fisher-matrix to disk....\n");

	if(tconf->mode==mode_sep_aDfisher)
		sprintf(fname,"%s/fisher_aD_%i.data",tconf->outputdir, NBIN);
	else if(tconf->mode==mode_sep_biases)
		sprintf(fname,"%s/fisher_%e_%i.data",tconf->outputdir,tugi_alpha,NBIN);
		else 
		sprintf(fname,"%s/fisher_%i.data",tconf->outputdir, NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");

	for(i=0;i<NFISHER;i++)
	{
		for(j=0;j<NFISHER;j++)
		{
			aux = gsl_matrix_get(data->fish,i,j);
			fprintf(file,"%e  ",aux);
		}
		fprintf(file,"\n");
	}

	fclose(file);

	return(0);
}
// ---  ---


// ---  ---
int WRITE_ANGULAR_II_ELLIPTICITY2DISK(struct DATA *data,int i)
{
	int t;
	double theta;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: writing II ellipticity angular correlation functions to disk....\n");

	sprintf(fname,"%s/angular_ii_%i_%i.data",tconf->outputdir,NBIN,i);	

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# theta eps_pp eps_cc eps_sp eps_ss \n");
	for(t=0;t<TSTEP;t++)
	{
		theta = tstep(t) * spirou_rad2min;
		fprintf(file,"%e  %e  %e  %e  %e\n",
			theta,data->epsilon_pp[t],data->epsilon_cc[t],data->epsilon_sp[t],data->epsilon_ss[t]);
	}
	fclose(file);

	return(0);
}
// ---  ---
int WRITE_ANGULAR_GI_ELLIPTICITY2DISK(struct DATA *data,int i)
{
	int t,j;
	double theta;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: writing GI ellipticity angular correlation functions to disk....\n");

	sprintf(fname,"%s/angular_gi_%i_%i.data",tconf->outputdir,NBIN,i);
	WRITE2CONFIG(fname);
	file = fopen(fname,"w+");
	fprintf(file,"# theta eps_gi_pp(times NBIN) eps_gi_cc(times NBIN) eps_gi_sp(times NBIN) eps_gi_ss(times NBIN) \n");
		
	
	for(t=0;t<TSTEP;t++)
	{
		theta = tstep(t) * spirou_rad2min;
		fprintf(file,"%e  ",theta);
		for(j=i;j<NBIN;j++)
		{
				fprintf(file,"%e  ",data->epsilon_gi_pp[j][t]);
		}
		for(j=i;j<NBIN;j++)
		{
				fprintf(file,"%e  ",data->epsilon_gi_cc[j][t]);
		}
		for(j=i;j<NBIN;j++)
		{
				fprintf(file,"%e  ",data->epsilon_gi_sp[j][t]);
		}
		for(j=i;j<NBIN;j++)
		{
				fprintf(file,"%e  ",data->epsilon_gi_ss[j][t]);
		}

		fprintf(file,"\n");	
	}
	fclose(file);

	return(0);
}
// ---  ---


/* --- function save_bias2disk [saves parameter estimation biases to disk] --- */
int SAVE_BIAS2DISK(struct DATA *data)
{
	int i;
	double aux;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: saving parameter estimation bias to disk....\n");
	if(tconf->mode==mode_sep_biases) sprintf(fname,"%s/bias_%e_%i.data",tconf->outputdir,tugi_alpha,NBIN);
	else sprintf(fname,"%s/bias_%i.data",tconf->outputdir,NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");

	for(i=0;i<NFISHER;i++)
	{
		aux = gsl_vector_get(data->bias,i);
		fprintf(file,"%e  ",aux);
	}
	fprintf(file,"\n");

	fclose(file);

	return(0);
}
/* ---  --- */
// ---  ---
int SAVE_ELLIPTICITY_SPECTRA2DISK(struct DATA *data)
{
	int i,l,j;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: saving ellipticity spectra....\n");
	//II
	sprintf(fname,"%s/ellipticity_eb_ii_%i.data",tconf->outputdir,NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# l ce cb (times nbin)\n");

	for(l=LMIN;l<LMAX;l++)
	{
		fprintf(file,"%i  ",l);
		for(i=0;i<NBIN;i++)
		{
			fprintf(file,"%e  %e  ", data->ce[l][i],data->cb[l][i]);
		}
		fprintf(file,"\n");
	}
	fclose(file);
	//GI
	if(tconf->flag_GIs==on)
	{
		sprintf(fname,"%s/ellipticity_eb_gi_%i.data",tconf->outputdir,NBIN);

		WRITE2CONFIG(fname);

		file = fopen(fname,"w+");
		fprintf(file,"# l ce cb (times nbin2)\n");

		for(l=LMIN;l<LMAX;l++)
		{
			fprintf(file,"%i  ",l);
			
			for(i=0;i<NBIN;i++)
			{
				for(j=i;j<NBIN;j++)
				{
				fprintf(file,"%e  ", data->ce_gi[l][i][j]);
				}
			}

			for(i=0;i<NBIN;i++)
			{
				for(j=i;j<NBIN;j++)
				{
				fprintf(file,"%e  ",data->cb_gi[l][i][j]);
				}
			}
			fprintf(file,"\n");
		}
		fclose(file);
	} // morphology-if
	return(0);
}
// ---  ---

// ---  ---
int SAVE_ELLIPTICITY_CMODE2DISK(struct DATA *data)
{
	int i,j,l;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: saving ellipticity spectra....\n");
	//II
	sprintf(fname,"%s/ellipticity_sc_ii_%i.data",tconf->outputdir,NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# l cs cc (times nbin) \n");

	for(l=LMIN;l<LMAX;l++)
	{
		fprintf(file,"%i  ",l);
		for(i=0;i<NBIN;i++)
		{
			fprintf(file,"%e  %e  ",data->cs[l][i],data->cc[l][i]);
		}
		fprintf(file,"\n");
	}
	fclose(file);
	if(tconf->flag_GIs==on)
	{
		sprintf(fname,"%s/ellipticity_sc_gi_%i.data",tconf->outputdir,NBIN);

		WRITE2CONFIG(fname);

		file = fopen(fname,"w+");
		fprintf(file,"# l cs cc (times nbin2)\n");

		for(l=LMIN;l<LMAX;l++)
		{
			fprintf(file,"%i  ",l);
			
			for(i=0;i<NBIN;i++)
			{
				for(j=i;j<NBIN;j++)
				{
				fprintf(file,"%e  ", data->cs_gi[l][i][j]);
				}
			}

			for(i=0;i<NBIN;i++)
			{
				for(j=i;j<NBIN;j++)
				{
				fprintf(file,"%e  ",data->cc_gi[l][i][j]);
				}
			}
			fprintf(file,"\n");
		}
		fclose(file);
	} // morphology-if
	return(0);
}
int SAVE_ELLIPTICITY_C_SEPERATION(struct DATA *data)
{
	int i,j,l;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: saving ellipticity spectra....\n");
	//II
	sprintf(fname,"%s/sep_ellipticity_C_ii_%i.data",tconf->outputdir,NBIN);

	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# l C X (times nbin) \n");

		for(l=LMIN;l<LMAX;l++)
		{
			fprintf(file,"%i  ",l);
			
			for(i=0;i<NBIN;i++)
			{
				for(j=i;j<NBIN;j++)
				{
				fprintf(file,"%e  ", data->cv[l][i][j]);
				}
			}

			for(i=0;i<NBIN;i++)
			{
				for(j=i;j<NBIN;j++)
				{
				fprintf(file,"%e  ",data->X[l][i][j]);
				}
			}
			fprintf(file,"\n");
		}
	fclose(file);
	
	return(0);
}
// ---  ---
// saving weak lensing correlation functions
int SAVE_LENSING_CORRELATION2DISK(struct DATA *data)
{
	char fname[256];
	FILE *file;
	int t,a,b;
	double theta;

	printf("tomo_ia: output: saving lensing correlation functions to disk....\n");

	sprintf(fname,"%s/gamma_%i.data",tconf->outputdir,NBIN);
	
	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# theta(arcmin) gamma[l] (times nbin2) \n");
	for(t=0;t<TSTEP;t++)
	{
		theta = tstep(t) * spirou_rad2min;
		fprintf(file,"%e  ",theta);

		for(a=0;a<NBIN;a++)
		{
			for(b=a;b<NBIN;b++)
			{
				fprintf(file,"%e  ",data->gamma[t][a][b]);
			}
		}
		fprintf(file,"\n");
	}
	fclose(file);

	return(0);
}

// saving the weak lensing correlation coefficient
int SAVE_LENSING_CCOEFFICIENTS2DISK(struct DATA *data)
{
	int l,a,b;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: saving weak lensing correlation coefficients to disk....\n");

	sprintf(fname,"%s/ccoefficient_nl_%i.data",tconf->outputdir,NBIN);
	
	WRITE2CONFIG(fname);

	file = fopen(fname,"w+");
	fprintf(file,"# l ccoefficiant[l] (times nbin2) \n");
	for(l=LMIN;l<LMAX;l++)
	{
		fprintf(file,"%i  ",l);

		for(a=0;a<NBIN;a++)
		{
			for(b=a+1;b<NBIN;b++)
			{
				fprintf(file,"%e  ",data->ccoefficient[l][a][b]);
			}
		}
		fprintf(file,"\n");
	}
	fclose(file);

	return(0);
}

int SAVE_SPECTRAL_MOMENTS2DISK(struct DATA *data)
{
	int j, i, a, b;
	FILE *file;
	char fname[256];

	printf("tomo_ia: output: saving spectral moments to disk....\n");

	for(j=0;j=SPECTRAL_MOMENT_MAX;j++)
	{
		sprintf(fname,"%s/spectral_moments_sigma_%i_%i-bin.data",tconf->outputdir,j,NBIN);
		
		WRITE2CONFIG(fname);

		file = fopen(fname,"w+");
		fprintf(file,"# just lensing (times nbin 2) \t lensing+alignment (times nbin 2)\n");
		for(i=0;i<2;i++)
		{
			for(a=0;a<NBIN;a++)
			{
				for(b=a+1;b<NBIN;b++)
				{
					fprintf(file,"%e  ",data->spectral_moment[j][i][a][b]);
				}
			}
		fprintf(file,"\n");
		}
		fclose(file);
	}
	return(0);
}