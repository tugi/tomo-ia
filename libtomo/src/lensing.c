#include <tomo.h>

/* --- function tstep [linear equidistant stepping in angle theta] --- */
double tstep_lin(int i)
{
	double result,delta;

	delta = pow(TMAX / TMIN,1.0 / ((double)(TSTEP - 1)));;
	result = TMIN * gsl_pow_int(delta,(double)i);

	return(result);
}
/* --- end of function tstep --- */
/* --- function tstep [linear equidistant stepping in angle theta] --- */
double tstep(int i)
{
	double result,delta;

	delta = pow(TMAX / TMIN, 1.0 / ((double)(TSTEP - 1)));;
	result = TMIN * gsl_pow_int(delta,i);

	return(result);
}
/* --- end of function tstep --- */
// ---  ---
int SPLIT_UP_GALAXY_SAMPLE(struct DATA *data)
{
	int i;
	double frac;

	printf("tomo_ia: lensing: splitting up galaxy sample for %i bin(s)....\n",NBIN);

	// find bin edges in z
	for(i=0;i<NBIN;i++)
	{

		frac = (double)(i) / (double)(NBIN);
		gdata->tomo_z[i] = p_galaxy_inverse(frac);
	}
	gdata->tomo_z[NBIN] = 4.0; // default max redshift

	// convert to comoving distance
	for(i=0;i<=NBIN;i++)
	{	
		gdata->tomo[i] = a2com(z2a(gdata->tomo_z[i]));
	}

	// list for checking
	for(i=0;i<=NBIN;i++)
	{
		printf("      %i --> z = %e --> chi = %e Mpc/h --> p = %e\n",
			i,gdata->tomo_z[i],i,gdata->tomo[i],p_galaxy_cumulative(gdata->tomo_z[i]));
	}
	printf("....done!\n",NBIN);
	return(0);
}
// ---  ---


// ---  ---
double p_galaxy_inverse(double frac)
{
	int status;
	int iter = 0, max_iter = 100;
	const gsl_root_fsolver_type *T;
	gsl_root_fsolver *s;
	double r = 0;
	double x_lo = 0.0, x_hi = 10.0;
	gsl_function F;

	F.function = &aux_p_galaxy_inverse;
	F.params = &frac;

	T = gsl_root_fsolver_bisection;
	s = gsl_root_fsolver_alloc(T);
	gsl_root_fsolver_set (s, &F, x_lo, x_hi);

	do {
		iter++;
		status = gsl_root_fsolver_iterate(s);
		r = gsl_root_fsolver_root(s);
		x_lo = gsl_root_fsolver_x_lower(s);
		x_hi = gsl_root_fsolver_x_upper(s);
		status = gsl_root_test_interval (x_lo, x_hi,0,0.001);
         } while (status == GSL_CONTINUE && iter < max_iter);

       gsl_root_fsolver_free (s);

       return(r);
}
// ---  ---


// ---  ---
double aux_p_galaxy_inverse(double z,void *params)
{
	double frac = *(double *)params;
	double result;

	result = p_galaxy_cumulative(z) - frac;

	return(result);
}
// ---  ---


// ---  ---
double p_galaxy_cumulative(double z)
{
	double aux,result;

	aux = pow(z / gcosmo->zmean,gcosmo->beta);
	result = 1.0 - exp(-aux) * (aux + 1.0);

	return(result);
}
// ---  ---


// ---  ---
int COMPUTE_LENSING_SPECTRA(struct DATA *data)
{
	int a;

	printf("tomo_ia: lensing: computing weak lensing spectra....\n");

#pragma omp parallel
	{
		int b,l;
		double aux;
		struct SCONFIG *sconfig = (struct SCONFIG *)calloc(1,sizeof(struct SCONFIG));

#pragma omp for
		for(a=0;a<NBIN;a++)
		{
			for(b=a;b<NBIN;b++)
			{
				for(l=LMIN;l<LMAX;l++)
				{
					sconfig->l = l;
					sconfig->a = a;
					sconfig->b = b;

					if(tconf->mode == mode_tomography)
						aux = tomo_spectrum(sconfig);
					else
						aux = tomo_noise(sconfig->l,sconfig->a,sconfig->b);

					data->c_noise[l][a][b] = data->c_noise[l][b][a] = aux;
				}
			}
		}

		free(sconfig);
	}
	return(0);
}
// ---  ---

// computing weak lensing correlation functions (from the shear spectra)
int COMPUTE_LENSING_CORRELATION(struct DATA *data)
{
	int t;

	printf("tomo_ia: lensing: computing weak lensing angular correlation function....\n");

#pragma omp parallel
	{
		double theta;
		int a,b;

#pragma omp for
		for(t=0;t<TSTEP;t++)
		{
			theta = tstep(t);
			for(a=0;a<NBIN;a++)
			{
				for(b=a;b<NBIN;b++)
				{
					data->gamma[t][a][b] = data->gamma[t][b][a] = gamma_lensing(theta,a,b);
				}
			}
		}
	}

	return(0);
}

// computing the correlation coefficient between weak lensing spectra
int COMPUTE_LENSING_CCOEFFICIENTS(struct DATA *data)
{
	int l;

	printf("tomo_ia: lensing: computing weak lensing correlation coefficients....\n");

#pragma omp parallel
	{
		int a,b;
		double aux;

#pragma omp for
		for(l=LMIN;l<LMAX;l++)
		{
			for(a=0;a<NBIN;a++)
			{
				for(b=0;b<NBIN;b++)
				{
					aux = tomo_noise(l,a,a) * tomo_noise(l,b,b);
					data->ccoefficient[l][a][b] = tomo_noise(l,a,b) / sqrt(aux);
				}
			}
		}
	}

	return(0);
}
// ---  ---

int COMPUTE_S2N_SPECTRUM(struct DATA *data)
{
	double noise,sum;
	int signum;
	int a,l;
	gsl_matrix *ss,*cc,*ci,*ciss,*lu;
	gsl_permutation *p;
	struct SCONFIG *sconfig;

	printf("tomo_ia: lensing: summing up the s2n-ratio for the weak lensing spectrum....\n");

	// construction of the covariance matrices on the spot
	// take account of full Gaussian formula
	// parallelisation at the point of covariance matrix construction

	ss = gsl_matrix_calloc(NBIN,NBIN);
	cc = gsl_matrix_calloc(NBIN,NBIN);
	ci = gsl_matrix_calloc(NBIN,NBIN);
	ciss = gsl_matrix_calloc(NBIN,NBIN);
	lu = gsl_matrix_calloc(NBIN,NBIN);
	p = gsl_permutation_alloc(NBIN);

	sum = 0.0;
	for(l=LMIN;l<LMAX;l++)
	{
		// compute values for the signal covariance
		gsl_matrix_set_zero(ss);
#pragma omp parallel
		{
			int b;
			double aux;

#pragma omp for
			for(a=0;a<NBIN;a++)
			{
				for(b=a;b<NBIN;b++)
				{
					aux = tomo_kappa(l,a,b);
					gsl_matrix_set(ss,a,b,aux);
					gsl_matrix_set(ss,b,a,aux);
				}
			}
		}

		// add noise on the diagonal for the full covariance
		gsl_matrix_set_zero(cc);
		noise = gcosmo->ellipticity / gcosmo->nmean * NBIN;
		for(a=0;a<NBIN;a++)
		{
			gsl_matrix_set(cc,a,a,noise);
		}
		gsl_matrix_add(cc,ss);

		// compute inverse covariance
		gsl_matrix_memcpy(lu,cc);
		gsl_linalg_LU_decomp(lu,p,&signum);
		gsl_linalg_LU_invert(lu,p,ci);

		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,ss,0.0,ciss);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ciss,ciss,0.0,cc);

		// trace out and sum up
		sum += gcosmo->f_sky * (2.0 * l + 1.0) / 2.0 * spirou_trace(cc);
		data->ck_s2n[l] = sqrt(sum);
	}

	gsl_matrix_free(ss);
	gsl_matrix_free(cc);
	gsl_matrix_free(ci);
	gsl_matrix_free(ciss);
	gsl_matrix_free(lu);
	gsl_permutation_free(p);

	return(0);
}
// ---  ---
int print_matrix(const gsl_matrix *m)
{
        int status, n = 0;
        size_t i, j;
        for (i = 0; i < m->size1; i++) {
                for (j = 0; j < m->size2; j++) {
                        if ((status = printf("%g ", gsl_matrix_get(m, i, j))) < 0)
                                return -1;
                        n += status;
                }

                if ((status = printf("\n")) < 0)
                        return -1;
                n += status;
        }

        return n;
}
int COMPUTE_S2N_SPECTRUM_SEPARATION(struct DATA *data)
{
	double sum, aux, noise, dck;
	int signum;
	int a,b,l;
	gsl_matrix *ss,*cc,*ci,*ciss,*lu;
	gsl_matrix *cisssq;
	gsl_permutation *p;
	struct SCONFIG *sconfig;

	printf("tomo_ia: lensing: summing up the s2n-ratio for separated....\n");

	// construction of the covariance matrices on the spot
	// take account of full Gaussian formula
	// parallelisation at the point of covariance matrix construction
	
	lu = gsl_matrix_calloc(NBIN,NBIN);
	p = gsl_permutation_alloc(NBIN);

	ss = gsl_matrix_calloc(NBIN,NBIN); //  signal
	cc = gsl_matrix_calloc(NBIN,NBIN); // signal+noise
	ci = gsl_matrix_calloc(NBIN,NBIN); // s+n invert
	ciss = gsl_matrix_calloc(NBIN,NBIN);
	cisssq = gsl_matrix_calloc(NBIN,NBIN);
	

	sum = 0.0;

	for(l=LMIN;l<LMAX;l++)
	{
		// compute values for the signal covariance
		gsl_matrix_set_zero(ss);
		for(a=0;a<NBIN;a++)
		{
			for(b=a;b<NBIN;b++)
			{
				aux = S_separation_adapter(data,l,a,b,tugi_alpha);
				gsl_matrix_set(ss,a,b,aux);
				gsl_matrix_set(ss,b,a,aux);
			}
		}

		// add noise on the diagonal for the full covariance
		gsl_matrix_set_zero(cc);

		for(a=0;a<NBIN;a++)
		{		
			for(b=a;b<NBIN;b++)
			{
				noise = C_separation_adapter(data, l,a,b,tugi_alpha);
				gsl_matrix_set(cc,a,b,noise);
				gsl_matrix_set(cc,b,a,noise);
			}
		}
		
		// compute inverse covariance
		gsl_matrix_memcpy(lu,cc);
		
		gsl_linalg_LU_decomp(lu,p,&signum);
		gsl_linalg_LU_invert(lu,p,ci);

		// Here needs to be taken the Submatrix! _only_ 
		
		//gsl_matrix_view ci_view = gsl_matrix_submatrix(ci,0,0,NBIN,NBIN);
		//gsl_matrix_view ss_view = gsl_matrix_submatrix(ss,0,0,NBIN,NBIN);
		
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,ss,0.0,ciss);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ciss,ciss,0.0,cisssq);

		// trace out and sum up
		dck = gcosmo->f_sky * (2.0 * l + 1.0) / 2.0 * spirou_trace(cisssq);
		sum += dck;
		data->ck_s2n[l] = sqrt(sum);
		data->dck_s2n[l] = dck;

	}
	
	gsl_matrix_free(cisssq);
	gsl_matrix_free(ss);
	gsl_matrix_free(cc);
	gsl_matrix_free(ci);
	gsl_matrix_free(ciss);
	gsl_matrix_free(lu);
	gsl_permutation_free(p);

	return(0);
}
// ---  ---


// ---  ---
int COMPUTE_S2N_ELLIPTICITY(struct DATA *data)
{
	double sum_e,sum_b,sum_s;
	int signum;
	int a,l;
	gsl_matrix *se,*sb,*ss,*gg,*nn;
	gsl_matrix *cc,*ci,*ciss,*lu;
	gsl_permutation *p;

	printf("tomo_ia: lensing: summing up the s2n-ratio for the ellipticity spectrum (e, b and s)....\n");

	// construction of the covariance matrices on the spot
	// take account of full Gaussian formula
	// parallelisation at the point of covariance matrix construction
	se = gsl_matrix_calloc(NBIN,NBIN);
	sb = gsl_matrix_calloc(NBIN,NBIN);
	ss = gsl_matrix_calloc(NBIN,NBIN);
	nn = gsl_matrix_calloc(NBIN,NBIN);
	gg = gsl_matrix_calloc(NBIN,NBIN);
	cc = gsl_matrix_calloc(NBIN,NBIN);
	ci = gsl_matrix_calloc(NBIN,NBIN);
	ciss = gsl_matrix_calloc(NBIN,NBIN);
	lu = gsl_matrix_calloc(NBIN,NBIN);
	p = gsl_permutation_alloc(NBIN);

	sum_e = sum_b = sum_s = 0.0;
	for(l=LMIN;l<LMAX;l++)
	{
		// compute values for the signal covariance
#pragma omp parallel
		{
			int b;
#pragma omp for
			for(a=0;a<NBIN;a++)
			{
				if(tconf->flag_GIs==on)
				{
					for(b=a;b<NBIN;b++)
					{
						// printf("b=%i\n",b);
						if(a==b) // diagonals has GI + II
						{
							gsl_matrix_set(se,a,a, data->ce[l][a] + data->ce_gi[l][a][a]);
							gsl_matrix_set(sb,a,a, data->cb[l][a] + data->cb_gi[l][a][a]);
							gsl_matrix_set(ss,a,a, data->cs[l][a] + data->cs_gi[l][a][a]);							
						}
						else // just GIs on the off diagonals
						{
							gsl_matrix_set(se,a,b, data->ce_gi[l][a][b]);
							gsl_matrix_set(sb,a,b, data->cb_gi[l][a][b]);
							gsl_matrix_set(ss,a,b, data->cs_gi[l][a][b]);

							gsl_matrix_set(se,b,a, data->ce_gi[l][a][b]);
							gsl_matrix_set(sb,b,a, data->cb_gi[l][a][b]);
							gsl_matrix_set(ss,b,a, data->cs_gi[l][a][b]);														
						}
											
					}
				}
				else
				{
					// No GIs
					gsl_matrix_set(se,a,a, data->ce[l][a]);
					gsl_matrix_set(sb,a,a, data->cb[l][a]);
					gsl_matrix_set(ss,a,a, data->cs[l][a]);					
				}
			}
		}

		// compute values for lensing
#pragma omp parallel
		{
			double aux;
			int b;

#pragma omp for
			for(a=0;a<NBIN;a++)
			{
				for(b=a;b<NBIN;b++)
				{
					aux = tomo_kappa(l,a,b);
					gsl_matrix_set(gg,a,b,aux);
					gsl_matrix_set(gg,b,a,aux);
				}
			}
		}

		// compute noise
#pragma omp parallel
		{
			double aux = gcosmo->ellipticity / gcosmo->nmean * NBIN;

#pragma omp for
			for(a=0;a<NBIN;a++)
			{
				gsl_matrix_set(nn,a,a,aux);
			}
		}

		// compute s2n for ellipticity e-mode
		// covariance has ellipticity E-mode, lensing E-mode, shape noise
		gsl_matrix_memcpy(cc,se);
		gsl_matrix_add(cc,gg);
		gsl_matrix_add(cc,nn);

		// compute inverse covariance
		gsl_matrix_memcpy(lu,cc);
		gsl_linalg_LU_decomp(lu,p,&signum);
		gsl_linalg_LU_invert(lu,p,ci);

		// matrix products
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,se,0.0,ciss);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ciss,ciss,0.0,cc);

		// trace out and sum up
		sum_e += gcosmo->f_sky * (2.0 * l + 1.0) / 2.0 * spirou_trace(cc);
		data->ce_s2n[l] = sqrt(sum_e);

		// compute s2n for ellipticity b-mode
		// covariance has ellipticity B-mode, *NO* lensing B-mode, shape noise
		gsl_matrix_memcpy(cc,sb);
		gsl_matrix_add(cc,nn);

		// compute inverse covariance
		gsl_matrix_memcpy(lu,cc);
		gsl_linalg_LU_decomp(lu,p,&signum);
		gsl_linalg_LU_invert(lu,p,ci);

		// matrix products
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,sb,0.0,ciss);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ciss,ciss,0.0,cc);

		// trace out and sum up
		sum_b += gcosmo->f_sky * (2.0 * l + 1.0) / 2.0 * spirou_trace(cc);
		data->cb_s2n[l] = sqrt(sum_b);

		if(tconf->flag_crossspectra==on)
		{
			// compute s2n for scalar ellipticity spectrum
			// covariance has ellipticity S-mode, lensing E-mode, shape noise
			gsl_matrix_memcpy(cc,ss);
			gsl_matrix_add(cc,gg);
			gsl_matrix_add(cc,nn);

			// compute inverse covariance
			gsl_matrix_memcpy(lu,cc);
			gsl_linalg_LU_decomp(lu,p,&signum);
			gsl_linalg_LU_invert(lu,p,ci);

			// matrix products
			gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,ss,0.0,ciss);
			gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ciss,ciss,0.0,cc);

			// trace out and sum up
			sum_s += gcosmo->f_sky * (2.0 * l + 1.0) / 2.0 * spirou_trace(cc);
			data->cs_s2n[l] = sqrt(sum_s);
		}
	}

	gsl_matrix_free(se);
	gsl_matrix_free(sb);
	gsl_matrix_free(ss);
	gsl_matrix_free(gg);
	gsl_matrix_free(cc);
	gsl_matrix_free(ci);
	gsl_matrix_free(ciss);
	gsl_matrix_free(lu);
	gsl_permutation_free(p);

	return(0);
}
// ---  ---


// --- cross spectra included, covariance matrices twice as large, 3 cases ---
int COMPUTE_S2N_CROSS(struct DATA *data)
{
	double sum_se,sum_cc,sum_sec;
	int signum;
	int a,l;
	gsl_matrix *ss,*se,*sc,*gg,*nn;
	gsl_matrix *cc,*ci,*lu,*ciss;
	gsl_permutation *p;
	gsl_matrix_view scope;

	printf("tomo_ia: lensing: summing up the s2n-ratio for the ellipticity spectrum (cross)....\n");

	// nxn matrices
	ss = gsl_matrix_calloc(NBIN,NBIN);
	se = gsl_matrix_calloc(NBIN,NBIN);
	sc = gsl_matrix_calloc(NBIN,NBIN);
	gg = gsl_matrix_calloc(NBIN,NBIN);
	nn = gsl_matrix_calloc(NBIN,NBIN);

	// 2nx2n matrices
	cc = gsl_matrix_calloc(NBIN2,NBIN2);
	ci = gsl_matrix_calloc(NBIN2,NBIN2);
	lu = gsl_matrix_calloc(NBIN2,NBIN2);
	ciss = gsl_matrix_calloc(NBIN2,NBIN2);
	p = gsl_permutation_alloc(NBIN2);

	sum_se = sum_cc = sum_sec = 0.0;
	for(l=LMIN;l<LMAX;l++)
	{
		// compute values for the signal covariance --> diagonal
#pragma omp parallel
		{
			int b;
#pragma omp for
			for(a=0;a<NBIN;a++)
			{
				if(tconf->flag_GIs==on)
				{
				for(b=a;b<NBIN;b++)
				{
						if(a==b) // diagonals has GI + II
						{
							gsl_matrix_set(ss,a,b, data->cs[l][a]+data->cs_gi[l][a][a]);
							gsl_matrix_set(se,a,b, data->ce[l][a]+data->ce_gi[l][a][a]);
							gsl_matrix_set(sc,a,b, data->cc[l][a]+data->cc_gi[l][a][a]);							
						}
						else // just GIs on the off diagonals
						{
							gsl_matrix_set(ss,a,b, data->cs_gi[l][a][b]);
							gsl_matrix_set(sc,a,b, data->ce_gi[l][a][b]);
							gsl_matrix_set(se,a,b, data->cc_gi[l][a][b]);

							gsl_matrix_set(ss,b,a, data->cs_gi[l][a][b]);
							gsl_matrix_set(sc,b,a, data->ce_gi[l][a][b]);
							gsl_matrix_set(se,b,a, data->cc_gi[l][a][b]);														
						}

				}
				}
				else
				{
				gsl_matrix_set(ss,a,a, data->cs[l][a]);
				gsl_matrix_set(se,a,a, data->ce[l][a]);
				gsl_matrix_set(sc,a,a, data->cc[l][a]);					
				}
			}
		}

		// compute values for lensing --> full matrix, symmetric
#pragma omp parallel
		{
			double aux;
			int b;
#pragma omp for
			for(a=0;a<NBIN;a++)
			{
				for(b=a;b<NBIN;b++)
				{
					aux = tomo_kappa(l,a,b);
					gsl_matrix_set(gg,a,b,aux);
					gsl_matrix_set(gg,b,a,aux);
				}
			}
		}

		// compute noise --> diagonal
#pragma omp parallel
		{
			double aux = gcosmo->ellipticity / gcosmo->nmean * NBIN;

#pragma omp for
			for(a=0;a<NBIN;a++)
			{
				gsl_matrix_set(nn,a,a,aux);
			}
		}

		// clear matrix
		gsl_matrix_set_zero(cc);
		gsl_matrix_set_zero(ci);
		gsl_matrix_set_zero(ciss);

		// assemble covariance: full covariance: IA-part (diagonal stripes)
		scope = gsl_matrix_submatrix(cc,0,0,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),ss);
		scope = gsl_matrix_submatrix(cc,0,NBIN,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),sc);
		scope = gsl_matrix_submatrix(cc,NBIN,0,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),sc);
		scope = gsl_matrix_submatrix(cc,NBIN,NBIN,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),se);

		// assemble covariance: full covariance: lensing part (symmetric, nothing else in particular)
		scope = gsl_matrix_submatrix(cc,0,0,NBIN,NBIN);
		gsl_matrix_add(&(scope.matrix),gg);
		scope = gsl_matrix_submatrix(cc,NBIN,NBIN,NBIN,NBIN);
		gsl_matrix_add(&(scope.matrix),gg);

		// assemble covariance: full covariance: noise (diagonal stripes)
		scope = gsl_matrix_submatrix(cc,0,0,NBIN,NBIN);
		gsl_matrix_add(&(scope.matrix),nn);
		scope = gsl_matrix_submatrix(cc,NBIN,NBIN,NBIN,NBIN);
		gsl_matrix_add(&(scope.matrix),nn);

		// invert full covariance
		gsl_matrix_memcpy(lu,cc);
		gsl_linalg_LU_decomp(lu,p,&signum);
		gsl_linalg_LU_invert(lu,p,ci);

		// assemble signal matrix: signal covariance, <ee> and <ss> combined
		gsl_matrix_set_zero(cc);
		gsl_matrix_set_zero(ciss);

		scope = gsl_matrix_submatrix(cc,0,0,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),ss);
		scope = gsl_matrix_submatrix(cc,NBIN,NBIN,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),se);

		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,cc,0.0,ciss);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ciss,ciss,0.0,cc);

		sum_se += gcosmo->f_sky * (2.0 * l + 1.0) / 2.0 * spirou_trace(cc);
		data->cse_s2n[l] = sqrt(sum_se);

		// assemble signal matrix: signal covariance, only <se>
		gsl_matrix_set_zero(cc);
		gsl_matrix_set_zero(ciss);

		scope = gsl_matrix_submatrix(cc,0,NBIN,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),sc);
		scope = gsl_matrix_submatrix(cc,NBIN,0,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),sc);

		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,cc,0.0,ciss);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ciss,ciss,0.0,cc);

		sum_cc += gcosmo->f_sky * (2.0 * l + 1.0) / 2.0 * spirou_trace(cc);
		data->cc_s2n[l] = sqrt(sum_cc);

		// assemble signal matrix: signal covariance, <ee>, <se> <ss> combined
		gsl_matrix_set_zero(cc);
		gsl_matrix_set_zero(ciss);

		scope = gsl_matrix_submatrix(cc,0,0,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),ss);
		scope = gsl_matrix_submatrix(cc,NBIN,NBIN,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),se);
		scope = gsl_matrix_submatrix(cc,0,NBIN,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),sc);
		scope = gsl_matrix_submatrix(cc,NBIN,0,NBIN,NBIN);
		gsl_matrix_memcpy(&(scope.matrix),sc);

		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,cc,0.0,ciss);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ciss,ciss,0.0,cc);

		sum_sec += gcosmo->f_sky * (2.0 * l + 1.0) / 2.0 * spirou_trace(cc);
		data->csec_s2n[l] = sqrt(sum_sec);
	}

	// nxn matrices
	gsl_matrix_free(ss);
	gsl_matrix_free(se);
	gsl_matrix_free(sc);
	gsl_matrix_free(gg);
	gsl_matrix_free(nn);

	// 2nx2n matrices
	gsl_matrix_free(cc);
	gsl_matrix_free(ci);
	gsl_matrix_free(lu);
	gsl_matrix_free(ciss);
	gsl_permutation_free(p);

	return(0);
}
// ---  ---
int COMPUTE_CNOISE_IA(struct DATA *data)
{
	int l,a,b;
	int ell;
	for(l=0;l<LSTEP;l++)
	{
		ell = lstep(l);
		for(a=0;a<NBIN;a++)
		{
			for(b=a;b<a;b++)
			{
				data->c_noise_p_ia[ell][a][b] = data->c_noise[ell][a][b]+data->ce[ell][a]+data->ce_gi[ell][a][b];				
			}
		}
	}
}
int PREPARE_SPECTRAL_SPLINES(struct DATA *data)
{
	int l,i,a,b;
	for(l=0;l<LSTEP_INT;l++)
	{
		data->ell[l]=lstep(l);
	}

	printf("tomo_ia: lensing: preparing lookup tables for Cl Ceii and Cegi....\n");

	for(i=0;i<2;i++)
		{
			for(a=0;a<NBIN;a++)
			{
				for(b=a;b<NBIN;b++)
				{
					gcosmo->acc_Cl[i][a][b] = gsl_interp_accel_alloc();
					gcosmo->spline_Cl[i][a][b] = gsl_spline_alloc(gsl_interp_cspline,LSTEP);
					if(i==0) 
						{
							gsl_spline_init(gcosmo->spline_Cl[i][a][b], data->ell, data->c_noise[a][b], LSTEP);
							gsl_spline_init(gcosmo->spline_Cl[i][b][a], data->ell, data->c_noise[a][b], LSTEP);
						}
					else 
						{
							gsl_spline_init(gcosmo->spline_Cl[i][a][b], data->ell, data->c_noise_p_ia[a][b], LSTEP);
							gsl_spline_init(gcosmo->spline_Cl[i][b][a], data->ell, data->c_noise_p_ia[a][b], LSTEP);
						}	
				}// b
			}// a
		}// i

	return(0);
}

int COMPUTE_SPECTRAL_MOMENTS(struct DATA *data)
{

int a;
#pragma omp parallel
{
	int j,i,b;

	printf("tomo_ia: lensing: computing spectral moments....\n");

#pragma omp for

	for(a=0;a<NBIN;a++)
	{
		for(b=a;b<NBIN;b++)
		{
			for(j=0;j=SPECTRAL_MOMENT_MAX; j++)
			{
				for(i=0;i<2;i++)
				{
					data->spectral_moment[j][i][a][b]=spectral_moment_j(j,i,a,b);
				}
			}
		}
	}

}
	return(0);
}

double spectral_moment_j(int j, int i, int a, int b)
{
	double result,aux,error;
	struct SCONFIG *sconfig = (struct SCONFIG *)calloc(1,sizeof(struct SCONFIG));
	sconfig->i = i; //intrinsic alignments switch
	sconfig->j = j; //j
	sconfig->i = a; // tomo a
	sconfig->j = b; // tomo b

	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	F.function = &aux_spectral_moment_j;
	F.params = sconfig;

	gsl_integration_qag(&F,200,600,eps,eps,NEVAL,GSL_INTEG_GAUSS61,w,&aux,&error);
	result = sqrt(aux / (2*pi) );	

	gsl_integration_workspace_free(w);
	free(sconfig);

	return(result);

}
double aux_spectral_moment_j(double l, void *param)
{
	double result, aux, factor;
	double Cl, sigma, filter, norm;
	int j,i,a,b;
	struct SCONFIG *sconfig = (struct SCONFIG *)param;
	a = sconfig->a;
	b = sconfig->b;
	i = sconfig->i;
	j = sconfig->j;

	Cl = gsl_spline_eval(gcosmo->spline_Cl[i][a][b],l,gcosmo->acc_Cl[i][a][b]);
	// if(i==0) Cl = tomo_noise(l,a,b);
	// else Cl = tomo_noise(l,a,b) + ii_emode(l,a) + gi_emode(l,a,b);

	
	factor = gsl_pow_int(l, (2 * j + 1) );

	aux = l;
	sigma = 30;
	norm = 1/sqrt(2*pi) * 1/sigma;

	//filter = norm * exp(-gsl_pow_int( (aux-300)/sigma, 2)/2);

	result = factor * Cl;// * filter * filter;

	return(result);
}

/* --- function p [redshift distribution, Bartelmann+Schneider] --- */
double p_galaxy(double z)
{
	double norm,aux,result,prenorm;

	aux = z / gcosmo->zmean;
	
	norm = gcosmo->pgal_prenorm * gcosmo->zmean / gcosmo->beta ;
	result = gsl_pow_int(aux,2) * exp(-pow(aux,gcosmo->beta)) / norm;

	return(result);
}
/* --- end of function p --- */

/* --- Gaussian cut for galaxy weighting --- */
double spirou_cut(double chi, double mu, double DCHI)
{
	double norm,result;

	norm = 1.0 / sqrt(2*pi) * 1.0 / DCHI;
	
	result = norm * exp( -0.5 * gsl_pow_int( (chi - mu) / DCHI, 2) );

	return(result);
}
/* --- end of function p --- */



// ---  ---
double tomo_spectrum(struct SCONFIG *sconfig)
{
	double aux,error,result;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	if(tconf->mode==mode_victor)
	{
		tconf->whichBardeen = BardeenWeyl;
	}
	// carry out integration
	F.function = &aux_tomo_spectrum;
	F.params = sconfig;

	gsl_integration_qag(&F,CHIMIN,CHIMAX,eps,eps,NEVAL,GSL_INTEG_GAUSS15,w,&aux,&error);

	// convert with Laplace psi = 2 kappa, and psi = 2 int dchi Phi
	result = gsl_pow_int(sconfig->l,4) * aux;

	// write out spectrum of the lensing potential
	//result = aux;

	gsl_integration_workspace_free(w);
	return(result);
}
// ---  ---


// ---  ---
double aux_tomo_spectrum(double chi,void *param)
{
	double a,dp,wl,k,result;
	struct SCONFIG *sconfig;

	sconfig = (struct SCONFIG *) param;

	a = gsl_spline_eval(gcosmo->spline_a,chi,gcosmo->acc_a);
	dp = gsl_spline_eval(gcosmo->spline_dplus,a,gcosmo->acc_dplus);
	wl = tomo_efficiency(chi,a,dp,sconfig->a) * tomo_efficiency(chi,a,dp,sconfig->b);

	k = sconfig->l / chi;
	result = wl / gsl_pow_int(chi,2) * cdm_potential(k);

	// correct by nonlinear transfer function if needed
	if(tconf->nonlinearPk == on ) result *= cdm_nonlinear_transfer(k,a,dp);
	// correct by nonlinear in bias mode (for spirals)
	//else if(tconf->mode == mode_bias) result *= cdm_nonlinear_transfer(k,a,dp);


	return(result);
}
// ---  ---
// weak lensing correlation function
double gamma_lensing(double theta,int a,int b)
{
	double aux,error,result;
	struct SCONFIG *sconfig = (struct SCONFIG *)calloc(1,sizeof(struct SCONFIG));

	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	sconfig->a = a;
	sconfig->b = b;
	sconfig->theta = theta;

	F.function = &aux_dgamma;
	F.params = sconfig;

	gsl_integration_qag(&F,LMIN,LMAX_INT,1e-5,eps,NEVAL,GSL_INTEG_GAUSS31,w,&aux,&error);
	result = 2.0 * pi * aux;

	gsl_integration_workspace_free(w);
	free(sconfig);

	return(result);
}


// integrand
double aux_dgamma(double l,void *param)
{
	struct SCONFIG *sconfig = (struct SCONFIG *)param;
	double x,aux,result;

	sconfig->l = l;

	x = l * sconfig->theta;
	aux = gsl_sf_bessel_J1(x) / pi / x;

	result = l * tomo_spectrum(sconfig) * gsl_pow_int(aux,2);

	return(result);
}
// ---  ---
double kronecker(int a,int b)
{
	double result;

	if(a == b) result = 1.0;
	else result = 0.0;

	return(result);
}
// ---  ---

// ---  ---
double tomo_noise(double l,int a,int b)
{
	double noise,result;
	struct SCONFIG *sconfig = (struct SCONFIG *)calloc(1,sizeof(struct SCONFIG));

	noise = gcosmo->ellipticity / gcosmo->nmean * NBIN;

	sconfig->l = l;
	sconfig->a = a;
	sconfig->b = b;

	result = tomo_spectrum(sconfig) + noise * kronecker(a,b);

	free(sconfig);
	return(result);
}
// ---  ---


// --- adapter for tomo_spectrum ---
double tomo_kappa(double l,int a,int b)
{
	double result;
	struct SCONFIG *sconfig = (struct SCONFIG *)calloc(1,sizeof(struct SCONFIG));
	
	sconfig->l = l;
	sconfig->a = a;
	sconfig->b = b;

	result = tomo_spectrum(sconfig);

	free(sconfig);

	return(result);
}
// ---  ---
double tomo_kappa_wIAs(double l, int a, int b, struct DATA *data)
{
	double result;
	struct SCONFIG *sconfig = (struct SCONFIG *)calloc(1,sizeof(struct SCONFIG));
	int lint = (int) l;
	sconfig->l = l;
	sconfig->a = a;
	sconfig->b = b;


	result = tomo_spectrum(sconfig);
	if(tconf->mode==mode_derivs) 
		{
			if(a==b) result += data->ce[lint][a];
			if(tconf->flag_GIs==on) result += data->ce_gi[lint][a][b];
		}
	free(sconfig);

	return(result);

}


/* ---  --- */
double spirou_delta(gsl_vector *x,gsl_vector *y)
{
	double xx,yy,mu,result;

	xx = gsl_blas_dnrm2(x);
	yy = gsl_blas_dnrm2(y);

	// avoid cos(angle) close to -1.0, because of singularities
	gsl_blas_ddot(x,y,&mu);
	if(mu < -0.9) mu = -0.9;

	result = sqrt(gsl_pow_int(xx,2) + 2.0 * xx * yy * mu + gsl_pow_int(yy,2));

	return(result);
}
/* ---  --- */


// ---  ---
double tomo_efficiency(double chi,double a,double dp,int i)
{
	double result;

	result = tomo_weighting(chi,i) / chi * dp / a * NBIN;

	return(result);
}
// ---  ---


// ---  ---
double tomo_weighting(double chi,int i)
{
	double result,aux,min,error;
	

	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	if(chi < gdata->tomo[i+1])
	{
		F.function = &aux_weighting;
		F.params = &chi;

		min = GSL_MAX(chi,gdata->tomo[i]);
		// gsl_integration_qag(&F,tomo[i],tomo[i+1],eps,eps,NEVAL,GSL_INTEG_GAUSS15,w,&result,&error);
		gsl_integration_qag(&F,min,gdata->tomo[i+1],eps,eps,NEVAL,GSL_INTEG_GAUSS15,w,&result,&error);
	}
	else
	{
		result = 0.0;
	}

	gsl_integration_workspace_free(w);

	return(fabs(result));
}
// ---  ---


/* --- function aux_weighting [defines integrand of function weighting] --- */
double aux_weighting(double chiprime,void *params)
{
	double chi,z,a,dzdchi,result;

	chi = *(double *)params;
	a = gsl_spline_eval(gcosmo->spline_a,chiprime,gcosmo->acc_a);
	z = a2z(a);

	dzdchi = hubble(a) / (spirou_dhubble);
	result = p_galaxy(z) * dzdchi * (chiprime - chi) / chiprime;

	return(result);
}
/* --- end of function aux_weighting --- */

double COMPUTE_C_separation_rot(struct DATA *data, double alpha)
{
	int l;
	printf("tomo_ia: lensing: computing C_separation_rot....\n");

	#pragma omp parallel
	{
		int a,b;
		double aux;
	#pragma omp for
		for(l=LMIN;l<LMAX;l++)
		{
			for(a=0;a<NBIN2;a++)
			{
				for(b=a;b<NBIN2;b++)
				{
					data->C_sep_rot[l][a][b]=C_separation_adapter(data,l,a,b,alpha);
					data->C_sep_rot[l][b][a]=C_separation_adapter(data,l,b,a,alpha);
				}
			}

		}
	}	
	return(0);
}
double POPULATE_SEPARATION_NOISE(struct DATA *data)
{
	printf("tomo_ia: lensing: filling noise....\n");	
	int a,b,l;
	for(l=LMIN;l<LMAX;l++)
	{
		for(a=0;a<2*NBIN;a++)
		{
			for(b=0;b<2*NBIN;b++)
			{
				data->noise_sep[l][a][b]=N_separation_adapter(l,a,b,tugi_alpha);				
			}
		}
	}
	return(0);
}
double POPULATE_SEPARATION_SIGNAL(struct DATA *data)
{
	printf("tomo_ia: lensing: filling signal w/ IA....\n");	
	int a,b,l;
	for(l=LMIN;l<LMAX;l++)
	{
		for(a=0;a<2*NBIN;a++)
		{
			for(b=0;b<2*NBIN;b++)
			{
				data->signal_sep[l][a][b]=S_separation_adapter(data,l,a,b,tugi_alpha);				
			}
		}
	}
	return(0);
}
double C_separation_adapter(struct DATA *data, int l, int a, int b, double alpha)
{	
	return(S_separation_adapter(data,l,a,b,alpha) + N_separation_adapter(l,a,b,alpha));
}

double S_separation_adapter(struct DATA *data, int l, int a, int b, double alpha)
{
	double sina = sin(alpha);
	double cosa = cos(alpha);
	double B = Sbb_separation(data,l,a,b);
	double BR = Sbr_separation(data,l,a,b);
	double R = Srr_separation(data,l,a,b);

	if(a<NBIN && b<NBIN)
	{
		return (B*cosa*cosa + 2*BR*cosa*sina + R*sina*sina);
	}
	else if(a<NBIN && b>=NBIN)
	{
		return (BR*cosa*cosa + (R-B)*cosa*sina - BR*sina*sina);
	}
	else if(a>=NBIN && b<NBIN)
	{
		return (BR*cosa*cosa + (R-B)*cosa*sina - BR*sina*sina);
	}
	else if(a>=NBIN && b>=NBIN)
	{
		return (R*cosa*cosa - 2*BR*cosa*sina + B*sina*sina);
	}
	else return(0);
}

double N_separation_adapter(int l, int a, int b, double alpha)
{
	double sina = sin(alpha);
	double cosa = cos(alpha);
	double B = Nbb_separation(l,a,b);
	double R = Nrr_separation(l,a,b);

	if(a<NBIN && b<NBIN)
	{
		return (B*cosa*cosa + R*sina*sina);
	}
	else if(a<NBIN && b>=NBIN)
	{
		return ((R-B)*cosa*sina); //
	}
	else if(a>=NBIN && b<NBIN)
	{
		return ((R-B)*cosa*sina); //
	}
	else if(a>=NBIN && b>=NBIN)
	{
		return (R*cosa*cosa + B*sina*sina);
	}
	else return(0);	
}

double Sbb_separation(struct DATA *data, int l, int a, int b)
{
	double result;
	double Cg, Ce, Cs, Cgi;
	double nsq = gcosmo->nmean * gcosmo->nmean;
	
	Cg = tomo_kappa(l,a,b);
	Ce = 0;
	Cs = 0;
	Cgi = 2*gcosmo->sepIA_pbr * data->ce_gi[l][a][b]; 
	if (a==b)
	{
		Cs = gcosmo->sepIA_pbb * gcosmo->sepIA_pbb * data->ceS[l][a];
		Ce = gcosmo->sepIA_pbr * gcosmo->sepIA_pbr * data->ceE[l][a];
	}
	
	result = tugi_a * nsq * (Cg+Ce+Cs+Cgi);

	return(result);
}
double Sbr_separation(struct DATA *data, int l, int a, int b)
{
	double result;
	double Cg, Ce, Cs, Cgi;
	double nsq = gcosmo->nmean * gcosmo->nmean;
	
	Cg = tomo_kappa(l,a,b);
	Ce = 0;
	Cs = 0;
	Cgi = data->ce_gi[l][a][b]; 
	
	if (a==b)
	{
		Cs = gcosmo->sepIA_pbb*gcosmo->sepIA_prb * data->ceS[l][a];
		Ce = gcosmo->sepIA_pbr*gcosmo->sepIA_prr * data->ceE[l][a];
	}	
	result = tugi_b * nsq * (Cg+Ce+Cs+Cgi);
	
	return(result);
}
double Srr_separation(struct DATA *data, int l, int a, int b)
{
	double result;
	double Cg, Ce, Cs, Cgi;
	double nsq = gcosmo->nmean * gcosmo->nmean;
	
	Cg = tomo_kappa(l,a,b);
	Ce = 0;
	Cs = 0;
	Cgi = 2*gcosmo->sepIA_prr * data->ce_gi[l][a][b]; 
	if(a==b)
	{
		Cs = gcosmo->sepIA_prb*gcosmo->sepIA_prb * data->ceS[l][a];
		Ce = gcosmo->sepIA_prr*gcosmo->sepIA_prr * data->ceE[l][a];
	}
	result = tugi_c * nsq * (Cg+Ce+Cs+Cgi);
	
	return(result);
}

double Nbb_separation(int l, int a, int b)
{
	if (a!=b) return(0);
	double result;
	double nblue = gcosmo->nmean * ( tugi_q * gcosmo->sepIA_pbb + (1.-tugi_q) * gcosmo->sepIA_pbr);

	result = gcosmo->ellipticity * NBIN * nblue;

	return(result);
}
double Nrr_separation(int l, int a, int b)
{
	if (a!=b) return(0);
	double result;
	double nred = gcosmo->nmean * ( (1.-tugi_q) * gcosmo->sepIA_prr + (tugi_q) * gcosmo->sepIA_prb);

	result = gcosmo->ellipticity * NBIN * nred;

	return(result);
}


double CnoIA_separation_adapter(struct DATA *data, int l, int a, int b, double alpha)
{	
	return(SnoIA_separation_adapter(data,l,a,b,alpha)+N_separation_adapter(l,a,b,alpha));
}

double SnoIA_separation_adapter(struct DATA *data, int l, int a, int b, double alpha)
{
	double sina = sin(alpha);
	double cosa = cos(alpha);
	double B = SnoIAbb_separation(data,l,a,b);
	double BR = SnoIAbr_separation(data,l,a,b);
	double R = SnoIArr_separation(data,l,a,b);

	if(a<NBIN && b<NBIN)
	{
		return (B*cosa*cosa + 2*BR*cosa*sina + R*sina*sina);
	}
	else if(a<NBIN && b>=NBIN)
	{
		return (BR*cosa*cosa + (R-B)*cosa*sina - BR*sina*sina);
	}
	else if(a>=NBIN && b<NBIN)
	{
		return (BR*cosa*cosa + (R-B)*cosa*sina - BR*sina*sina);
	}
	else if(a>=NBIN && b>=NBIN)
	{
		return (R*cosa*cosa - 2*BR*cosa*sina + B*sina*sina);
	}
	else return(0);
}

double SnoIAbb_separation(struct DATA *data, int l, int a, int b)
{
	double result;
	double Cg, Ce, Cs, Cgi;
	double nsq = gcosmo->nmean * gcosmo->nmean;
	
	Cg = tomo_kappa(l,a,b);
	Ce = 0;
	Cs = 0;
	Cgi = 0;
	
	result = tugi_a * nsq * (Cg+Ce+Cs+Cgi);

	return(result);
}
double SnoIAbr_separation(struct DATA *data, int l, int a, int b)
{
	double result;
	double Cg, Ce, Cs, Cgi;
	double nsq = gcosmo->nmean * gcosmo->nmean;
	
	Cg = tomo_kappa(l,a,b);
	Ce = 0;
	Cs = 0;
	Cgi = 0;

	result = tugi_b * nsq * (Cg+Ce+Cs+Cgi);
	
	return(result);
}
double SnoIArr_separation(struct DATA *data, int l, int a, int b)
{
	double result;
	double Cg, Ce, Cs, Cgi;
	double nsq = gcosmo->nmean * gcosmo->nmean;
	
	Cg = tomo_kappa(l,a,b);
	Ce = 0;
	Cs = 0;
	Cgi = 0;

	result = tugi_c * nsq * (Cg+Ce+Cs+Cgi);
	
	return(result);
}

// ---  ---
double map_multipole(double xx,double lmax)
{
	double delta,result;

	delta = lmax - LMIN;
	result = delta * xx + LMIN;

	return(result);
}
// ---  ---


// ---  ---
double map_polar_angle(double xx)
{
	double result;

	result = 2.0 * pi * xx;

	return(result);
}
// ---  ---
