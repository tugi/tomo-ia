#include <tomo.h>
double tugi_integrate_cquad_k(int flag_function, double r, int n)
{
	double aux,error,result;
	size_t nevals = NEVAL;

	struct LPARAM lparam;
	lparam.r = r;
	lparam.n = n;

	gsl_integration_cquad_workspace *w = gsl_integration_cquad_workspace_alloc(NEVAL);
	gsl_function F;
	switch(flag_function)
	{
		case(flag_zeta):
			F.function = &aux_zeta_ext;
			F.params = &lparam;
		break;
	}
	
	gsl_integration_cquad(&F, KMIN_INT, KMAX_INT, epsabs, eps, w, &aux, &error, &nevals);
	gsl_integration_cquad_workspace_free(w);

	return(aux);
}
double aux_zeta_ext(double k, void *param)
{
	double x,r,result;
	struct LPARAM *lparam = (struct LPARAM *)param;
	int n;

	r = lparam->r;
	n = lparam->n;
	x = k * r;

	result = gsl_pow_int(x,n-2) * gsl_sf_bessel_jl(n,x) * cdm_smooth(k);
	return(result);
}

double tugi_integrate_cquad_lij(int flag_function, int l, int i, int j)
{
	double aux,error,result;
	size_t nevals = NEVAL;

	struct LPARAM lparam;
	lparam.l = l;
	lparam.i = i;
	lparam.j = j;

	gsl_integration_cquad_workspace *w = gsl_integration_cquad_workspace_alloc(NEVAL);
	gsl_function F;
	switch(flag_function)
	{
		case(flag_ii_emode):
			F.function = &aux_ii_demode;
			F.params = &l;	
		break;
		case(flag_ii_bmode):
			F.function = &aux_ii_dbmode;
			F.params = &l;	
		break;
		case(flag_ii_smode):
			F.function = &aux_ii_dsmode;
			F.params = &l;	
		break;
		case(flag_ii_cmode):
			F.function = &aux_ii_dcmode;
			F.params = &l;	
		break;
		case(flag_gi_emode):
			F.function = &aux_gi_demode;
			F.params = &lparam;	
		break;
		case(flag_gi_bmode):
			F.function = &aux_gi_dbmode;
			F.params = &lparam;	
		break;
		case(flag_gi_smode):
			F.function = &aux_gi_dsmode;
			F.params = &lparam;	
		break;
		case(flag_gi_cmode):
			F.function = &aux_gi_dcmode;
			F.params = &lparam;	
		break;
	}




	gsl_integration_cquad(&F, TMIN_INT, TMAX_INT, epsabs, eps, w, &aux, &error, &nevals);
	gsl_integration_cquad_workspace_free(w);

	return(aux);
}
double tugi_integrate_cquad_tij(int flag_function, double theta, int i, int j)
{
	double aux, error, result, lower, upper;
	size_t nevals = NEVAL;

	struct LPARAM lparam;
	lparam.theta = theta;
	lparam.i = i;
	lparam.j = j;

	gsl_integration_cquad_workspace *w = gsl_integration_cquad_workspace_alloc(NEVAL);
	gsl_function F;
	switch(flag_function)
	{
		case(flag_ii_pp):
			lparam.index = index_pp;
			F.function = &aux_correlation_ii_dchi;
			F.params = &lparam;
			lower = GSL_MAX(gdata->tomo[i],CHIMIN);
			upper = gdata->tomo[i+1];
		break;
		case(flag_ii_cc):
			lparam.index = index_cc;
			F.function = &aux_correlation_ii_dchi;
			F.params = &lparam;
			lower = GSL_MAX(gdata->tomo[i],CHIMIN);
			upper = gdata->tomo[i+1];
		break;
		case(flag_ii_sp):
			lparam.index = index_sp;
			F.function = &aux_correlation_ii_dchi;
			F.params = &lparam;
			lower = GSL_MAX(gdata->tomo[i],CHIMIN);
			upper = gdata->tomo[i+1];
		break;
		case(flag_ii_ss):
			lparam.index = index_ss;
			F.function = &aux_correlation_ii_dchi;
			F.params = &lparam;
			lower = GSL_MAX(gdata->tomo[i],CHIMIN);
			upper = gdata->tomo[i+1];
		break;
		case(flag_gi_pp):
			lparam.index = index_pp;
			F.function = &aux_correlation_gi_dchi;
			F.params = &lparam;	
			lower = CHIMIN;
			upper = CHIMAX;
		break;
		case(flag_gi_cc):
			lparam.index = index_cc;
			F.function = &aux_correlation_gi_dchi;
			F.params = &lparam;	
			lower = CHIMIN;
			upper = CHIMAX;
		break;
		case(flag_gi_sp):
			lparam.index = index_sp;
			F.function = &aux_correlation_gi_dchi;
			F.params = &lparam;	
			lower = CHIMIN;
			upper = CHIMAX;	
		break;
		case(flag_gi_ss):
			lparam.index = index_ss;
			F.function = &aux_correlation_gi_dchi;
			F.params = &lparam;	
			lower = CHIMIN;
			upper = CHIMAX;	
		break;
	}

	gsl_integration_cquad(&F,lower,upper,epsabs,eps,w,&aux, &error, &nevals);
	gsl_integration_cquad_workspace_free(w);

	return(aux);
}
/* --- function ellipticity_emode [angular ellipticity spectrum, e-mode, factor nbin^2 for normalised p_i(z)] --- */
double ii_emode(int l,int i)
{
	double aux, result;
	
	aux = tugi_integrate_cquad_lij(flag_ii_emode,l,i,0);

	result = pi * aux * gsl_pow_int(NBIN,2);

	return(result);
}
/* ---  --- */


/* --- function ellipticity-bmode [angular ellipticity spectrum, b-mode, factor nbin^2 for normalised p_i(z)] --- */
double ii_bmode(int l,int i)
{
	double aux, result;
	
	aux = tugi_integrate_cquad_lij(flag_ii_bmode,l,i,0);

	result = pi * aux * gsl_pow_int(NBIN,2);

	return(result);
}
/* ---  --- */


/* --- function aux_demode [dtheta-integration for ellipticity_emode] --- */
double aux_ii_demode(double theta,void *param)
{
	double aux,result;
	double x,pp,cc;
	int l = *(int *)param;

	x = l * theta;
	pp = gsl_spline_eval(gcosmo->spline_pp,theta,gcosmo->acc_pp);
	cc = gsl_spline_eval(gcosmo->spline_cc,theta,gcosmo->acc_cc);

	aux = (pp + cc) * gsl_sf_bessel_Jn(0,x) + (pp - cc) * gsl_sf_bessel_Jn(4,x);
	result = theta * aux;

	return(result);
}
/* ---  --- */


/* --- function aux_dbmode [dtheta-integration for ellipticity_bmode] --- */
double aux_ii_dbmode(double theta,void *param)
{
	double aux,result;
	double x,pp,cc;
	int l = *(int *)param;

	x = l * theta;

	pp = gsl_spline_eval(gcosmo->spline_pp,theta,gcosmo->acc_pp);
	cc = gsl_spline_eval(gcosmo->spline_cc,theta,gcosmo->acc_cc);

	aux = (pp + cc) * gsl_sf_bessel_Jn(0,x) - (pp - cc) * gsl_sf_bessel_Jn(4,x);
	result = theta * aux;

	return(result);
}
/* ---  --- */


// ---  ---
double ii_smode(int l,int i)
{
	double aux,result;
	aux = tugi_integrate_cquad_lij(flag_ii_smode,l,i,0);
	result = pi * aux * gsl_pow_int(NBIN,2);

	return(result);
}
// ---  ---


// ---  ---
double ii_cmode(int l,int i)
{
	double aux,result;
	aux = tugi_integrate_cquad_lij(flag_ii_cmode,l,i,0);
	result = pi * aux * gsl_pow_int(NBIN,2);

	return(result);
}
// ---  ---


// ---  ---
double aux_ii_dsmode(double theta,void *param)
{
	double aux,result;
	double x,ss;
	int l = *(int *)param;

	x = l * theta;

	ss = gsl_spline_eval(gcosmo->spline_ss,theta,gcosmo->acc_ss);

	aux = ss * gsl_sf_bessel_Jn(0,x);
	result = theta * aux;

	return(result);
}
// ---  ---


// ---  ---
double aux_ii_dcmode(double theta,void *param)
{
	double aux,result;
	double x,sp;
	int l = *(int *)param;

	x = l * theta;

	sp = gsl_spline_eval(gcosmo->spline_sp,theta,gcosmo->acc_sp);

	aux = sp * gsl_sf_bessel_Jn(2,x);
	result = theta * aux;

	return(result);
}
// ---  ---

// GIs
double gi_emode(int l,int i, int j)
{
	double aux,result;
	aux = tugi_integrate_cquad_lij(flag_gi_emode,l,i,j);
	result = pi * aux * gsl_pow_int(NBIN,2);

	return(result);
}
/* ---  --- */


/* --- function ellipticity-bmode [angular ellipticity spectrum, b-mode, factor nbin^2 for normalised p_i(z)] --- */
double gi_bmode(int l,int i, int j)
{
	double aux,result;
	aux = tugi_integrate_cquad_lij(flag_gi_bmode,l,i,j);
	result = pi * aux * gsl_pow_int(NBIN,2);

	return(result);
}
/* ---  --- */


/* --- function aux_demode [dtheta-integration for ellipticity_emode] --- */
double aux_gi_demode(double theta,void *param)
{
	double aux,result;
	double x,pp,cc;		
	
	struct LPARAM *lparam = (struct LPARAM *)param;
	x = lparam->l * theta;

	pp = gsl_spline_eval(gcosmo->spline_gi_pp[lparam->j],theta,gcosmo->acc_gi_pp[lparam->j]);
	cc = gsl_spline_eval(gcosmo->spline_gi_cc[lparam->j],theta,gcosmo->acc_gi_cc[lparam->j]);

	aux = (pp + cc) * gsl_sf_bessel_Jn(0,x) + (pp - cc) * gsl_sf_bessel_Jn(4,x);
	result = theta * aux;

	return(result);
}
/* ---  --- */


/* --- function aux_dbmode [dtheta-integration for ellipticity_bmode] --- */
double aux_gi_dbmode(double theta,void *param)
{
	double aux,result;
	double x,pp,cc;

	struct LPARAM *lparam = (struct LPARAM *)param;
	x = lparam->l * theta;

	pp = gsl_spline_eval(gcosmo->spline_gi_pp[lparam->j],theta,gcosmo->acc_gi_pp[lparam->j]);
	cc = gsl_spline_eval(gcosmo->spline_gi_cc[lparam->j],theta,gcosmo->acc_gi_cc[lparam->j]);

	aux = (pp + cc) * gsl_sf_bessel_Jn(0,x) - (pp - cc) * gsl_sf_bessel_Jn(4,x);
	result = theta * aux;

	return(result);
}
/* ---  --- */
// ---  ---
double gi_smode(int l,int i, int j)
{
	double aux,result;
	aux = tugi_integrate_cquad_lij(flag_gi_smode,l,i,j);
	result = pi * aux * gsl_pow_int(NBIN,2);

	return(result);
}
// ---  ---


// ---  ---
double gi_cmode(int l,int i, int j)
{
	double aux,result;
	aux = tugi_integrate_cquad_lij(flag_gi_cmode,l,i,j);
	result = pi * aux * gsl_pow_int(NBIN,2);

	return(result);
}
// ---  ---


// ---  ---
double aux_gi_dsmode(double theta,void *param)
{
	double aux,result;
	double x,ss;

	struct LPARAM *lparam = (struct LPARAM *)param;
	x = lparam->l * theta;

	ss = gsl_spline_eval(gcosmo->spline_gi_ss[lparam->j],theta,gcosmo->acc_gi_ss[lparam->j]);

	aux = ss * gsl_sf_bessel_Jn(0,x);
	result = theta * aux;

	return(result);
}
// ---  ---


// ---  ---
double aux_gi_dcmode(double theta,void *param)
{
	double aux,result;
	double x,sp;

	struct LPARAM *lparam = (struct LPARAM *)param;
	x = lparam->l * theta;

	sp = gsl_spline_eval(gcosmo->spline_gi_sp[lparam->j],theta,gcosmo->acc_gi_sp[lparam->j]);

	aux = sp * gsl_sf_bessel_Jn(2,x);
	result = theta * aux;

	return(result);
}
// ---  ---


/* --- function correlation_pp [angular correlation function <e+e+>, uncorrected for nbin] --- */
double correlation_ii_pp(double theta,int i)
{
	double result;
	
	result = tugi_integrate_cquad_tij(flag_ii_pp, theta, i, 0);
	
	if(tconf->morphology==spiral)
	{
		result *= gsl_pow_int(spirou_alpha,2);	
	}
	
	return(result);
}
/* ---  --- */


/* --- function correlation_cc [angular correlation function <exex>, uncorrected for nbin] --- */
double correlation_ii_cc(double theta, int i)
{
	double result;

	result = tugi_integrate_cquad_tij(flag_ii_cc, theta, i, 0);

	if(tconf->morphology==spiral)
	{
		result *= gsl_pow_int(spirou_alpha,2);	
	}

	return(result);
}
/* ---  --- */


/* --- function aux_correlation_dchi [dchi-integration for correlation_pp and correlation_cc] --- */
double aux_correlation_ii_dchi(double chi,void *param)
{
	double aux,error,result;
	double a,z,dzdchi,fraction;
	double lower, upper;
	size_t nevals = NEVAL;

	struct LPARAM *lparam = (struct LPARAM *)param;

	gsl_integration_cquad_workspace *w = gsl_integration_cquad_workspace_alloc(NEVAL);
	gsl_function F;

	lparam->chi = chi;

	switch(tconf->morphology)
	{
		case ellipse:
			fraction = (1.0-tugi_q);
			lower = GSL_MAX(gdata->tomo[lparam->i],CHIMIN);
			upper = gdata->tomo[lparam->i+1];
			// lower = GSL_MAX(chi - DCHI_ellipticals, CHIMIN);
			// upper = chi + DCHI_ellipticals;
			break;

		case spiral:
			fraction = tugi_q;
			lower = chi-DCHI_spirals;
			upper = chi+DCHI_spirals;
			break;
	}
	F.function = &aux_correlation_ii_dchiprime;
	F.params = lparam;

	gsl_integration_cquad(&F,lower,upper,epsabs,eps,w,&aux, &error, &nevals);

	a = gsl_spline_eval(gcosmo->spline_a,chi,gcosmo->acc_a);
	z = a2z(a);
	dzdchi = hubble(a) / (spirou_dhubble);

	result = fraction * aux * p_galaxy(z) * dzdchi;

	gsl_integration_cquad_workspace_free(w);

	return(result);
}
/* ---  --- */


/* --- function aux_correlation_dchiprime [dchiprime-integration for aux_correlation_dchi] --- */
double aux_correlation_ii_dchiprime(double chiprime,void *param)
{
	double aux,result;
	double a,z,dzdchi;
	double r,alpha;
	double delta_chi,sum_chi;
	struct LPARAM *lparam = (struct LPARAM *)param;
	double chi, theta;
	double deltaCHI = 2*DCHI_ellipticals;

	chi = lparam->chi;
	theta = lparam->theta;

	// chi, chiprime --> alpha, r
	delta_chi = lparam->chi - chiprime;
	sum_chi = lparam->theta * (lparam->chi + chiprime) / 2.0;

	// r = gsl_pow_int(chi,2) + gsl_pow_int(chiprime,2) - 2*chi*chiprime*cos(theta);
	// r = sqrt(r);

	// alpha = asin( (chi+chiprime)/r * sin(theta/2.0));

	alpha = pi / 2.0 - atan2(delta_chi,sum_chi);
	r = gsl_pow_int(delta_chi,2) + gsl_pow_int(sum_chi,2);
	r = sqrt(r);

	// catch r out of range:
	if((r > CMAX) || (r < CMIN)) return(0.0);

	// evaluate 3d correlation functions
	switch(lparam->index)
	{
		case index_pp:
			aux = ellipticity_pp(r,alpha);
			break;

		case index_cc:
			aux = ellipticity_cc(r,alpha);
			break;

		case index_sp:
			aux = ellipticity_sp(r,alpha);
			break;

		case index_ss:
			aux = ellipticity_ss(r,alpha);
			break;
	}

	// weight with galaxy clustering
	a = gsl_spline_eval(gcosmo->spline_a,chiprime,gcosmo->acc_a);
	z = a2z(a);
	dzdchi = hubble(a) / (spirou_dhubble);
	if(tconf->mode==mode_gravslip) aux *= gsl_pow_int(gcosmo->tugi_mu,2);
	result = aux * p_galaxy(z) * dzdchi;

	switch(tconf->morphology)
	{
		case ellipse:
			result *= (1.0 - tugi_q);// * spirou_cut(chiprime, chi, deltaCHI);
			break;
		case spiral:
			result *= tugi_q;
			break;
	}
	return(result);
}
/* ---  --- */

// angular correlation function <esep>
double correlation_ii_sp(double theta,int i)
{
	double aux, result;

	aux = tugi_integrate_cquad_tij(flag_ii_sp, theta, i, 0);

	result = 3.0 / 4.0 * aux / sqrt(2.0);
	if(tconf->morphology==spiral)
		{
			result *= gsl_pow_int(spirou_alpha,2);	
		}
	return(result);
}


// angular correlation function <eses>
double correlation_ii_ss(double theta,int i)
{
	double aux, result;

	aux = tugi_integrate_cquad_tij(flag_ii_ss,theta,i,0);
	result = gsl_pow_int(3.0 / 4.0 , 2) * aux / 2.0;
	if(tconf->morphology==spiral)
	{
		result *= gsl_pow_int(spirou_alpha,2);	
	}

	return(result);
}


// GIs
/* --- function correlation_pp [angular correlation function <e+e+>, uncorrected for nbin] --- */
double correlation_gi_pp(double theta,int i,int j)
{
	double result;
	result = tugi_integrate_cquad_tij(flag_gi_pp, theta, i, j);
	return(result);
}
/* ---  --- */


/* --- function correlation_cc [angular correlation function <exex>, uncorrected for nbin] --- */
double correlation_gi_cc(double theta,int i,int j)
{
	double result;
	result = tugi_integrate_cquad_tij(flag_gi_cc, theta, i, j);
	return(result);
}
/* ---  --- */



/* --- function aux_correlation_dchi [dchi-integration for correlation_pp and correlation_cc] --- */
double aux_correlation_gi_dchi(double chi,void *param)
{
	double aux, error, result;
	double wl, a, z, dp, dzdchi;
	int i, j;

	struct LPARAM *lparam = (struct LPARAM *)param;

	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	lparam->chi = chi;
	
	// i < j --> j for lensing, i for IA
	j = lparam->j;		
	i = lparam->i;

	// Weak Lensing Weighting
	a = gsl_spline_eval(gcosmo->spline_a,chi,gcosmo->acc_a);
	dp = gsl_spline_eval(gcosmo->spline_dplus,a,gcosmo->acc_dplus);		
	wl = tomo_weighting(chi,j) * chi * dp / a;

	// Integrate for IA
	F.function = &aux_correlation_gi_dchiprime;
	F.params = lparam;
	gsl_integration_qag(&F,GSL_MAX(gdata->tomo[i],CHIMIN),gdata->tomo[i+1],eps,eps,NEVAL,GSL_INTEG_GAUSS61,w,&aux,&error);

	result = wl * aux;
	
	gsl_integration_workspace_free(w);

	return(result);
}
/* ---  --- */


/* --- function aux_correlation_dchiprime [dchiprime-integration for aux_correlation_dchi] --- */
double aux_correlation_gi_dchiprime(double chiprime,void *param)
{
	double aux,result;
	double a,z,dzdchi;
	double r,alpha;
	double delta_chi,sum_chi;
	struct LPARAM *lparam = (struct LPARAM *)param;
	double chi, theta;

	chi = lparam->chi;
	theta = lparam->theta;

	// chi, chiprime --> alpha, r
	delta_chi = lparam->chi - chiprime;
	sum_chi = lparam->theta * (lparam->chi + chiprime) / 2.0;

	// r = gsl_pow_int(chi,2) + gsl_pow_int(chiprime,2) - 2*chi*chiprime*cos(theta);
	// r = sqrt(r);
	r = gsl_pow_int(delta_chi,2) + gsl_pow_int(sum_chi,2);
	r = sqrt(r);

	// alpha = asin( (chi+chiprime)/r * sin(theta/2.0));
	alpha = pi / 2.0 - atan2(delta_chi,sum_chi);

	// catch r & alpha out of range:
	if((r > CMAX) || (r < CMIN)) return(0.0);
	//if((alpha > alphaMAX) || (alpha < alphaMIN)) return(0.0);

	// evaluate 3d correlation functions ** HERE ARE THE ANTICORR. TAKEN CARE OF **
	
	switch(lparam->index)
	{
		case index_pp:
			aux = -1.0 * ellipticity_gi_pp(r,alpha);
			break;

		case index_cc:
			aux = ellipticity_gi_cc(r,alpha);
			break;

		case index_sp:
			aux = -1.0 * ellipticity_gi_sp(r,alpha);
			break;

		case index_ss:
			aux = ellipticity_gi_ss(r,alpha);
			break;
	}

	a = gsl_spline_eval(gcosmo->spline_a,chiprime,gcosmo->acc_a);
	z = a2z(a);
	dzdchi = hubble(a) / (spirou_dhubble);
	if(tconf->mode==mode_gravslip) aux *= ( gcosmo->tugi_mu*gcosmo->tugi_mu*(gcosmo->tugi_eta+1.) );

	result = 0.5 * aux / gcosmo->tugi_hooke * p_galaxy(z) * dzdchi;
	//  factor 1/2 from symmetrisation, divide by hooke because it's in ellipticity as a square.
	
	switch(tconf->morphology)
	{
		case ellipse:
			result *= (1-tugi_q);
			break;
		case spiral:
			result *= tugi_q;
			break;
	}	return(result);
}
/* ---  --- */
double correlation_gi_ss(double theta,int i,int j)
{
	double result;
	result = tugi_integrate_cquad_tij(flag_gi_ss, theta, i, j);
	return(result);
}
/* ---  --- */
double correlation_gi_sp(double theta,int i,int j)
{
	double result;
	result = tugi_integrate_cquad_tij(flag_gi_sp, theta, i, j);
	return(result);
}
/* ---  --- */


int COMPUTE_ELLIPTICITY_SPECTRA(struct DATA *data)
{
	int i;
	int l;
	printf("tomo_ia: correlation: computing tomographic ellipticity spectra (e- and b-mode)....\n");

	for(i=0;i<NBIN;i++)
	{
		// counter
		printf("tomo_ia: correlation: bin number %i/%i....\n",i+1,NBIN);

		COMPUTE_ANGULAR_CORRELATIONS(data,i);

		WRITE_ANGULAR_II_ELLIPTICITY2DISK(data,i);
		if(tconf->flag_GIs==on)	WRITE_ANGULAR_GI_ELLIPTICITY2DISK(data,i);
		
		// compute spectra
		printf("tomo_ia: correlation: computing ellipticity spectra....\n");

#pragma omp parallel for
		for(l=LMIN;l<LMAX;l++)
		{
			int j;

				data->ce[l][i] = ii_emode(l,i);
				if(data->ce[l][i] < 0.0) data->ce[l][i] = 0.0;

				if(tconf->mode!=mode_gravslip) //save b mode in case of gravslip
				{
					data->cb[l][i] = ii_bmode(l,i);
					if(data->cb[l][i] < 0.0) data->cb[l][i] = 0.0;
				}
				if(tconf->flag_GIs==on)
				{
					for(j=i;j<NBIN;j++)
					{
					data->ce_gi[l][i][j] = data->ce_gi[l][j][i] = gi_emode(l,i,j);
					// if(data->ce_gi[l][i][j] > 0.0) data->ce_gi[l][i][j] = data->ce_gi[l][j][i] = 0.0;

					if(tconf->mode!=mode_gravslip)
						{
						data->cb_gi[l][i][j] = data->cb_gi[l][j][i] = gi_bmode(l,i,j);
						//if(data->cb_gi[l][i][j] > 0.0) data->cb_gi[l][i][j] = data->cb_gi[l][j][i] = -data->cb_gi[l][j][i];
						}
					}
				}// morphology-if

		} // l-loop (multipoles), parallel
	} // i-loop (bins)

	return(0);
}
// ---  ---

int COMPUTE_ZETAS_AND_ELL_SPECTRA(struct DATA *data)
{
	if(tconf->morphology == both) 
		{
			COMPUTE_BOTH_ELLIPTICITY_SPECTRA(data);
		}
	else 
		{
			COMPUTE_ZETA_COEFFICIENTS(data); // II
			COMPUTE_ZETAPRIME_COEFFICIENTS(data); // GI
			COMPUTE_ELLIPTICITY_SPECTRA(data);
		}
	return(0);
}

// this computes both ellipticty e-modes (spiral and elliptical) and stores it in ce[]
int COMPUTE_BOTH_ELLIPTICITY_SPECTRA(struct DATA *data)
{
	int i;
	int l;
	printf("tomo_ia: correlation: computing **both** tomographic ellipticity spectra (e- and b-mode)....\n");

	tconf->morphology = spiral;
	tconf->flag_GIs = off;
	gcosmo->rsmoothscale = gcosmo->rsmoothscale_spiral;

	COMPUTE_ZETA_COEFFICIENTS(data);
	printf("tomo_ia: correlation: spirals ... \n");
	for(i=0;i<NBIN;i++)
	{
		// counter
		printf("tomo_ia: correlation: bin number %i/%i....\n",i+1,NBIN);

		COMPUTE_ANGULAR_CORRELATIONS(data,i);

		WRITE_ANGULAR_II_ELLIPTICITY2DISK(data,i);

		// compute spectra
		printf("tomo_ia: correlation: computing spiral ellipticity spectra....\n");

#pragma omp parallel for
		for(l=LMIN;l<LMAX;l++)
		{
			double aux;
				aux = ii_emode(l,i);
				if(aux < 0.0) aux = 0.0;
				if(tconf->flag_separation == on)
				{
					data->ceS[l][i] = aux;
				}
				else 
				{
					data->ce[l][i] = aux;
				}
				
				if(tconf->flag_separation == on)
				{
					data->cbS[l][i] = ii_bmode(l,i);
				}
				else 
				{
					data->cb[l][i] = ii_bmode(l,i);
					if(data->cb[l][i] < 0.0) data->cb[l][i] = 0.0;
				}
				
		} // l-loop (multipoles), parallel
	} // i-loop (bins)

	tconf->morphology = ellipse;
	tconf->flag_GIs = on;
	gcosmo->rsmoothscale = gcosmo->rsmoothscale_ellipse;		
	printf("tomo_ia: correlation: ellipticals ... \n");
	COMPUTE_ZETA_COEFFICIENTS(data);
	COMPUTE_ZETAPRIME_COEFFICIENTS(data);
	for(i=0;i<NBIN;i++)
	{
		// counter
		printf("tomo_ia: correlation: bin number %i/%i....\n",i+1,NBIN);

		COMPUTE_ANGULAR_CORRELATIONS(data,i);

		WRITE_ANGULAR_II_ELLIPTICITY2DISK(data,i);
		WRITE_ANGULAR_GI_ELLIPTICITY2DISK(data,i);

		// compute spectra
		printf("tomo_ia: correlation: computing ellipse ellipticity spectra....\n");

#pragma omp parallel for
		for(l=LMIN;l<LMAX;l++)
		{
			int j;
			double aux;
				aux = ii_emode(l,i);
				if(aux < 0.0) aux = 0.0;
				if(tconf->flag_separation == on)
				{
					data->ceE[l][i] = aux;	
				}
				else 
				{
					data->ce[l][i] += aux;
				}

				if(tconf->flag_separation == on)
				{
					data->cbE[l][i] = 0.0;
				}
				else 
				{
					data->cb[l][i] = ii_bmode(l,i);
					if(data->cb[l][i] < 0.0) data->cb[l][i] += 0.0;
				}
				
				for(j=i;j<NBIN;j++)
				{

					data->ce_gi[l][i][j] = data->ce_gi[l][j][i] = gi_emode(l,i,j);
					// if(data->ce_gi[l][i][j] > 0.0) data->ce_gi[l][i][j] = data->ce_gi[l][j][i] = 0.0;
					if(tconf->flag_separation == off)
					{
						data->cb_gi[l][i][j] = data->cb_gi[l][j][i] = gi_bmode(l,i,j);
						//if(data->cb_gi[l][i][j] > 0.0) data->cb_gi[l][i][j] = data->cb_gi[l][j][i] = -data->cb_gi[l][j][i];
					}	
					// if(data->cb_gi[l][i][j] > 0.0) data->cb_gi[l][i][j] = data->cb_gi[l][j][i] = 0.0;
				}
			
		} // l-loop (multipoles), parallel
	} // i-loop (bins)
	tconf->morphology = both;
	tconf->flag_GIs = on;

	return(0);
}
// ---  ---

// ---  ---
int COMPUTE_ELLIPTICITY_CMODE(struct DATA *data)
{
	int i;
	int l;

	printf("tomo_ia: correlation: computing tomographic ellipticity spectra (scalar and cross)....\n");

	for(i=0;i<NBIN;i++)
	{
		// counter
		printf("tomo_ia: correlation: bin number %i/%i....\n",i+1,NBIN);

		COMPUTE_ANGULAR_CORRELATIONS(data,i);

		// compute spectra
		printf("tomo_ia: correlation: computing ellipticity spectra....\n");

#pragma omp parallel for
		for(l=LMIN;l<LMAX;l++)
		{
			int j;
				data->cs[l][i] = ii_smode(l,i);
				// if(data->cs[l][i] < 0.0) data->cs[l][i] = 0.0;
				data->cc[l][i] = ii_cmode(l,i);
				// if(data->cc[l][i] < 0.0) data->cc[l][i] = 0.0;
				
				if(tconf->flag_GIs==on)
				{
					for(j=i; j<NBIN; j++)
					{
					data->cs_gi[l][i][j] = data->cs_gi[l][j][i] = gi_smode(l,i,j);
					//if(data->cs_gi[l][i][j] < 0.0) data->cs_gi[l][i][j] = 0.0;
					data->cc_gi[l][i][j] = data->cc_gi[l][j][i] = gi_cmode(l,i,j);
					//if(data->cc_gi[l][i][j] < 0.0) data->cc_gi[l][i][j] = 0.0;
					}
				}

		} // l-loop (multipoles), parallel
	} // i-loop (bins)

	return(0);
}
// ---  ---



/* ---  --- */
int COMPUTE_ANGULAR_CORRELATIONS(struct DATA *data, int i)
{
	int t;

	printf("tomo_ia: correlation: computing angular correlation functions for a specific bin....");

	gcosmo->acc_pp = gsl_interp_accel_alloc();
	gcosmo->spline_pp = gsl_spline_alloc(gsl_interp_steffen,TSTEP);
	gcosmo->acc_cc = gsl_interp_accel_alloc();
	gcosmo->spline_cc = gsl_spline_alloc(gsl_interp_steffen,TSTEP);


	if(tconf->flag_crossspectra==on)
	{
		gcosmo->acc_sp = gsl_interp_accel_alloc();
		gcosmo->spline_sp = gsl_spline_alloc(gsl_interp_steffen,TSTEP);
		gcosmo->acc_ss = gsl_interp_accel_alloc();
		gcosmo->spline_ss = gsl_spline_alloc(gsl_interp_steffen,TSTEP);
	}

	if(tconf->flag_GIs==on)
	{
		int j;
		for(j=0;j<NBIN;j++)
		{
		gcosmo->acc_gi_pp[j] = gsl_interp_accel_alloc();
		gcosmo->spline_gi_pp[j] = gsl_spline_alloc(gsl_interp_steffen,TSTEP);
		gcosmo->acc_gi_cc[j] = gsl_interp_accel_alloc();
		gcosmo->spline_gi_cc[j] = gsl_spline_alloc(gsl_interp_steffen,TSTEP);
			if(tconf->flag_crossspectra==on)
			{
				gcosmo->acc_gi_sp[j] = gsl_interp_accel_alloc();
				gcosmo->spline_gi_sp[j] = gsl_spline_alloc(gsl_interp_steffen,TSTEP);
				gcosmo->acc_gi_ss[j] = gsl_interp_accel_alloc();
				gcosmo->spline_gi_ss[j] = gsl_spline_alloc(gsl_interp_steffen,TSTEP);
			}
		}
	}
#pragma omp parallel
	{
		double theta;
		int j;
#pragma omp for
		for(t=0;t<TSTEP;t++)
		{
			
			theta = data->theta[t] = tstep(t);

			data->epsilon_pp[t] = correlation_ii_pp(theta,i);
			data->epsilon_cc[t] = correlation_ii_cc(theta,i);
			if(tconf->flag_crossspectra==on)
			{
				data->epsilon_sp[t] = correlation_ii_sp(theta,i);
				data->epsilon_ss[t] = correlation_ii_ss(theta,i);
			}
		if(tconf->flag_GIs==on)
		{
			for(j=0;j<NBIN;j++)
			{
				data->epsilon_gi_pp[j][t] = correlation_gi_pp(theta,i,j);
				data->epsilon_gi_cc[j][t] = correlation_gi_cc(theta,i,j);
				if(tconf->flag_crossspectra==on)
				{
					data->epsilon_gi_ss[j][t] = correlation_gi_ss(theta,i,j);
					data->epsilon_gi_sp[j][t] = correlation_gi_sp(theta,i,j);
				}
			}//for
		} // GI-if

		} // for theta
	} // pragma

	printf("done!\n");
//II
	gsl_spline_init(gcosmo->spline_pp, data->theta, data->epsilon_pp, TSTEP);
	gsl_spline_init(gcosmo->spline_cc, data->theta, data->epsilon_cc, TSTEP);

	if(tconf->flag_crossspectra==on)
	{
		gsl_spline_init(gcosmo->spline_sp, data->theta, data->epsilon_sp, TSTEP);
		gsl_spline_init(gcosmo->spline_ss, data->theta, data->epsilon_ss, TSTEP);
	}
//GI
	if(tconf->flag_GIs==on)
	{
		int j;
		for(j=0;j<NBIN;j++)
		{
		gsl_spline_init(gcosmo->spline_gi_pp[j], data->theta, data->epsilon_gi_pp[j], TSTEP);
		gsl_spline_init(gcosmo->spline_gi_cc[j], data->theta, data->epsilon_gi_cc[j], TSTEP);

		if(tconf->flag_crossspectra==on)
			{
				gsl_spline_init(gcosmo->spline_gi_ss[j], data->theta, data->epsilon_gi_ss[j], TSTEP);
				gsl_spline_init(gcosmo->spline_gi_sp[j], data->theta, data->epsilon_gi_sp[j], TSTEP);
			}
		}
	}

	return(0);
}
/* ---  --- */

