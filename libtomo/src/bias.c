#include <tomo.h>

/* --- function spirou_trace --- */
double spirou_trace(gsl_matrix *matrix)
{
	double result;
	int i;

	result = 0.0;
	for(i=0;i<matrix->size1;i++)
	{
		result += gsl_matrix_get(matrix,i,i);
	}

	return(result);
}
/* --- end of function spirou_trace --- */

// ---  ---
int COMPUTE_FIDUCIAL_SPECTRUM(struct DATA *data)
{
	int l;

	printf("tomo_ia: bias: computing fiducial weak lensing spectra for the covariance matrix....\n");

#pragma omp parallel
	{
		int a,b;

#pragma omp for
		for(l=LMIN;l<LMAX;l++)
		{
			for(a=0;a<NBIN;a++)
			{
				for(b=a;b<NBIN;b++)
				{
					if(tconf->mode==mode_gravslip) 
						{
							if((l==LMIN)&&(a==0)&&(b==0)) printf("tomo_ia: bias: Mode Gravslip: computing tomo_kappa+IA....\n");
							if(a==b) data->cv[l][a][b] = data->cv[l][b][a] = tomo_kappa(l,a,b)+data->ce[l][a]+data->ce_gi[l][a][b];
							else data->cv[l][a][b] = data->cv[l][b][a] = tomo_kappa(l,a,b)+data->ce_gi[l][a][b];
						}

					else if(tconf->mode==mode_derivs) 
						{
							if((l==LMIN)&&(a==0)&&(b==0)) printf("tomo_ia: bias: Mode Derivatives: computing tomo_kappa_wIAs....\n");
							data->cv[l][a][b] = data->cv[l][b][a] = tomo_kappa_wIAs(l,a,b,data);
						}
					else if(tconf->mode==mode_sep_biases) 
						{
							if((l==LMIN)&&(a==0)&&(b==0)) printf("tomo_ia: bias: Mode Sep Biases: computing SnoIA_separation_adapter....\n");
							data->cv[l][a][b] = data->cv[l][b][a] = 
								gcosmo->nmean * gcosmo->nmean * tomo_kappa(l,a,b) * 
								((tugi_q) * cos(tugi_alpha) + (1.0-tugi_q) * sin(tugi_alpha)) *
								((tugi_q) * cos(tugi_alpha) + (1.0-tugi_q) * sin(tugi_alpha));//SnoIA_separation_adapter(data,l,a,b,tugi_alpha);						}
						}
					else if(tconf->mode==mode_sep_dchisq)
						{
							if((l==LMIN)&&(a==0)&&(b==0)) printf("tomo_ia: bias: Mode Dchisquare: computing C_separation_adapter....\n");
							data->cv[l][a][b] = data->cv[l][b][a] = C_separation_adapter(data,l,a,b,tugi_alpha);
						}
					else if(tconf->mode==mode_sep_aDfisher)
						{
							if((l==LMIN)&&(a==0)&&(b==0)) printf("tomo_ia: bias: Mode aDFisher: computing S_separation_adapter....\n");
							data->cv[l][a][b] = data->cv[l][b][a] = S_separation_adapter(data,l,a,b,tugi_alpha);
						}
					else if(tconf->mode==mode_victor) 
						{
							if((l==LMIN)&&(a==0)&&(b==0)) printf("tomo_ia: bias: Mode Victor: computing tomo_kappa+IA....\n");
							if(a==b) data->cv[l][a][b] = data->cv[l][b][a] = tomo_kappa(l,a,b)+data->ce[l][a]+data->ce_gi[l][a][b];
							else data->cv[l][a][b] = data->cv[l][b][a] = tomo_kappa(l,a,b)+data->ce_gi[l][a][b];
						}
					else 
						{
							if((l==LMIN)&&(a==0)&&(b==0)) printf("tomo_ia: bias: Other Modes: computing tomo_kappa....\n");
							data->cv[l][a][b] = data->cv[l][b][a] = tomo_kappa(l,a,b);
						}
				}
			}
		}
	}

	return(0);
}
// ---  ---
int COMPUTE_X_SPECTRUM(struct DATA *data)
{
	int l;

	printf("tomo_ia: bias: computing X weak lensing spectra for the covariance matrix....\n");
	gcosmo->sepIA_pbb = 0.9;
	gcosmo->sepIA_prr = 0.9;
	gcosmo->sepIA_prb = 1.0-gcosmo->sepIA_pbb;
	gcosmo->sepIA_pbr = 1.0-gcosmo->sepIA_prr;
	
	POPULATE_SEPARATION_NOISE(data);

#pragma omp parallel
	{
		int a,b;

#pragma omp for
		for(l=LMIN;l<LMAX;l++)
		{
			for(a=0;a<NBIN;a++)
			{
				for(b=a;b<NBIN;b++)
				{
					data->X[l][a][b] = data->X[l][b][a] = S_separation_adapter(data,l,a,b,tugi_alpha)+data->noise_sep[l][a][b];
				}
			}
		}
	}
	gcosmo->sepIA_pbb = 1.0;
	gcosmo->sepIA_prr = 1.0;
	gcosmo->sepIA_prb = 1.0-gcosmo->sepIA_pbb;
	gcosmo->sepIA_pbr = 1.0-gcosmo->sepIA_prr;
	POPULATE_SEPARATION_NOISE(data);

	return(0);
}

// ---  ---
int COMPUTE_SPECTRUM_P(struct DATA *data)
{
	int i,l;

	printf("tomo_ia: bias: computing spectra for increased cosmological parameters x-->x+dx....\n");

	for(i=NFISHER_START;i<NFISHER_END+1;i++)
	{
		SET_FIDUCIAL_COSMOLOGY(data);

		FisherParam_switch_plus(i);
		
		// compute accelerators
		RECALC_COSMOLOGY(data);

		// compute spectra
#pragma omp parallel
		{
			int a,b;

#pragma omp for
			for(l=LMIN;l<LMAX;l++)
			{
				for(a=0;a<NBIN;a++)
				{
					for(b=a;b<NBIN;b++)
					{
						if(tconf->mode==mode_gravslip)
						{
							if(a==b)
							{
								data->cu[l][a][b][i] = data->cu[l][b][a][i] = tomo_kappa(l,a,b)+data->ce[l][a]+data->ce_gi[l][a][b];
							}
							else
							{
								data->cu[l][a][b][i] = data->cu[l][b][a][i] = tomo_kappa(l,a,b)+data->ce_gi[l][a][b];
							}
						}
						else if(tconf->mode==mode_derivs) 
						{
							data->cu[l][a][b][i] = data->cu[l][b][a][i] = tomo_kappa_wIAs(l,a,b,data);
						}						
						else if(tconf->mode==mode_sep_biases) 
						{
							data->cu[l][a][b][i] = data->cu[l][b][a][i] = SnoIA_separation_adapter(data,l,a,b,tugi_alpha);
						}
						else if(tconf->mode==mode_sep_aDfisher) 
						{
							data->cu[l][a][b][i] = data->cu[l][b][a][i] = S_separation_adapter(data,l,a,b,tugi_alpha);
						}
						else if(tconf->mode==mode_victor)
						{
							if(a==b)
							{
								data->cu[l][a][b][i] = data->cu[l][b][a][i] = tomo_kappa(l,a,b)+data->ce[l][a]+data->ce_gi[l][a][b];
							}
							else
							{
								data->cu[l][a][b][i] = data->cu[l][b][a][i] = tomo_kappa(l,a,b)+data->ce_gi[l][a][b];
							}
						}
						else
						{
							data->cu[l][a][b][i] = data->cu[l][b][a][i] = tomo_kappa(l,a,b);
						}
					}
				}
			}
		}

		FREE_INTERPOLATION_MEMORY(data);
	}

	return(0);
}
// ---  ---


// ---  ---
int COMPUTE_SPECTRUM_M(struct DATA *data)
{
	int i,l;

	printf("tomo_ia: bias: computing spectra for decreased cosmological parameters x-->x-dx....\n");

	for(i=NFISHER_START;i<NFISHER_END+1;i++)
	{
		SET_FIDUCIAL_COSMOLOGY(data);

		FisherParam_switch_minus(i);

		// compute accelerators
		RECALC_COSMOLOGY(data);

		// compute spectra
#pragma omp parallel
		{
			int a,b;

#pragma omp for
			for(l=LMIN;l<LMAX;l++)
			{
				for(a=0;a<NBIN;a++)
				{
					for(b=a;b<NBIN;b++)
					{
						if(tconf->mode==mode_gravslip)
						{
							if(a==b)
							{
								data->cl[l][a][b][i] = data->cl[l][b][a][i] = tomo_kappa(l,a,b)+data->ce[l][a]+data->ce_gi[l][a][b];
							}
							else
							{
								data->cl[l][a][b][i] = data->cl[l][b][a][i] = tomo_kappa(l,a,b)+data->ce_gi[l][a][b];
							}
						}
						else if(tconf->mode==mode_derivs) 
							data->cl[l][a][b][i] = data->cl[l][b][a][i] = tomo_kappa_wIAs(l,a,b,data);
						else if(tconf->mode==mode_sep_biases) 
							data->cl[l][a][b][i] = data->cl[l][b][a][i] = SnoIA_separation_adapter(data,l,a,b,tugi_alpha);
						else if(tconf->mode==mode_sep_aDfisher) 
							data->cl[l][a][b][i] = data->cl[l][b][a][i] = S_separation_adapter(data,l,a,b,tugi_alpha);
						else if(tconf->mode==mode_victor)
						{
							if(a==b)
							{
								data->cl[l][a][b][i] = data->cl[l][b][a][i] = tomo_kappa(l,a,b)+data->ce[l][a]+data->ce_gi[l][a][b];
							}
							else
							{
								data->cl[l][a][b][i] = data->cl[l][b][a][i] = tomo_kappa(l,a,b)+data->ce_gi[l][a][b];
							}
						}
						else
							data->cl[l][a][b][i] = data->cl[l][b][a][i] = tomo_kappa(l,a,b);
					}
				}
			}
		}

		FREE_INTERPOLATION_MEMORY(data);
	}

	return(0);
}
// ---  ---


// ---  ---
int COMPUTE_SPECTRUM_DERIVATIVE(struct DATA *data)
{
	int i,l;
	double delta;

	printf("tomo_ia: bias: computing the derivatives of the weak lensing spectra....\n");

	SET_FIDUCIAL_COSMOLOGY(data);

	for(i=NFISHER_START;i<NFISHER_END+1;i++)
	{
		delta = FisherParam_switch_delta(i);
#pragma omp parallel
		{
			int a,b;
			double aux;

#pragma omp for
			for(l=LMIN;l<LMAX;l++)
			{
				for(a=0;a<NBIN;a++)
				{
					for(b=a;b<NBIN;b++)
					{
						aux = data->cu[l][a][b][i] - data->cl[l][a][b][i];
						aux /= 2.0 * delta;

						data->dc[l][a][b][i] = data->dc[l][b][a][i] = aux;
					}
				}
			}
		}
		if(tconf->mode==mode_derivs) WRITE_DERVIATIVE_2DISK(data, i);
	}

	return(0);
}
// ---  ---


// ---  ---
int CONSTRUCT_FISHER_MATRIX(struct DATA *data)
{
	int a,b,i,j,l;
	int signum;
	gsl_matrix *cc,*da,*db,*ci,*lu,*cida,*cidb;
	gsl_permutation *p;
	double aux,noise,sum;

	printf("tomo_ia: bias: constructing Fisher-matrix....\n");

	data->fish = gsl_matrix_calloc(NFISHER,NFISHER);
	cc = gsl_matrix_calloc(NBIN,NBIN);
	da = gsl_matrix_calloc(NBIN,NBIN);
	db = gsl_matrix_calloc(NBIN,NBIN);
	ci = gsl_matrix_calloc(NBIN,NBIN);
	lu = gsl_matrix_calloc(NBIN,NBIN);
	cida = gsl_matrix_calloc(NBIN,NBIN);
	cidb = gsl_matrix_calloc(NBIN,NBIN);
	p = gsl_permutation_calloc(NBIN);

	noise = gcosmo->ellipticity / gcosmo->nmean * NBIN;

	for(i=0;i<NFISHER;i++)
	{
		for(j=i;j<NFISHER;j++)
		{
			sum = 0.0;
			for(l=LMIN;l<LMAX;l++)
			{
				// construct matrices
				for(a=0;a<NBIN;a++)
				{
					for(b=0;b<NBIN;b++)
					{
						if(tconf->mode==mode_sep_aDfisher)
						{
							aux = data->cv[l][a][b];
							if(a == b) aux += data->noise_sep[l][a][b];
						}
						else if(tconf->mode==mode_sep_biases)
						{
							aux = data->cv[l][a][b];
							if(a == b) aux += data->noise_sep[l][a][b];
						}
						else
						{
							aux = data->cv[l][a][b];
							if(a == b) aux += noise;
						}
						gsl_matrix_set(cc,a,b,aux);

						gsl_matrix_set(da,a,b,data->dc[l][a][b][i]);
						gsl_matrix_set(db,a,b,data->dc[l][a][b][j]);
					}
				}

				// invert covariance matrix
				gsl_matrix_memcpy(lu,cc);
				gsl_linalg_LU_decomp(lu,p,&signum);
				gsl_linalg_LU_invert(lu,p,ci);

				// assemble product and trace out
				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,da,0.0,cida);
				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,db,0.0,cidb);
				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,cida,cidb,0.0,cc);

				sum += gcosmo->f_sky * (2.0 * l + 1.0) / 2.0 * spirou_trace(cc);
			}

			gsl_matrix_set(data->fish,i,j,sum);
			gsl_matrix_set(data->fish,j,i,sum);
		} // j-loop
	} // i-loop

	gsl_matrix_free(cc);
	gsl_matrix_free(da);
	gsl_matrix_free(db);
	gsl_matrix_free(ci);
	gsl_matrix_free(lu);
	gsl_matrix_free(cida);
	gsl_matrix_free(cidb);
	gsl_permutation_free(p);

	return(0);
}
// ---  ---
int CALCULATE_DCHISQ_SEP(struct DATA *data)
{
	int a,b,i,j,l;
	int signum1, signum2, signum3;
	gsl_matrix *x1,*c1,*x2,*c2,*lu1,*lu2,*lu3,*x2i,*x2ic2;
	gsl_permutation *p1, *p2, *p3;
	double sum, aux, detc1, detx1;
	double lndetc1, lndetx1;

	printf("tomo_ia: bias: calculating delta Chi squared....\n");

	
	x1 = gsl_matrix_calloc(NBIN,NBIN);
	c1 = gsl_matrix_calloc(NBIN,NBIN);
	x2 = gsl_matrix_calloc(NBIN,NBIN);
	c2 = gsl_matrix_calloc(NBIN,NBIN);

	lu1 = gsl_matrix_calloc(NBIN,NBIN);
	lu2 = gsl_matrix_calloc(NBIN,NBIN);
	lu3 = gsl_matrix_calloc(NBIN,NBIN);

	x2i = gsl_matrix_calloc(NBIN,NBIN);
	x2ic2 = gsl_matrix_calloc(NBIN,NBIN);
	
	p1 = gsl_permutation_calloc(NBIN);
	p2 = gsl_permutation_calloc(NBIN);
	p3 = gsl_permutation_calloc(NBIN);

	sum = 0;
	for(l=LMIN;l<LMAX;l++)
		{
			// construct matrices
			for(a=0;a<NBIN;a++)
			{
				for(b=0;b<NBIN;b++)
				{
					gsl_matrix_set(c1,a,b,data->cv[l][a][b]);
					gsl_matrix_set(c2,a,b,data->cv[l][a][b]);
					gsl_matrix_set(x1,a,b,data->X[l][a][b]);
					gsl_matrix_set(x2,a,b,data->X[l][a][b]);
				}
			}
			gsl_matrix_memcpy(lu1,x1);
			gsl_linalg_LU_decomp(lu1,p1,&signum1);

			lndetx1 = gsl_linalg_LU_lndet(lu1);

			gsl_matrix_memcpy(lu2,c1);
			gsl_linalg_LU_decomp(lu2,p2,&signum2);

			lndetc1 = gsl_linalg_LU_lndet(lu2);

			// invert X matrix
			gsl_matrix_memcpy(lu3,x2);
			gsl_linalg_LU_decomp(lu3,p3,&signum3);
			gsl_linalg_LU_invert(lu3,p3,x2i);

			// assemble product and trace out
			gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,x2i,c2,0.0,x2ic2);

			aux = gcosmo->f_sky * (2.0 * l + 1.0) * ( (lndetx1 - lndetc1) + (spirou_trace(x2ic2) - NBIN) ); 
			sum += aux;
			data->deltaDchisq[l] = aux;
			data->Dchisq[l] = sum;
		}


	gsl_matrix_free(x1);
	gsl_matrix_free(c1);
	gsl_matrix_free(x2);
	gsl_matrix_free(c2);

	gsl_matrix_free(lu1);
	gsl_matrix_free(lu2);
	gsl_matrix_free(lu3);
	
	gsl_matrix_free(x2i);
	gsl_matrix_free(x2ic2);
	
	gsl_permutation_free(p1);
	gsl_permutation_free(p2);
	gsl_permutation_free(p3);

	return(0);
}


/* --- function compute_spectrum_pp [x-->x+dx, y-->y+dy] --- */
int COMPUTE_SPECTRUM_PP(struct DATA *data)
{
	int i,j,l;

	printf("tomo_ia: bias: computing covariance for x-->x+dx, y-->y+dy....\n");

	for(i=NFISHER_START;i<NFISHER_END+1;i++)
	{
		SET_FIDUCIAL_COSMOLOGY(data);

		FisherParam_switch_plus(i);

		for(j=i+1;j<NFISHER_END+1;j++)
		{
			FisherParam_switch_plus(j);

			// fix normalisation and recache accelerators
			RECALC_COSMOLOGY(data);
			printf("tomo_ia: bias: + + computing spectra.... (i= %i j= %i) \n", i , j);

			// compute covariance matrix, leave out diagonal (computed differently)
#pragma omp parallel
			{
				double aux;
				int a,b;

#pragma omp for
				for(l=LMIN;l<LMAX;l++)
				{
					for(a=0;a<NBIN;a++)
					{
						for(b=a;b<NBIN;b++)
						{
							if(tconf->mode==mode_sep_biases)
							{
								aux = gcosmo->nmean * gcosmo->nmean * tomo_kappa(l,a,b) * 
								((tugi_q) * cos(tugi_alpha) + (1.0-tugi_q) * sin(tugi_alpha)) *
								((tugi_q) * cos(tugi_alpha) + (1.0-tugi_q) * sin(tugi_alpha));//SnoIA_separation_adapter(data,l,a,b,tugi_alpha);
							}
							else 
								aux = tomo_kappa(l,a,b);
							data->cuu[l][a][b][i][j] = aux;
							data->cuu[l][a][b][j][i] = aux;
							data->cuu[l][b][a][i][j] = aux;
							data->cuu[l][b][a][j][i] = aux;
						}
					}
				}
			} // parallel

			//FisherParam_switch_plus_revert(j);

			FREE_INTERPOLATION_MEMORY(data);
		}
	}

	return(0);
}
/* ---  --- */


/* --- function compute_spectrum_pm [x-->x+dx, y-->y-dy] --- */
int COMPUTE_SPECTRUM_PM(struct DATA *data)
{
	int i,j,l;

	printf("tomo_ia: bias: computing covariance for x-->x+dx, y-->y-dy....\n");

	for(i=NFISHER_START;i<NFISHER_END+1;i++)
	{
		SET_FIDUCIAL_COSMOLOGY(data);

		FisherParam_switch_plus(i);

		for(j=NFISHER_START;j<NFISHER_END+1;j++)
		{
			if(i == j) continue;

			FisherParam_switch_minus(j);

			// fix normalisation and recache accelerators
			RECALC_COSMOLOGY(data);

			// compute covariance matrix, leave out diagonal (computed differently)
			printf("tomo_ia: bias: + - computing spectra.... (i= %i j= %i) \n", i , j);
#pragma omp parallel
			{
				double aux;
				int a,b;

#pragma omp for
				for(l=LMIN;l<LMAX;l++)
				{
					for(a=0;a<NBIN;a++)
					{
						for(b=a;b<NBIN;b++)
						{
							if(tconf->mode==mode_sep_biases)
							{
								aux = gcosmo->nmean * gcosmo->nmean * tomo_kappa(l,a,b) * 
								((tugi_q) * cos(tugi_alpha) + (1.0-tugi_q) * sin(tugi_alpha)) *
								((tugi_q) * cos(tugi_alpha) + (1.0-tugi_q) * sin(tugi_alpha));//SnoIA_separation_adapter(data,l,a,b,tugi_alpha);
							}							
							else 
							{
								aux = tomo_kappa(l,a,b);
							}
							data->cul[l][a][b][i][j] = aux;
							data->cul[l][b][a][i][j] = aux;

						}
					}
				}
			} // parallel

			// FisherParam_switch_minus_revert(j);
			FREE_INTERPOLATION_MEMORY(data);
		}
	}

	return(0);
}
/* ---  --- */


/* --- function compute_spectrum_mm [x-->x-dx, y-->y-dy] --- */
int COMPUTE_SPECTRUM_MM(struct DATA *data)
{
	int i,j,l;

	printf("tomo_ia: bias: computing covariance for x-->x-dx, y-->y-dy....\n");

	for(i=NFISHER_START;i<NFISHER_END+1;i++)
	{
		SET_FIDUCIAL_COSMOLOGY(data);

		FisherParam_switch_minus(i);

		for(j=i+1;j<NFISHER_END+1;j++)
		{
			FisherParam_switch_minus(j);

			// fix normalisation and recache accelerators
			RECALC_COSMOLOGY(data);
			printf("tomo_ia: bias: - - computing spectra.... (i= %i j= %i) \n", i , j);

			// compute covariance matrix, leave out diagonal (computed differently)
#pragma omp parallel
			{
				double aux;
				int a,b;

#pragma omp for
				for(l=LMIN;l<LMAX;l++)
				{
					for(a=0;a<NBIN;a++)
					{
						for(b=a;b<NBIN;b++)
						{
							if(tconf->mode==mode_sep_biases)
							{
								aux = gcosmo->nmean * gcosmo->nmean * tomo_kappa(l,a,b) * 
								((tugi_q) * cos(tugi_alpha) + (1.0-tugi_q) * sin(tugi_alpha)) *
								((tugi_q) * cos(tugi_alpha) + (1.0-tugi_q) * sin(tugi_alpha));//SnoIA_separation_adapter(data,l,a,b,tugi_alpha);
							}
							else
							{
								aux = tomo_kappa(l,a,b);
							}
							data->cll[l][a][b][i][j] = aux;
							data->cll[l][a][b][j][i] = aux;
							data->cll[l][b][a][i][j] = aux;
							data->cll[l][b][a][j][i] = aux;
						}
					}
				}
			} // parallel

			// FisherParam_switch_minus_revert(j);
			FREE_INTERPOLATION_MEMORY(data);
		}
	}

	return(0);
}
/* ---  --- */


/* --- function compute_hessian_derivative [computes second derivatives d^2C/dx/dy] --- */
int COMPUTE_HESSIAN_DERIVATIVE(struct DATA *data)
{
	int i,j,l;
	double i_delta,j_delta;

	printf("tomo_ia: bias: computing matrix of second derivatives d^2C/dx/dy....\n");

	SET_FIDUCIAL_COSMOLOGY(data);

	for(i=NFISHER_START;i<NFISHER_END+1;i++)
	{
		i_delta = FisherParam_switch_delta(i);


		for(j=NFISHER_START;j<NFISHER_END+1;j++)
		{
			j_delta = FisherParam_switch_delta(j);


#pragma omp parallel
			{
				double aux,result;
				int a,b;

#pragma omp for
				for(l=LMIN;l<LMAX;l++)
				{
					// diagonal elements --> Laplacian approximation
					if(i == j)
					{
						for(a=0;a<NBIN;a++)
						{
							for(b=a;b<NBIN;b++)
							{
								aux = data->cu[l][a][b][i] -
									2.0 * data->cv[l][a][b] +
									data->cl[l][a][b][i];

								result = aux / gsl_pow_int(i_delta,2);
								data->d2c[l][a][b][i][i] = result;
								data->d2c[l][b][a][i][i] = result;
							}
						}
					}

					// off-diagonal elements --> second order Taylor approximation
					else
					{
						for(a=0;a<NBIN;a++)
						{
							for(b=a;b<NBIN;b++)
							{
								aux = data->cuu[l][a][b][i][j] -
								      data->cul[l][a][b][i][j] -
								      data->cul[l][a][b][j][i] +
								      data->cll[l][a][b][i][j];

								result = aux / i_delta / j_delta / 4.0;
								data->d2c[l][a][b][i][j] = result;
								data->d2c[l][b][a][i][j] = result;
							}
						}
					}
				} // l-loop
			} // parallel environment
		} // j-loop
	} // i-loop

	return(0);
}
/* ---  --- */


/* --- function construct_gmatrix --- */
int CONSTRUCT_GMATRIX(struct DATA *data)
{
	int a,b,i,j,l;
	double noise,sum,aux;
	gsl_matrix *ci,*da,*db,*dab,*cida,*cidb,*cidab,*cc,*qq,*cfct,*ct,*cf,*lu,*identity,*cidabci,*cidacidb;
	int signum;
	gsl_permutation *p;

	printf("tomo_ia: bias: constructing G-matrix....\n");

	// allocate matrices
	data->gext = gsl_matrix_calloc(NFISHER,NFISHER);

	ci = gsl_matrix_calloc(NBIN,NBIN);		// inverse covariance
	da = gsl_matrix_calloc(NBIN,NBIN);		// derivative of C, parameter a
	db = gsl_matrix_calloc(NBIN,NBIN);		// derivative of C, parameter b
	dab = gsl_matrix_calloc(NBIN,NBIN);		// second derivative
	cida = gsl_matrix_calloc(NBIN,NBIN);		// inverse covariance * derivative
	cidb = gsl_matrix_calloc(NBIN,NBIN);		// inverse covariance * derivative
	cidab = gsl_matrix_calloc(NBIN,NBIN);		// inverse covariance * second derivative
	cc = gsl_matrix_calloc(NBIN,NBIN);		// aux matrix
	qq = gsl_matrix_calloc(NBIN,NBIN);		// aux matrix
	cfct = gsl_matrix_calloc(NBIN,NBIN);		// inverse covariance * true covariance
	ct = gsl_matrix_calloc(NBIN,NBIN);		// true covariance
	cf = gsl_matrix_calloc(NBIN,NBIN);		// wrong covariance
	lu = gsl_matrix_calloc(NBIN,NBIN);		// LU decomposition
	p = gsl_permutation_calloc(NBIN);		// permutation structure
	identity = gsl_matrix_calloc(NBIN,NBIN);	// identitiy matrix
	gsl_matrix_set_identity(identity);
	cidacidb = gsl_matrix_calloc(NBIN,NBIN);	// inverse cov. * second derivative * inverse cov.

	noise = gcosmo->ellipticity / gcosmo->nmean * NBIN;

	for(i=NFISHER_START;i<NFISHER_END+1;i++)
	{
		for(j=i;j<NFISHER;j++)
		{
			sum = 0.0;
			for(l=LMIN;l<LMAX;l++)
			{
				gsl_matrix_set_zero(cc);
				// generate full covariance including noise and invert
				for(a=0;a<NBIN;a++)
				{
					for(b=0;b<NBIN;b++)
					{
						if(tconf->mode==mode_sep_biases)
						{
							aux = data->cv[l][a][b];
							if(a == b) aux += data->noise_sep[l][a][b];
						}
						else
						{
							aux = data->cv[l][a][b];
							if(a == b) aux += noise;
						}
						gsl_matrix_set(cc,a,b,aux);

						// first derivatives
						gsl_matrix_set(da,a,b,data->dc[l][a][b][i]);
						gsl_matrix_set(db,a,b,data->dc[l][a][b][j]);

						// second derivatives
						gsl_matrix_set(dab,a,b,data->d2c[l][a][b][i][j]);
					}
				}

				gsl_matrix_memcpy(lu,cc);
				gsl_linalg_LU_decomp(lu,p,&signum);
				gsl_linalg_LU_invert(lu,p,ci);

				// generate true and false (noiseless) covariance matrices
				for(a=0;a<NBIN;a++)
				{
					for(b=0;b<NBIN;b++)
					{
						// wrong covariance: just lensing
						if(tconf->mode==mode_sep_biases)
							{
							aux = data->cv[l][a][b];
							if (a==b) aux += data->noise_sep[l][a][b];
							}
						else
							{
							aux = data->cv[l][a][b];
							if(a == b) aux += noise;
							}
						gsl_matrix_set(cf,a,b,aux);

						// true covariance: lensing + ia on the diagonal
						if(tconf->mode==mode_sep_biases)
						{
							aux = data->signal_sep[l][a][b]+data->noise_sep[l][a][b];
						}
						else
						{
							aux = data->cv[l][a][b];
							if(a == b) aux += (data->ce[l][a] + noise);
							// add gi_emode
							if(tconf->flag_GIs==on) aux += data->ce_gi[l][a][b];							
						}
						gsl_matrix_set(ct,a,b,aux);
						// printf("tomo_ia: bias: populated one set of matrices....\n");
					}
				}

				gsl_matrix_memcpy(lu,cf);
				gsl_linalg_LU_decomp(lu,p,&signum);
				gsl_linalg_LU_invert(lu,p,cf);

				// matrix products, first term --> CC
				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,dab,0.0,cidab);

				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,cf,ct,0.0,cfct);
				gsl_matrix_sub(cfct,identity);

				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,cidab,cfct,0.0,cc);

				// matrix products, second term --> QQ
				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,da,0.0,cida);
				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,db,0.0,cidb);
				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,cida,cidb,0.0,cidacidb);

				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,cf,ct,0.0,cfct);
				gsl_matrix_scale(cfct,2.0);
				gsl_matrix_sub(cfct,identity);

				gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,cidacidb,cfct,0.0,qq);

				//printf("%i --> cc = %e, qq = %e\n",l,spirou_trace(cc),spirou_trace(qq));

				// subtract matrices
				gsl_matrix_sub(cc,qq);

				// trace out
				sum += (2.0 * l + 1.0) * spirou_trace(cc);
			} // l-loop

			gsl_matrix_set(data->gext,i,j,sum);
			gsl_matrix_set(data->gext,j,i,sum);
			//printf("%i %i --> %e\n",i,j,sum);
		} // j-loop
	} // i-loop

	// deallocate matrices
	gsl_matrix_free(ci);
	gsl_matrix_free(da);
	gsl_matrix_free(db);
	gsl_matrix_free(cida);
	gsl_matrix_free(cidb);
	gsl_matrix_free(cidab);
	gsl_matrix_free(cc);
	gsl_matrix_free(qq);
	gsl_matrix_free(cfct);
	gsl_matrix_free(cf);
	gsl_matrix_free(lu);
	gsl_permutation_free(p);
	gsl_matrix_free(identity);
	gsl_matrix_free(cidacidb);

	return(0);
}
/* ---  --- */


/* --- function construct_avector --- */
int CONSTRUCT_AVECTOR(struct DATA *data)
{
	int a,b,i,l;
	double noise,sum,aux;
	gsl_matrix *da,*cf,*ct,*ci,*cc,*cfct,*cida,*lu,*identity;
	int signum;
	gsl_permutation *p;

	printf("tomo_ia: bias: constructing a-vector....\n");

	// allocate matrices
	data->avec = gsl_vector_calloc(NFISHER);

	cf = gsl_matrix_calloc(NBIN,NBIN);		// wrong covariance
	ct = gsl_matrix_calloc(NBIN,NBIN);		// true covariance
	da = gsl_matrix_calloc(NBIN,NBIN);		// derivative
	cc = gsl_matrix_calloc(NBIN,NBIN);		// aux matrix
	ci = gsl_matrix_calloc(NBIN,NBIN);		// inverse covariance
	cfct = gsl_matrix_calloc(NBIN,NBIN);		// inverse covariance * true covariance
	cida = gsl_matrix_calloc(NBIN,NBIN);		// inverse covariance * derivative
	lu = gsl_matrix_calloc(NBIN,NBIN);		// LU-decomposition
	p = gsl_permutation_calloc(NBIN);		// permutation
	identity = gsl_matrix_calloc(NBIN,NBIN);	// identity matrix
	gsl_matrix_set_identity(identity);

	noise = gcosmo->ellipticity / gcosmo->nmean * NBIN;

	for(i=NFISHER_START;i<NFISHER_END+1;i++)
	{
		sum = 0.0;
		for(l=LMIN;l<LMAX;l++)
		{
			// generate full covariance matrix and invert
			gsl_matrix_set_zero(cc);
			for(a=0;a<NBIN;a++)
			{
				for(b=0;b<NBIN;b++)
				{
					// full covariance including noise
					if(tconf->mode==mode_sep_biases)
					{
						aux = data->cv[l][a][b];
						if(a == b) aux += data->noise_sep[l][a][b];	
					}
					else
					{
						aux = data->cv[l][a][b];
						if(a == b) aux += noise;						
					}
					gsl_matrix_set(cc,a,b,aux);

					// gradient
					gsl_matrix_set(da,a,b,data->dc[l][a][b][i]);
				}
			}

			gsl_matrix_memcpy(lu,cc);
			gsl_linalg_LU_decomp(lu,p,&signum);
			gsl_linalg_LU_invert(lu,p,ci);

			// generate true and wrong noiseless covariance matrices
			for(a=0;a<NBIN;a++)
			{
				for(b=0;b<NBIN;b++)
				{

					// wrong covariance: just lensing
					if(tconf->mode==mode_sep_biases)
					{
						aux = data->cv[l][a][b];
						if(a == b) aux += data->noise_sep[l][a][b];	
					}
					else
					{
						aux = data->cv[l][a][b];
						if(a == b) aux += noise;
					}
					gsl_matrix_set(cf,a,b,aux);

					// true covariance: lensing + ia on the diagonal
					if(tconf->mode==mode_sep_biases)
					{
						aux = data->signal_sep[l][a][b]+data->noise_sep[l][a][b];
						//C_separation_adapter(data, l, a, b, tugi_alpha);
					}
					else
					{
						aux = data->cv[l][a][b];
						if(a == b) aux += data->ce[l][a] + noise;
						// add gi_emode
						if(tconf->flag_GIs==on) aux += data->ce_gi[l][a][b];						
					}
					gsl_matrix_set(ct,a,b,aux);
				}
			}

			gsl_matrix_memcpy(lu,cf);
			gsl_linalg_LU_decomp(lu,p,&signum);
			gsl_linalg_LU_invert(lu,p,cf);

			// matrix products
			gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,cf,ct,0.0,cfct);
			gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,ci,da,0.0,cida);
			gsl_matrix_sub(cfct,identity);
			gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,cida,cfct,0.0,cc);

			// trace out
			sum += (2.0 * l + 1.0) * spirou_trace(cc);
		} // l-loop
		gsl_vector_set(data->avec,i,sum);
		//printf("%i --> %e\n",i,sum);
	} // i-loop

	// deallocate matrices
	gsl_matrix_free(cf);
	gsl_matrix_free(ct);
	gsl_matrix_free(da);
	gsl_matrix_free(cc);
	gsl_matrix_free(ci);
	gsl_matrix_free(cfct);
	gsl_matrix_free(cida);
	gsl_matrix_free(lu);
	gsl_permutation_free(p);
	gsl_matrix_free(identity);

	return(0);
}
/* ---  --- */


/* --- function compute_estimation_bias [solves linear system G delta = a for delta = G^-1 a] --- */
int COMPUTE_ESTIMATION_BIAS(struct DATA *data)
{
	printf("tomo_ia: bias: computing parameter estimation bias....\n");

	data->bias = gsl_vector_calloc(NFISHER);
	gsl_linalg_HH_solve(data->gext,data->avec,data->bias);

	return(0);
}
/* ---  --- */

int FisherParam_switch_plus(int iFisher)
{

	if(tconf->mode==mode_victor)
	{
		tconf->alphapm=plus;
		if(iFisher==alphaB) tconf->whichalpha=alphaB;
		else if(iFisher==alphaM) tconf->whichalpha=alphaM;	
		return(0);	
	}
	
	switch(iFisher)
		{
			case tugi_fisher_omegaM:
				gcosmo->omega_m *= (1.0 + eps_derivative);
				break;

			case tugi_fisher_sigma8:
				gcosmo->sigma8 *= (1.0 + eps_derivative);
				break;

			case tugi_fisher_Hubble:
				gcosmo->h *= (1.0 + eps_derivative);
				break;

			case tugi_fisher_ns:
				gcosmo->n_s *= (1.0 + eps_derivative);
				break;

			case tugi_fisher_eos:
				gcosmo->w *= (1.0 - eps_derivative);
				break;
			// Alignment Parameter
			case tugi_fisher_align:
				gcosmo->tugi_align *= (1.0 + eps_derivative); 	
				break;
			// Here be Gravslip				
			case tugi_fisher_eta:
				gcosmo->tugi_eta *= (1.0 + eps_derivative);
				break;
			case tugi_fisher_mu:
				gcosmo->tugi_mu *= (1.0 + eps_derivative);
				break;
			case tugi_fisher_IAa:
				gcosmo->tugi_align *= (1.0 + eps_derivative);
				break;
			case tugi_fisher_IAD:
				gcosmo->tugi_hooke *= (1.0 + eps_derivative);
				break;
				
	}
	return(0);
}

int FisherParam_switch_minus(int iFisher)
{

	if(tconf->mode==mode_victor)
	{
		tconf->alphapm = minus;
		if(iFisher==alphaB) tconf->whichalpha = alphaB;
		else if(iFisher==alphaM) tconf->whichalpha = alphaM;	
		return(0);	
	}

	switch(iFisher)
		{
			case tugi_fisher_omegaM:
				gcosmo->omega_m *= (1.0 - eps_derivative);
				break;

			case tugi_fisher_sigma8:
				gcosmo->sigma8 *= (1.0 - eps_derivative);
				break;

			case tugi_fisher_Hubble:
				gcosmo->h *= (1.0 - eps_derivative);
				break;

			case tugi_fisher_ns:
				gcosmo->n_s *= (1.0 - eps_derivative);
				break;

			case tugi_fisher_eos:
				gcosmo->w *= (1.0 + eps_derivative);
				break;
			// Alignment Parameter
			case tugi_fisher_align:
				gcosmo->tugi_align *= (1.0 - eps_derivative); 	
				break;
			// Here be Gravslip				
			case tugi_fisher_eta:
				gcosmo->tugi_eta *= (1.0 - eps_derivative);
				break;
			case tugi_fisher_mu:
				gcosmo->tugi_mu *= (1.0 - eps_derivative);
				break;
			case tugi_fisher_IAa:
				gcosmo->tugi_align *= (1.0 - eps_derivative);
				break;
			case tugi_fisher_IAD:
				gcosmo->tugi_hooke *= (1.0 - eps_derivative);
				break;
				
	}
	return(0);	
}
int FisherParam_switch_plus_revert(int iFisher)
{
	if(tconf->mode==mode_victor)
	{
		tconf->alphapm = neutral;
		if(iFisher==alphaB) tconf->whichalpha = alphaB;
		else if(iFisher==alphaM) tconf->whichalpha = alphaM;	
		return(0);	
	}
	switch(iFisher)
		{
			case tugi_fisher_omegaM:
				gcosmo->omega_m /= (1.0 + eps_derivative);
				break;

			case tugi_fisher_sigma8:
				gcosmo->sigma8 /= (1.0 + eps_derivative);
				break;

			case tugi_fisher_Hubble:
				gcosmo->h /= (1.0 + eps_derivative);
				break;

			case tugi_fisher_ns:
				gcosmo->n_s /= (1.0 + eps_derivative);
				break;

			case tugi_fisher_eos:
				gcosmo->w /= (1.0 - eps_derivative);
				break;
			// Alignment Parameter
			case tugi_fisher_align:
				gcosmo->tugi_align /= (1.0 + eps_derivative); 	
				break;
			// Here be Gravslip				
			case tugi_fisher_eta:
				gcosmo->tugi_eta /= (1.0 + eps_derivative);
				break;
			case tugi_fisher_mu:
				gcosmo->tugi_mu /= (1.0 + eps_derivative);
				break;
			case tugi_fisher_IAa:
				gcosmo->tugi_align /= (1.0 + eps_derivative);
				break;
			case tugi_fisher_IAD:
				gcosmo->tugi_hooke /= (1.0 + eps_derivative);
				break;
				
	}
	return(0);
}

int FisherParam_switch_minus_revert(int iFisher)
{
	if(tconf->mode==mode_victor)
	{
		tconf->alphapm = neutral;
		if(iFisher==alphaB) tconf->whichalpha = alphaB;
		else if(iFisher==alphaM) tconf->whichalpha = alphaM;	
		return(0);	
	}

	switch(iFisher)
		{
			case tugi_fisher_omegaM:
				gcosmo->omega_m /= (1.0 - eps_derivative);
				break;

			case tugi_fisher_sigma8:
				gcosmo->sigma8 /= (1.0 - eps_derivative);
				break;

			case tugi_fisher_Hubble:
				gcosmo->h /= (1.0 - eps_derivative);
				break;

			case tugi_fisher_ns:
				gcosmo->n_s /= (1.0 - eps_derivative);
				break;

			case tugi_fisher_eos:
				gcosmo->w /= (1.0 + eps_derivative);
				break;
			// Alignment Parameter
			case tugi_fisher_align:
				gcosmo->tugi_align /= (1.0 - eps_derivative); 	
				break;
			// Here be Gravslip				
			case tugi_fisher_eta:
				gcosmo->tugi_eta /= (1.0 - eps_derivative);
				break;
			case tugi_fisher_mu:
				gcosmo->tugi_mu /= (1.0 - eps_derivative);
				break;
			case tugi_fisher_IAa:
				gcosmo->tugi_align /= (1.0 - eps_derivative);
				break;
			case tugi_fisher_IAD:
				gcosmo->tugi_hooke /= (1.0 - eps_derivative);
				break;
				
	}
	return(0);	
}

double FisherParam_switch_delta(int iFisher)
{
	double delta;
	switch(iFisher)
			{
				case tugi_fisher_omegaM:
					delta = eps_derivative * gcosmo->omega_m;
					break;

				case tugi_fisher_sigma8:
					delta = eps_derivative * gcosmo->sigma8;
					break;

				case tugi_fisher_Hubble:
					delta = eps_derivative * gcosmo->h;
					break;

				case tugi_fisher_ns:
					delta = eps_derivative * gcosmo->n_s;
					break;

				case tugi_fisher_eos:
					delta = eps_derivative * fabs(gcosmo->w);
					break;
				
				// Alignment Parameter
				case tugi_fisher_align:
					delta = eps_derivative * gcosmo->tugi_align; 	
					break;
				
				// Here be Gravslip				
				case tugi_fisher_eta:
					delta = eps_derivative * gcosmo->tugi_eta;
					break;
				case tugi_fisher_mu:
					delta = eps_derivative * gcosmo->tugi_mu;
					break;
				
				case tugi_fisher_IAa:
					delta = eps_derivative * gcosmo->tugi_align;
					break;
				case tugi_fisher_IAD:
					delta = eps_derivative * gcosmo->tugi_hooke;
					break;

				case tugi_fisher_alphaM:
					delta = tugi_delta_alphaM * tugi_alphaM;
					break;
				case tugi_fisher_alphaB:
					delta = tugi_delta_alphaB * tugi_alphaB;
					break;
			}//switch
	//printf("tomo_ia: bias: delta = %e \n", delta);
	return(delta);
}
