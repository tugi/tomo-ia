#include <tomo.h>

/* --- function spirou_zeta [spherical Bessel-weighted moments of the CDM spectrum] --- */
double spirou_zeta(double r, int n)
{
	double aux,error,result;
	struct ZETAPARAM zetaparam;
	tconf->whichBardeen=BardeenPsi;

	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	zetaparam.n = n;
	zetaparam.r = r;
	
	F.function = &aux_spirou_zeta;
	F.params = &zetaparam;

	if(tconf->flag_extPk==on) aux = tugi_integrate_cquad_k(flag_zeta, r, n);	
	else gsl_integration_qagiu(&F,0.0,0.0,smalleps,NEVAL,w,&aux,&error);
	
	result = gsl_pow_int(-1.0,n) * twopi * aux / gsl_pow_int(r,2);
	
	switch(tconf->morphology)
	{
		case ellipse:
			result = gsl_pow_int(-1.0,n) * aux / gsl_pow_int(r,2);	//Philipps convention	
			result /= pi;						//Philipps convention
			result /= twopi;					//Philipps convention
			result *= gsl_pow_int(3.0/2.0 * gcosmo->omega_m, 2);	//Philipps convention
			result /= gsl_pow_int(spirou_dhubble,4);		//Philipps convention
			// printf("A = %e <----- with *hooke \n" , result/(gsl_pow_int(-1.0,n) * aux / gsl_pow_int(r,2)) * hooke);
			// printf("A = %e <----- with *hooke ** 2\n" , result/(gsl_pow_int(-1.0,n) * aux / gsl_pow_int(r,2)) * hooke * hooke);
			break;
		case spiral:
			result += 0.0;
			break;
	}

	gsl_integration_workspace_free(w);
	return(result);
}
/* ---  --- */


/* --- function aux_spirou_zeta [integrand of spirou_zeta, include galaxy scale smoothing] --- */
double aux_spirou_zeta(double k, void *param)
{
	double x,r,result;
	struct ZETAPARAM *zetaparam = (struct ZETAPARAM *)param;
	int n;

	// Gravitational Slip parameter mu:
	//double tugi_aux = gcosmo->tugi_mu;

	r = zetaparam->r;
	n = zetaparam->n;
	x = k * r;

	result = gsl_pow_int(x,n-2) * gsl_sf_bessel_jl(n,x) * cdm_smooth(k);

	return(result);
}
/* ---  --- */

/* --- function tugi_zetaprime [spherical Bessel-weighted moments of the CDM spectrum @ \sqrt{Pk_Weyl Pk_Psi}] --- */
double tugi_zetaprime(double r, int n)
{

	double aux,error,result;
	struct ZETAPARAM zetaparam;
	tconf->whichBardeen=BardeenTugi;

	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	zetaparam.n = n;
	zetaparam.r = r;

	F.function = &aux_spirou_zeta;
	F.params = &zetaparam;

	if(tconf->flag_extPk==on) aux = tugi_integrate_cquad_k(flag_zeta, r, n);	
	else gsl_integration_qagiu(&F,0.0,0.0,smalleps,NEVAL,w,&aux,&error);
	
	result = gsl_pow_int(-1.0,n) * twopi * aux / gsl_pow_int(r,2);
	
	switch(tconf->morphology)
	{
		case ellipse:
			result = gsl_pow_int(-1.0,n) * aux / gsl_pow_int(r,2);	//Philipps convention	
			result /= pi;						//Philipps convention
			result /= twopi;					//Philipps convention
			result *= gsl_pow_int(3.0/2.0 * gcosmo->omega_m, 2);	//Philipps convention
			result /= gsl_pow_int(spirou_dhubble,4);		//Philipps convention
			// printf("A = %e <----- with *hooke \n" , result/(gsl_pow_int(-1.0,n) * aux / gsl_pow_int(r,2)) * hooke);
			// printf("A = %e <----- with *hooke ** 2\n" , result/(gsl_pow_int(-1.0,n) * aux / gsl_pow_int(r,2)) * hooke * hooke);
			break;
		case spiral:
			result += 0.0;
			break;
	}

	gsl_integration_workspace_free(w);
	return(result);
}
/* ---  --- */


/* --- function aux_tugi_zetaprime [integrand of tugi_zetaprime, include galaxy scale smoothing] --- */
double aux_tugi_zetaprime(double k, void *param)
{
	double x,r,result;
	struct ZETAPARAM *zetaparam = (struct ZETAPARAM *)param;
	int n;

	// Gravitational Slip parameter mu:
	//double tugi_aux = gcosmo->tugi_mu;

	r = zetaparam->r;
	n = zetaparam->n;
	x = k * r;
	result = gsl_pow_int(x,n-2) * gsl_sf_bessel_jl(n,x) * cdm_smooth(k);

	return(result);
}
/* ---  --- */


/* --- function spirou_zeta_norm [normalisation] --- */
double spirou_zeta_norm()
{
	double aux,error,result;

	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	F.function = &aux_spirou_zeta_norm;
	F.params = NULL;

	gsl_integration_qag(&F,0.0,20.0,eps,eps,NEVAL,GSL_INTEG_GAUSS15,w,&aux,&error);
	result = aux / 15.0;

	gsl_integration_workspace_free(w);

	return(result);
}
/* ---  --- */


/* --- function aux_spirou_zeta_norm [dk-integration] --- */
double aux_spirou_zeta_norm(double k, void *param)
{
	double result;
	// Gravitational Slip parameter mu:
	//double tugi_aux = gcosmo->tugi_mu;

	result = gsl_pow_int(k,2) * gsl_sf_bessel_jl(0,k) * cdm_smooth(k);

	return(result);
}
/* ---  --- */
double ellipticity_pp(double r,double alpha)
{
	double aux,result;
	double x,y,z;
	double zeta2,zeta3,zeta4;
	double zsqr2,zsqr3,zsqr4;

	// evaluate splines
	zeta2 = gsl_spline_eval(gcosmo->spline_z2,r,gcosmo->acc_z2);
	zeta3 = gsl_spline_eval(gcosmo->spline_z3,r,gcosmo->acc_z3);
	zeta4 = gsl_spline_eval(gcosmo->spline_z4,r,gcosmo->acc_z4);

	// squares
	zsqr2 = gsl_pow_int(zeta2,2);
	zsqr3 = gsl_pow_int(zeta3,2);
	zsqr4 = gsl_pow_int(zeta4,2);

	switch(tconf->morphology)
	{
		case ellipse:
			result = gsl_pow_int(gcosmo->tugi_hooke,2) * (4 * zeta2 + 4 * zeta3 * gsl_pow_int(sin(alpha),2)  + zeta4 * gsl_pow_int(sin(alpha),4));
			break;

		case spiral:
			//compute contributions
			x = 336.0 * zsqr2  + 472.0 * zeta2 * zeta3 + 155.0 * zsqr3 +
				58.0 * zeta2 * zeta4 + 26.0 * zeta3 * zeta4 + 3.0 * zsqr4;
			y = 18.0 * zeta2 * zeta3 - 7.0 * zsqr3 - 8.0 * zeta3 * zeta4 - zsqr4;
			z = 17.0 * zsqr3 + 6.0 * zeta2 * zeta4 + 6.0 * zeta3 * zeta4 + zsqr4;

			// collect results
			aux = x + 4.0 * y * cos(2.0 * alpha) + z * cos(4.0 * alpha);
			result = gsl_pow_int(gcosmo->tugi_align / 14.0,2) / 144.0 * aux;

			break;
		
	}
	return(result);
}
/* ---  --- */


/* --- function ellipticity_cc [3d <exex> correlation function] --- */
double ellipticity_cc(double r,double alpha)
{
	double aux,result;
	double x,y;
	double zeta2,zeta3,zeta4;
	double zsqr2,zsqr3,zsqr4;

	// evaluate splines
	zeta2 = gsl_spline_eval(gcosmo->spline_z2,r,gcosmo->acc_z2);
	zeta3 = gsl_spline_eval(gcosmo->spline_z3,r,gcosmo->acc_z3);
	zeta4 = gsl_spline_eval(gcosmo->spline_z4,r,gcosmo->acc_z4);

	// squares
	zsqr2 = gsl_pow_int(zeta2,2);
	zsqr3 = gsl_pow_int(zeta3,2);
	zsqr4 = gsl_pow_int(zeta4,2);

	switch(tconf->morphology)
	{
		case ellipse:
			result = 4.0 * gsl_pow_int(gcosmo->tugi_hooke,2) * (zeta2 + gsl_pow_int(sin(alpha),2) * zeta3);
			break;

		case spiral:
			// compute contributions
			x = 42.0 * zsqr2 + 59.0 * zeta2 * zeta3 + 13.0 * zsqr3 + 5.0 * zeta2 * zeta4 + zeta3 * zeta4;
			y = 9.0 * zeta2 * zeta3 + 5.0 * zsqr3 + 3.0 * zeta2 * zeta4 - zeta3 * zeta4;

			// collect results
			aux = x + y * cos(2.0 * alpha);
			result = gsl_pow_int(gcosmo->tugi_align / 14.0,2) / 18.0 * aux;
			break;
	}
	return(result);
}
/* ---  --- */


// function ellipticity_sp [3d <e+es> correlation function]
double ellipticity_sp(double r,double alpha)
{
	double aux,result;
	double x,y,z;
	double zeta2,zeta3,zeta4;
	double zsqr2,zsqr3,zsqr4;

	// evaluate splines
	zeta2 = gsl_spline_eval(gcosmo->spline_z2,r,gcosmo->acc_z2);
	zeta3 = gsl_spline_eval(gcosmo->spline_z3,r,gcosmo->acc_z3);
	zeta4 = gsl_spline_eval(gcosmo->spline_z4,r,gcosmo->acc_z4);

	// squares
	zsqr2 = gsl_pow_int(zeta2,2);
	zsqr3 = gsl_pow_int(zeta3,2);
	zsqr4 = gsl_pow_int(zeta4,2);

	switch(tconf->morphology)
	{
		case ellipse:
			result = -6.0 * gsl_pow_int(sin(alpha),2) * zeta3 - gsl_pow_int(sin(alpha),4) * zeta4;
			result *= gcosmo->tugi_hooke*gcosmo->tugi_hooke;
			break;
		case spiral:
			// compute contributions
			x = 7.0 * zsqr4 + (50.0 * zeta3 + 66.0 * zeta2) * zeta4 + 223.0 * zsqr3 + 408.0 * zeta2 * zeta3;
			y = 4.0 * zsqr4 + (32.0 * zeta3 + 48.0 * zeta2) * zeta4 + 172.0 * zsqr3 + 408.0 * zeta2 * zeta3;
			z = 3.0 * zsqr4 + (18.0 * zeta3 + 18.0 * zeta2) * zeta4 + 51.0 * zsqr3;
			
			// collect results
			aux = x - y * cos(2.0 * alpha) - z * cos(4.0 * alpha);
			result = gsl_pow_int(gcosmo->tugi_align / 14.0,2) / 108.0 * fabs(aux);
			break;
	}
	return(result);
}

// function ellipticity_ss [3d <eses> correlation function]
double ellipticity_ss(double r,double alpha)
{
	double aux,result;
	double x,y,z;
	double zeta2,zeta3,zeta4;
	double zsqr2,zsqr3,zsqr4;

	// evaluate splines
	zeta2 = gsl_spline_eval(gcosmo->spline_z2,r,gcosmo->acc_z2);
	zeta3 = gsl_spline_eval(gcosmo->spline_z3,r,gcosmo->acc_z3);
	zeta4 = gsl_spline_eval(gcosmo->spline_z4,r,gcosmo->acc_z4);

	// squares
	zsqr2 = gsl_pow_int(zeta2,2);
	zsqr3 = gsl_pow_int(zeta3,2);
	zsqr4 = gsl_pow_int(zeta4,2);

	switch(tconf->morphology)
	{
		case ellipse:
			result = 8.0 * zeta2 + 8.0 * gsl_pow_int(sin(alpha),2) * zeta3 + gsl_pow_int(sin(alpha),4) * zeta4;
			result *= gcosmo->tugi_hooke*gcosmo->tugi_hooke;
			break;
			
		case spiral:
			// compute contributions
			x = 59.0 * zsqr4 + (490.0 * zeta3 + 426.0 * zeta2) * zeta4 + 1907.0 * zsqr3 + 3864.0 * zeta2 * zeta3 + 2448.0 * zsqr2;
			y = 60.0 * zsqr4 + (480.0 * zeta3 + 288.0 * zeta2) * zeta4 + 1284.0 * zsqr3 + 1800.0 * zeta2 * zeta3;
			z = 9.0 * zsqr4 + (54.0 * zeta3 + 54.0 * zeta2) * zeta4 + 153.0 * zsqr3;

			// collect results
			aux = x + y * cos(2.0 * alpha) + z * cos(4.0 * alpha);
			result = gsl_pow_int(gcosmo->tugi_align / 14.0,2) / 324.0 * aux;
			break;
	}
	return(result);
}
// ---  ---

/* --- function ellipticity_pp [3d <e+e+> correlation function] --- */
double ellipticity_gi_pp(double r,double alpha)
{
	double aux,result;
	double x,y,z;
	double zeta2,zeta3,zeta4;
	double zsqr2,zsqr3,zsqr4;

	// evaluate splines
	zeta2 = gsl_spline_eval(gcosmo->spline_z2p,r,gcosmo->acc_z2p);
	zeta3 = gsl_spline_eval(gcosmo->spline_z3p,r,gcosmo->acc_z3p);
	zeta4 = gsl_spline_eval(gcosmo->spline_z4p,r,gcosmo->acc_z4p);

	// squares
	zsqr2 = gsl_pow_int(zeta2,2);
	zsqr3 = gsl_pow_int(zeta3,2);
	zsqr4 = gsl_pow_int(zeta4,2);

	switch(tconf->morphology)
	{
		case ellipse:
			result = gsl_pow_int(gcosmo->tugi_hooke,2) * (4 * zeta2 + 4 * zeta3 * gsl_pow_int(sin(alpha),2)  + zeta4 * gsl_pow_int(sin(alpha),4));
			break;

		case spiral:
			//compute contributions
			x = 336.0 * zsqr2  + 472.0 * zeta2 * zeta3 + 155.0 * zsqr3 +
				58.0 * zeta2 * zeta4 + 26.0 * zeta3 * zeta4 + 3.0 * zsqr4;
			y = 18.0 * zeta2 * zeta3 - 7.0 * zsqr3 - 8.0 * zeta3 * zeta4 - zsqr4;
			z = 17.0 * zsqr3 + 6.0 * zeta2 * zeta4 + 6.0 * zeta3 * zeta4 + zsqr4;

			// collect results
			aux = x + 4.0 * y * cos(2.0 * alpha) + z * cos(4.0 * alpha);
			result = gsl_pow_int(gcosmo->tugi_align / 14.0,2) / 144.0 * aux;

			break;
		
	}
	return(result);
}
/* ---  --- */


/* --- function ellipticity_cc [3d <exex> correlation function] --- */
double ellipticity_gi_cc(double r,double alpha)
{
	double aux,result;
	double x,y;
	double zeta2,zeta3,zeta4;
	double zsqr2,zsqr3,zsqr4;

	// evaluate splines
	zeta2 = gsl_spline_eval(gcosmo->spline_z2p,r,gcosmo->acc_z2p);
	zeta3 = gsl_spline_eval(gcosmo->spline_z3p,r,gcosmo->acc_z3p);
	zeta4 = gsl_spline_eval(gcosmo->spline_z4p,r,gcosmo->acc_z4p);

	// squares
	zsqr2 = gsl_pow_int(zeta2,2);
	zsqr3 = gsl_pow_int(zeta3,2);
	zsqr4 = gsl_pow_int(zeta4,2);

	switch(tconf->morphology)
	{
		case ellipse:
			result = 4.0 * gsl_pow_int(gcosmo->tugi_hooke,2) * (zeta2 + gsl_pow_int(sin(alpha),2) * zeta3);
			break;

		case spiral:
			// compute contributions
			x = 42.0 * zsqr2 + 59.0 * zeta2 * zeta3 + 13.0 * zsqr3 + 5.0 * zeta2 * zeta4 + zeta3 * zeta4;
			y = 9.0 * zeta2 * zeta3 + 5.0 * zsqr3 + 3.0 * zeta2 * zeta4 - zeta3 * zeta4;

			// collect results
			aux = x + y * cos(2.0 * alpha);
			result = gsl_pow_int(gcosmo->tugi_align / 14.0,2) / 18.0 * aux;
			break;
	}
	return(result);
}
/* ---  --- */


// function ellipticity_sp [3d <e+es> correlation function]
double ellipticity_gi_sp(double r,double alpha)
{
	double aux,result;
	double x,y,z;
	double zeta2,zeta3,zeta4;
	double zsqr2,zsqr3,zsqr4;

	// evaluate splines
	zeta2 = gsl_spline_eval(gcosmo->spline_z2p,r,gcosmo->acc_z2p);
	zeta3 = gsl_spline_eval(gcosmo->spline_z3p,r,gcosmo->acc_z3p);
	zeta4 = gsl_spline_eval(gcosmo->spline_z4p,r,gcosmo->acc_z4p);

	// squares
	zsqr2 = gsl_pow_int(zeta2,2);
	zsqr3 = gsl_pow_int(zeta3,2);
	zsqr4 = gsl_pow_int(zeta4,2);

	switch(tconf->morphology)
	{
		case ellipse:
			result = -6.0 * gsl_pow_int(sin(alpha),2) * zeta3 - gsl_pow_int(sin(alpha),4) * zeta4;
			result *= gcosmo->tugi_hooke*gcosmo->tugi_hooke;
			break;
		case spiral:
			// compute contributions
			x = 7.0 * zsqr4 + (50.0 * zeta3 + 66.0 * zeta2) * zeta4 + 223.0 * zsqr3 + 408.0 * zeta2 * zeta3;
			y = 4.0 * zsqr4 + (32.0 * zeta3 + 48.0 * zeta2) * zeta4 + 172.0 * zsqr3 + 408.0 * zeta2 * zeta3;
			z = 3.0 * zsqr4 + (18.0 * zeta3 + 18.0 * zeta2) * zeta4 + 51.0 * zsqr3;
			
			// collect results
			aux = x - y * cos(2.0 * alpha) - z * cos(4.0 * alpha);
			result = gsl_pow_int(gcosmo->tugi_align / 14.0,2) / 108.0 * fabs(aux);
			break;
	}
	return(result);
}

// function ellipticity_ss [3d <eses> correlation function]
double ellipticity_gi_ss(double r,double alpha)
{
	double aux,result;
	double x,y,z;
	double zeta2,zeta3,zeta4;
	double zsqr2,zsqr3,zsqr4;

	// evaluate splines
	zeta2 = gsl_spline_eval(gcosmo->spline_z2p,r,gcosmo->acc_z2p);
	zeta3 = gsl_spline_eval(gcosmo->spline_z3p,r,gcosmo->acc_z3p);
	zeta4 = gsl_spline_eval(gcosmo->spline_z4p,r,gcosmo->acc_z4p);

	// squares
	zsqr2 = gsl_pow_int(zeta2,2);
	zsqr3 = gsl_pow_int(zeta3,2);
	zsqr4 = gsl_pow_int(zeta4,2);

	switch(tconf->morphology)
	{
		case ellipse:
			result = 8.0 * zeta2 + 8.0 * gsl_pow_int(sin(alpha),2) * zeta3 + gsl_pow_int(sin(alpha),4) * zeta4;
			result *= gcosmo->tugi_hooke*gcosmo->tugi_hooke;
			break;
			
		case spiral:
			// compute contributions
			x = 59.0 * zsqr4 + (490.0 * zeta3 + 426.0 * zeta2) * zeta4 + 1907.0 * zsqr3 + 3864.0 * zeta2 * zeta3 + 2448.0 * zsqr2;
			y = 60.0 * zsqr4 + (480.0 * zeta3 + 288.0 * zeta2) * zeta4 + 1284.0 * zsqr3 + 1800.0 * zeta2 * zeta3;
			z = 9.0 * zsqr4 + (54.0 * zeta3 + 54.0 * zeta2) * zeta4 + 153.0 * zsqr3;

			// collect results
			aux = x + y * cos(2.0 * alpha) + z * cos(4.0 * alpha);
			result = gsl_pow_int(gcosmo->tugi_align / 14.0,2) / 324.0 * aux;
			break;
	}
	return(result);
}
// ---  ---


/* --- function compute_zeta_coefficients --- */
int COMPUTE_ZETA_COEFFICIENTS(struct DATA *data)
{
	int i;
	double aux;


	printf("tomo_ia: alignment: computing zeta-coefficients ... ");

	
	switch(tconf->morphology)
	{
		case ellipse:
		aux = 1.0;
		break;

		case spiral:
		aux = spirou_zeta_norm();
		break;
	}
#pragma omp parallel
	{
	double r;
#pragma omp for
	for(i=0;i<CSTEP;i++)
		{
			r = data->r[i] = chistep(i);
			data->zeta2[i] = spirou_zeta(r,2) / aux;
			data->zeta3[i] = spirou_zeta(r,3) / aux;
			data->zeta4[i] = spirou_zeta(r,4) / aux;
		}
	}
	gsl_spline_init(gcosmo->spline_z2,data->r,data->zeta2,CSTEP);
	gsl_spline_init(gcosmo->spline_z3,data->r,data->zeta3,CSTEP);
	gsl_spline_init(gcosmo->spline_z4,data->r,data->zeta4,CSTEP);

	printf("done!\n");

	return(0);
}
/* ---  --- */

int COMPUTE_ZETAPRIME_COEFFICIENTS(struct DATA *data)
{
	int i;
	double aux;


	printf("tomo_ia: alignment: computing zetaprime-coefficients....");

	
	switch(tconf->morphology)
	{
		case ellipse:
		aux = 1.0;
		break;

		case spiral:
		aux = spirou_zeta_norm();
		break;
	}
#pragma omp parallel
	{
	double r;
#pragma omp for
	for(i=0;i<CSTEP;i++)
		{
			r = data->r[i] = chistep(i);
			data->zeta2[i] = tugi_zetaprime(r,2) / aux;
			data->zeta3[i] = tugi_zetaprime(r,3) / aux;
			data->zeta4[i] = tugi_zetaprime(r,4) / aux;
		}
	}
	gsl_spline_init(gcosmo->spline_z2p,data->r,data->zetaprime2,CSTEP);
	gsl_spline_init(gcosmo->spline_z3p,data->r,data->zetaprime3,CSTEP);
	gsl_spline_init(gcosmo->spline_z4p,data->r,data->zetaprime4,CSTEP);

	printf("done!\n");

	return(0);
}