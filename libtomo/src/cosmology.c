#include <tomo.h>

/* --- function init_cosmology [initialises cosmological model + numerical parameters] --- */
int INIT_COSMOLOGY(struct DATA *data)
{
	printf("tomo_ia: cosmology: initialising cosmologcal model....\n");
	// cosmology

	SET_FIDUCIAL_COSMOLOGY(data);

	data->cosmology->anorm = 1.0;		// anorm (Amplitude of Power Spectrum, NOT SCALE FACTOR YOU RETARD!!)
	
	data->cosmology->rsmooth = 8.0;			// sigma8-smoothing

	data->cosmology->rhocrit0 = 3 * gsl_pow_int(spirou_hubble * data->cosmology->h,2) / (8 * pi * tugi_G);



	switch(tconf->surveytype)
	{
		case euclid:
			data->cosmology->zmean = 0.64;			// median of 0.9
			data->cosmology->beta = 1.5;
			data->cosmology->f_sky = 0.5;
			data->cosmology->nmean = 4.727e8;
			data->cosmology->pgal_prenorm = gsl_sf_gamma(3.0/data->cosmology->beta);
			break;
		// CFHTLenS
		case cfhtlens:
			
			data->cosmology->zmean = 0.7;
			data->cosmology->beta = 1.5;
			data->cosmology->f_sky = 0.003730637772;//100.0/0.003731;
			//ATTENTION: NUMBER OF GALAXIES PER STERAD,*NOT* PER ARCMIN^2
			data->cosmology->nmean = 11.0*(180.0/pi)*(180.0/pi)*60*60;//11.0*(180.0*60.0/pi)*(180.0*60.0/pi);	
			data->cosmology->pgal_prenorm = gsl_sf_gamma(3.0/data->cosmology->beta);
			break;

		// Kiessling Sunglass
		case sunglass:	
			data->cosmology->zmean = 0.7;
			data->cosmology->beta = 1.5;
			data->cosmology->f_sky = 0.002424066129;
			//ATTENTION: NUMBER OF GALAXIES PER STERAD,*NOT* PER ARCMIN^2
			//data->cosmology->nmean = 11.0*(180.0*60.0/pi)*(180.0*60.0/pi);
			//For arc min**2:
			data->cosmology->nmean = 30.0*(180.0/pi)*(180.0/pi);			
			data->cosmology->pgal_prenorm = gsl_sf_gamma(3.0/data->cosmology->beta);
			break;

		// Test fuer Robert
		case robert_test:	
			data->cosmology->zmean = 0.9;
			data->cosmology->beta = 1.5;
			data->cosmology->f_sky = 0.5;
			//ATTENTION: NUMBER OF GALAXIES PER STERAD,*NOT* PER ARCMIN^2
			//data->cosmology->nmean = 11.0*(180.0*60.0/pi)*(180.0*60.0/pi);
			//For arc min**2:
			data->cosmology->nmean = 40.0*(180.0/pi)*(180.0/pi)*60.0*60.0;			
			data->cosmology->pgal_prenorm = gsl_sf_gamma(3.0/data->cosmology->beta);
			break;

	}

	// JPAS: alternative setting
	data->cosmology->ellipticity = gsl_pow_int(spirou_shape,2);

	// smoothing scale (for zeta-functions, on the spatial scale corresponding to 1e11 Msolar/h)

	data->cosmology->rsmoothscale_ellipse = 
	pow(3.0/(4.0 * pi ) * cdm_halo_mass*1e10 / (data->cosmology->omega_m * spirou_rhocrit) , 0.3333);
	
	data->cosmology->rsmoothscale_spiral = 
	pow(1e11 / (4.0 * pi) * 1.0 / (data->cosmology->omega_m * spirou_rhocrit) , 0.3333);

	switch(tconf->morphology)
	{

		case ellipse:
			data->cosmology->rsmoothscale = data->cosmology->rsmoothscale_ellipse;		
		break;

		case spiral:
			data->cosmology->rsmoothscale = data->cosmology->rsmoothscale_spiral;
		break;
		
	}
	return(0);
}
/* --- end of function init_cosmology --- */


// --- function set_fiducial cosmology ---
int SET_FIDUCIAL_COSMOLOGY(struct DATA *data)
{
	printf("tomo_ia: cosmology: setting fiducial cosmological model....\n");

	data->cosmology->omega_m = conf_Omega_m;		// matter density
	data->cosmology->omega_q = 1.0 - data->cosmology->omega_m; //flatness

	data->cosmology->omega_b = conf_Omega_B;		// baryonic density, for shape parameter
	data->cosmology->sigma8 = conf_sigma8;			// fluctuation amplitude
	data->cosmology->n_s = conf_ns;			// spectral index
	data->cosmology->h = conf_h;			// hubble parameter, for shape parameter
	data->cosmology->w = conf_w;			// de eos parameter

	data->cosmology->wprime = conf_wprime;			// time evolution of de eos

	// Alignment Stuff
	data->cosmology->tugi_align = spirou_align;
	data->cosmology->tugi_hooke = hooke;
	// Weird Hilbert et al normalisation: gsl_pow_int(1/spirou_dhubble,2) * 3.0/2.0 * gcosmo->omega_m * hooke * hooke;
	
	// Gravitational slip stuff
	data->cosmology->tugi_mu = 1.0;
	data->cosmology->tugi_eta = 1.0;
	// smoothing scale (for zeta-functions, on the spatial scale corresponding to 1e11 Msolar/h)

	data->cosmology->sepIA_pbb = sep_pbb;
	data->cosmology->sepIA_pbr = 1.0-sep_prr;
	data->cosmology->sepIA_prr = sep_prr;
	data->cosmology->sepIA_prb = 1.0-sep_pbb;
	
	tconf->whichalpha=alphaM;
	tconf->alphapm=neutral;

	return(0);
}
// ---  ---


/* --- function prepare_new_cosmology --- */
int PREPARE_NEW_COSMOLOGY(struct DATA *data)
{
	
	double aux;

	printf("tomo_ia: cosmology: preparing a new cosmological setup....\n");
	gcosmo->omega_q = 1.0 - gcosmo->omega_m;
	
	gcosmo->anorm = 1.0;
	gcosmo->anorm = gcosmo->sigma8 / sigma8();
	ALLOCATE_INTERPOLATION_MEMORY(data);
	COMPUTE_EVOLUTION(data);
	COMPUTE_CDM_PARAMETERS(data);
	COMPUTE_COMOVING_DISTANCE(data);

	return(0);
}

/* --- function recalc_cosmology --- */
int RECALC_COSMOLOGY(struct DATA *data)
{
	printf("tomo_ia: cosmology: recalculating cosmological setup....\n");
	gcosmo->omega_q = 1.0 - gcosmo->omega_m;

	gcosmo->anorm = 1.0;
	gcosmo->anorm = gcosmo->sigma8 / sigma8();
	
	ALLOCATE_INTERPOLATION_MEMORY(data);
	COMPUTE_EVOLUTION(data);
	COMPUTE_CDM_PARAMETERS(data);
	COMPUTE_COMOVING_DISTANCE(data);
	
	switch(tconf->mode)
	{
		case mode_gravslip:
			COMPUTE_ZETA_COEFFICIENTS(data);
			COMPUTE_ZETAPRIME_COEFFICIENTS(data);
			COMPUTE_ELLIPTICITY_SPECTRA(data);
			break;
		case mode_derivs:
			if(tconf->morphology==both) COMPUTE_BOTH_ELLIPTICITY_SPECTRA(data);			
			else
			{
				COMPUTE_ZETA_COEFFICIENTS(data);
				COMPUTE_ZETAPRIME_COEFFICIENTS(data);
				COMPUTE_ELLIPTICITY_SPECTRA(data);
			}
			break;
		case mode_sep_aDfisher:
			COMPUTE_BOTH_ELLIPTICITY_SPECTRA(data);			
			break;
		case mode_victor:
			COMPUTE_ZETA_COEFFICIENTS(data);
			COMPUTE_ZETAPRIME_COEFFICIENTS(data); // GI
			COMPUTE_ELLIPTICITY_SPECTRA(data);
			break;
	}

	return(0);
}


/* ---  --- */


/* --- function allocate_interpolation_memory [allocate memory for spline interpolation] --- */
int ALLOCATE_INTERPOLATION_MEMORY(struct DATA *data)
{
	printf("tomo_ia: cosmology: allocating memory for interpolation functions....\n");

	// growth function and derivative
	gcosmo->acc_dplus = gsl_interp_accel_alloc();
	gcosmo->spline_dplus = gsl_spline_alloc(gsl_interp_cspline,ASTEP);
	gcosmo->acc_a = gsl_interp_accel_alloc();
	gcosmo->spline_a = gsl_spline_alloc(gsl_interp_cspline,ASTEP);

	// zeta-splines for angular momentum
	gcosmo->acc_z2 = gsl_interp_accel_alloc();
	gcosmo->spline_z2 = gsl_spline_alloc(gsl_interp_cspline,CSTEP);
	gcosmo->acc_z3 = gsl_interp_accel_alloc();
	gcosmo->spline_z3 = gsl_spline_alloc(gsl_interp_cspline,CSTEP);
	gcosmo->acc_z4 = gsl_interp_accel_alloc();
	gcosmo->spline_z4 = gsl_spline_alloc(gsl_interp_cspline,CSTEP);

	// zetaprime-splines for angular momentum
	gcosmo->acc_z2p = gsl_interp_accel_alloc();
	gcosmo->spline_z2p = gsl_spline_alloc(gsl_interp_cspline,CSTEP);
	gcosmo->acc_z3p = gsl_interp_accel_alloc();
	gcosmo->spline_z3p = gsl_spline_alloc(gsl_interp_cspline,CSTEP);
	gcosmo->acc_z4p = gsl_interp_accel_alloc();
	gcosmo->spline_z4p = gsl_spline_alloc(gsl_interp_cspline,CSTEP);

	// nonlinear CDM spectrum --> kept fixed
	gcosmo->acc_ksigma = gsl_interp_accel_alloc();
	gcosmo->spline_ksigma = gsl_spline_alloc(gsl_interp_cspline,ASTEP);
	gcosmo->acc_nslope = gsl_interp_accel_alloc();
	gcosmo->spline_nslope = gsl_spline_alloc(gsl_interp_cspline,ASTEP);
	gcosmo->acc_curv = gsl_interp_accel_alloc();
	gcosmo->spline_curv = gsl_spline_alloc(gsl_interp_cspline,ASTEP);

	return(0);
}
/* --- end of function allocate_interpolation_memory --- */


/* --- function free_interpolation_memory --- */
int FREE_INTERPOLATION_MEMORY(struct DATA *data)
{
	printf("tomo_ia: cosmology: deallocating memory for interpolation functions....\n");

	// growth function and derivative
	gsl_interp_accel_free(gcosmo->acc_dplus);
	gsl_spline_free(gcosmo->spline_dplus);
	gsl_interp_accel_free(gcosmo->acc_a);
	gsl_spline_free(gcosmo->spline_a);

	gsl_interp_accel_free(gcosmo->acc_z2);
	gsl_spline_free(gcosmo->spline_z2);
	gsl_interp_accel_free(gcosmo->acc_z3);
	gsl_spline_free(gcosmo->spline_z3);
	gsl_interp_accel_free(gcosmo->acc_z4);
	gsl_spline_free(gcosmo->spline_z4);
	gsl_interp_accel_free(gcosmo->acc_z2p);
	gsl_spline_free(gcosmo->spline_z2p);
	gsl_interp_accel_free(gcosmo->acc_z3p);
	gsl_spline_free(gcosmo->spline_z3p);
	gsl_interp_accel_free(gcosmo->acc_z4p);
	gsl_spline_free(gcosmo->spline_z4p);

	// nonlinear CDM spectrum
	gsl_interp_accel_free(gcosmo->acc_ksigma);
	gsl_spline_free(gcosmo->spline_ksigma);
	gsl_interp_accel_free(gcosmo->acc_nslope);
	gsl_spline_free(gcosmo->spline_nslope);
	gsl_interp_accel_free(gcosmo->acc_curv);
	gsl_spline_free(gcosmo->spline_curv);

	return(0);
}
/* --- end of function free_interpolation_memory --- */


/* --- function compute_evolution --- */
int COMPUTE_EVOLUTION(struct DATA *data)
{
	int i;
	double a;

	printf("tomo_ia: cosmology: computing accelerators for computing source terms....\n");

	// no parallelisation due to differential equation
	for(i=0;i<ASTEP;i++)
	{
		// printf("i= %i, ",i);
		data->a[i] = a = astep(i);
		// printf("a done, ");
		data->dp[i] = d_plus(a);
		// printf("dp done, ");
		data->chi[i] = a2com(a);
		// printf("chi done!\n");	
	}

	gsl_spline_init(gcosmo->spline_dplus,data->a,data->dp,ASTEP);

	return(0);
}
/* --- end of function compute_evolution --- */


/* --- function compute_comoving_distance --- */
int COMPUTE_COMOVING_DISTANCE(struct DATA *data)
{
	int i;
	double temp[ASTEP];
	
	printf("tomo_ia: cosmology: swapping (chi,a)-pairs....\n");

	// swap chi
	for(i=0;i<ASTEP;i++)
	{
		temp[i] = data->chi[i];
	}

	for(i=0;i<ASTEP;i++)
	{
		data->chi[i] = temp[ASTEP-i-1];
	}

	// swap a
	for(i=0;i<ASTEP;i++)
	{
		temp[i] = data->a[i];
	}

	for(i=0;i<ASTEP;i++)
	{
		data->a[i] = temp[ASTEP-i-1];
	}

	gsl_spline_init(gcosmo->spline_a,data->chi,data->a,ASTEP);

	return(0);
}
/* --- end of function compute_comoving distance --- */

/* --- function astep [linear equidistant stepping in scale factor a] --- */
double astep(int i)
{
	double result,delta;

	delta = (AMAX - AMIN) / ((double)(ASTEP-1));
	result = AMIN + delta * i;

	return(result);
}
/* --- end of function astep --- */


/* --- function lstep --- */
double lstep(int i)
{
	double result,delta;

	delta = pow(LMAX / LMIN,1.0 / ((double)(LSTEP - 1)));;
	result = LMIN * pow(delta,(double)i);

	return(result);
}
/* --- end of function lstep --- */


/* --- function a2z --- */
double a2z(double a)
{
	double result;

	result = 1.0 / a - 1.0;

	return(result);
}
/* --- end of function a2z --- */


/* --- function z2a --- */
double z2a(double z)
{
	double result;

	result = 1.0 / (1.0 + z);

	return(result);
}
/* --- end of function z2a --- */


/* --- function a2com [comoving distance in Mpc/h] --- */
double a2com(double a)
{
	double result,error;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	F.function = &aux_dcom;
	F.params = NULL;

	gsl_integration_qag(&F,1.0,a,0.0,smalleps,NEVAL,GSL_INTEG_GAUSS61,w,&result,&error);

	gsl_integration_workspace_free(w);

	return(result);
}
/* --- end of function a2com --- */


/* --- function aux_dcom [auxiliary function for a2com] --- */
double aux_dcom(double a,void *params)
{
	double result;

	result = - spirou_dhubble / gsl_pow_int(a,2) / hubble(a);
	//result = 1/(sqrt(2*pi)) * exp(0.5*-gsl_pow_int(a,2));

	return(result);
}
/* --- end of function aux_dcom --- */


// ---  ---
double hubble(double a)
{
	double aux,result,error;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	F.function = &aux_dhubble;
	F.params = NULL;

	gsl_integration_qag(&F,a,1.0,0.0,eps,NEVAL,GSL_INTEG_GAUSS21,w,&aux,&error);
	gsl_integration_workspace_free(w);

	result = sqrt(gcosmo->omega_m / gsl_pow_int(a,3) + (1.0 - gcosmo->omega_m) * exp(3.0 * aux));

	return(result);
}


// ---  ---
double aux_dhubble(double a,void *params)
{
	double result;
	
	result = (1.0 + w_de(a)) / a;

	return(result);
}


// ---  ---
double deceleration(double a)
{
	double result;

	result = -(1.0 + dhubble(a));

	return(result);
}
// ---  ---


// --- derivative dlnH/dlna ---
double dhubble(double a)
{
	double aux,error,result;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	F.function = &aux_dhubble;
	F.params = NULL;

	gsl_integration_qag(&F,a,1.0,0.0,eps,NEVAL,GSL_INTEG_GAUSS21,w,&aux,&error);
	gsl_integration_workspace_free(w);

	result = gcosmo->omega_m / gsl_pow_int(a,3) +
		(1.0 - gcosmo->omega_m) * (1.0 + w_de(a)) * exp(3.0 * aux);

	result /= gcosmo->omega_m / gsl_pow_int(a,3) +
		(1.0 - gcosmo->omega_m) * exp(3.0 * aux);

	result *= -1.5;

	return(result);
}
// ---  ---


// ---  ---
double omega(double a)
{
	double aux,result;

	aux = hubble(a);
	result = gcosmo->omega_m / gsl_pow_int(a,3) / gsl_pow_int(aux,2);

	return(result);
}
// ---  ---


/* --- d_plus_function [defines f0 = dy1/da and f1 = dy2/da] --- */
int d_plus_function(double t, const double y[], double f[],void *params)
{
	double aux1,aux2;

	// determine coefficients
	aux1 = 1.5 * omega(t) / gsl_pow_int(t,2);
	aux2 = (deceleration(t) - 2.0) / t;

	/* derivatives f_i = dy_i/dt */
	f[0] = y[1];
	f[1] = aux1 * y[0] + aux2 * y[1];

	return(GSL_SUCCESS);
}
/* --- end of function dplus_function --- */


/* --- function dplus --- */
double aux_d_plus(double a,double *result_d_plus,double *result_d_plus_prime)
{
	double result;
	int status;
	const gsl_odeiv2_step_type *T = gsl_odeiv2_step_rk8pd;
	gsl_odeiv2_step *s = gsl_odeiv2_step_alloc(T, 2);
	gsl_odeiv2_control *c = gsl_odeiv2_control_y_new(1e-6, 0.0);
	gsl_odeiv2_evolve *e = gsl_odeiv2_evolve_alloc(2);
	double t = 1e-2,h = 1e-4;		// initial scale factor and accuracy
	double y[2] = {1.0,0.0};		// initial conditions

	gsl_odeiv2_system sys = {d_plus_function, NULL, 2, NULL};

	while(t < a)
	{
		status = gsl_odeiv2_evolve_apply(e, c, s, &sys, &t, a, &h, y);
		if (status != GSL_SUCCESS) break;
	}

	result = *result_d_plus = y[0];		/* d_plus */
	*result_d_plus_prime = y[1];		/* d(d_plus)/da */

	gsl_odeiv2_evolve_free(e);
	gsl_odeiv2_control_free(c);
	gsl_odeiv2_step_free(s);

	return(result);
}
/* --- end of function dplus --- */


/* --- function d_plus --- */
double d_plus(double a)
{
	double dummy,norm,result;

	aux_d_plus(a,&result,&dummy);
	aux_d_plus(1.0,&norm,&dummy);

	result /= norm;

	return(result);
}
/* --- end of function d_plus --- */


/* --- function w [dark energy eos parameterisation] --- */
double w_de(double a)
{
	double result;

	result = gcosmo->w + (1.0 - a) * gcosmo->wprime;

	return(result);
}
/* --- end of function w --- */


/* --- function cdm_spectrum --- */
double cdm_spectrum(double k)
{
	double result,aux;

	/*
	switch (tconf->baryon_transfer)
	{
		case off:
			aux = cdm_transfer(k);
		break;
		case on:
			aux = baryon_transfer(k);
		break;
	}
	*/

	aux = cdm_transfer(k);
	result = pow(k,gcosmo->n_s) * gsl_pow_int((gcosmo->anorm * aux),2);

	return(result);
}
/* --- end of function cdm_spectrum --- */


/* ---  --- */
double cdm_potential(double k)
{
	double a,aux,x,unit,result;

//Gravitational Slip parameter Sigma:
	double tugi_sigma = gcosmo->tugi_mu*(1+gcosmo->tugi_eta);

	a = gcosmo->n_s - 4.0;

	x = pow(k,a);
	switch (tconf->baryon_transfer)
	{
		case off:
			aux = gsl_pow_int(cdm_transfer(k),2);
		break;

		case on:
			aux = gsl_pow_int(baryon_transfer(k),2);
		break;
	}

	unit = 1.5 * gcosmo->omega_m / gsl_pow_int(spirou_dhubble,2);

	if(tconf->mode==mode_gravslip) result = gsl_pow_int(tugi_sigma,2) * gsl_pow_int(gcosmo->anorm * unit,2) * x * aux;
	else result = gsl_pow_int(gcosmo->anorm * unit,2) * x * aux;

	return(result);
}
/* ---  --- */


/* --- function cdm_transfer [Bardeen - CDM transfer function] --- */
double cdm_transfer(double k)
{
	double result,q,shape,fb,aux;
	double alpha = 2.34, beta = 3.89, gamma = 16.1, delta = 5.46, epsilon = 6.71;

	fb = gcosmo->omega_b / gcosmo->omega_m;
	shape = gcosmo->omega_m * gcosmo->h * exp(-gcosmo->omega_b - sqrt(2.0*gcosmo->h) * fb);
	q = k / shape;

	aux = 1.0 + beta * q + gsl_pow_int(gamma * q,2) + gsl_pow_int(delta * q,3) + gsl_pow_int(epsilon * q,4);
	result = gsl_sf_log(1.0 + alpha * q) / (alpha * q) * pow(aux,-1.0/4.0);

	return(result);
}
/* --- end of function cdm_transfer --- */

double baryon_transfer(double k)
{
	printf("Baryon Transfer happening, attention!!");
	double result;
	double rk,e,thet,thetsq,thetpf,b1,b2,zd,ze,rd,re,rke,s,rks,q,y,g;
	double ab,a1,a2,ac,bc,f,c1,c2,tc,bb,bn,ss,tb,tk_eh;
	double hsq,om_mhsq,om_b,om_m;
	double fb,x,q2;
	
	// convert k to Mpc-1 rather than hMpc-1
	rk = k * gcosmo->h;
	hsq = gsl_pow_int(gcosmo->h,2);
	om_mhsq = gcosmo->omega_m * hsq;
	fb = gcosmo->omega_b / gcosmo->omega_m;
	
	// constants
	e = exp(1.0);
	thet = 2.728 / 2.7;
	thetsq = gsl_pow_int(thet,2);
	thetpf = gsl_pow_int(thetsq,2);
	
	// Equation 4 - redshift of drag epoch
	b1 = 0.313 * pow(om_mhsq,-0.419) * (1.0 + 0.607 * pow(om_mhsq,0.674));
	b2 = 0.238 * pow(om_mhsq,0.223);
	zd = 1291.* (1.0 + b1 * pow(gcosmo->omega_b * hsq,b2)) * 
		pow(om_mhsq,0.251) / (1.0 + 0.659 * pow(om_mhsq,0.828));
	
	// Equation 2 - redshift of matter-radiation equality
	ze = 2.50e4 * om_mhsq / thetpf;
	
	// value of R=(ratio of baryon-photon momentum density) at drag epoch
	rd = 31500.0 * gcosmo->omega_b * hsq / (thetpf * zd);
	
	// value of R=(ratio of baryon-photon momentum density) at epoch of matter-radiation equality
	re = 31500.0 * gcosmo->omega_b * hsq / (thetpf * ze);
	
	// Equation 3 - scale of ptcle horizon at matter-radiation equality
	rke = 7.46e-2 * om_mhsq / thetsq;
	
	// Equation 6 - sound horizon at drag epoch
	s = (2.0 / 3.0 / rke) * sqrt(6.0 / re) * log((sqrt(1.0 + rd) + sqrt(rd + re)) / (1.0 + sqrt(re)));
	
	// Equation 7 - silk damping scale
	rks = 1.6 * pow(gcosmo->omega_b * hsq,0.52) * pow(om_mhsq,0.73) * (1.0 + pow(10.4 * om_mhsq,-0.95));
	
	// Equation 10  - define q
	q = rk / 13.41 / rke;
	
	// Equations 11 - CDM transfer function fits
	a1 = pow(46.9 * om_mhsq,0.670) * (1.0 + pow(32.1 * om_mhsq,-0.532));
	a2 = pow(12.0 * om_mhsq,0.424) * (1.0 + pow(45.0 * om_mhsq,-0.582));
	ac = pow(a1,(-fb)) * pow(a2,gsl_pow_int(-(fb),3));
	
	// Equations 12 - CDM transfer function fits
	b1 = 0.944 / (1.0 + pow(458.0 * om_mhsq,-0.708));
	b2 = pow(0.395 * om_mhsq,-0.0266);
	bc = 1.0 / (1.0 + b1 * (pow(1.0 - fb,b2) - 1.0));
	
	// Equation 18
	f = 1.0 / (1.0 + gsl_pow_int(rk * s / 5.4,4));
	
	// Equation 20
	c1 = 14.2 + 386.0 / (1.0 + 69.9 * pow(q,1.08));
	c2 = 14.2 / ac + 386.0 / (1.0 + 69.9 * pow(q,1.08));
	
	// Equation 17 - CDM transfer function
	q2 = gsl_pow_int(q,2);
	x = log(e + 1.8 * bc * q);
	tc = f * x / (x + c1 * q2) + (1.0 - f) * x / (x + c2 * q2);
	
	// Equation 15
	y = (1.0 + ze) / (1.0 + zd);
	g = y * (-6.0 * sqrt(1.0 + y) + (2.0 + 3.0 * y) * log((sqrt(1.0 + y) + 1.0) / (sqrt(1.0 + y) - 1.0)));
	
	// Equation 14
	ab = g * 2.07 * rke * s / pow(1.0 + rd,0.75);
	
	// Equation 23
	bn = 8.41 * pow(om_mhsq,0.435);
	
	// Equation 22
	ss = s / pow(1.0 + gsl_pow_int(bn / rk / s,3),1.0 / 3.0);
	
	// Equation 24
	bb = 0.5 + (fb) + (3.0 - 2.0 * fb) * sqrt(gsl_pow_int(17.2 * om_mhsq,2) + 1.0);
	
	// Equations 19 & 21
	tb = log(e + 1.8 * q) / (log(e + 1.8 * q) + c1 *q2) / (1.0 + gsl_pow_int(rk * s /5.2,2));
	tb = (tb + ab * exp(-pow(rk / rks,1.4)) / (1.0 + gsl_pow_int(bb / rk / s,3))) * sin(rk * ss) / rk / ss;
	
	// Equation 8
	result = fb * tb + (1.0 - fb) * tc;
	
	return(result);
}
/* ---  --- */

/* --- function sigma8 --- */
double sigma8()
{
	double result,aux,error;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	F.function = &aux_dsigma8;
	F.params = NULL;

	gsl_integration_qagiu(&F,0.0,0.0,smalleps,NEVAL,w,&aux,&error);
	result = sqrt(aux / 2.0 / pi2);	

	gsl_integration_workspace_free(w);
	
	return(result);
}
/* --- end of function sigma8 --- */


/* --- function aux_dsigma8 --- */
double aux_dsigma8(double k,void *params)
{
	double result,window,aux;

	aux = gcosmo->rsmooth * k;
	window = 3.0 * gsl_sf_bessel_j1(aux) / aux;
	result = cdm_spectrum(k) * gsl_pow_int(k * window,2);

	return(result);
}
/* --- end of function aux_dsigma8 --- */


/* --- function cdm_smooth [CDM spectrum with a Gaussian cutoff on scale rsmoothscale] --- */
double cdm_smooth(double k)
{
	double result,smooth;
	
	// apply smoothing
	smooth = exp(-gsl_pow_int(k * gcosmo->rsmoothscale,2) / 2.0);
	switch(tconf->nonlinearPk)
	{
		case on:
			switch(tconf->morphology)
			{
				case ellipse:
					result = cdm_nonlinear(k, 1.0, 1.0) * gsl_pow_int(smooth,2);
					break;
				case spiral:
					result = cdm_spectrum(k) * gsl_pow_int(smooth,2);
					break;
			} 
			break;
			
		case off:
			result = cdm_spectrum(k) * gsl_pow_int(smooth,2);
			break;
	}
	return(result);
}
/* --- end of function cdm_smooth --- */

/* --- function compute_cdm_parameters [determines parameters for the nonlinear CDM spectrum] --- */
int COMPUTE_CDM_PARAMETERS(struct DATA *data)
{
	
	int i;
	printf("tomo_ia: cosmology: computing parameters for nonlinear CDM spectrum....\n");

#pragma omp parallel
	{
		double a,rs;

#pragma omp for
		for(i=0;i<ASTEP;i++)
		{
			a = astep(i);

			if(a < 0.1)
			{
				rs = 2.597919e-04;
			}
			else
			{
				rs = rscale(a);
			}

			data->ksigma[i] = 1.0 / rs;
			data->nslope[i] = -3.0 - rs / sigma2(rs) * dsigma2(rs);

			data->curv[i] = gsl_pow_int(rs / sigma2(rs) * dsigma2(rs),2)
				- rs / sigma2(rs) * dsigma2(rs)
				- gsl_pow_int(rs,2) / sigma2(rs) * ddsigma2(rs);
		}
	}

	gsl_spline_init(gcosmo->spline_ksigma,data->a,data->ksigma,ASTEP);
	gsl_spline_init(gcosmo->spline_nslope,data->a,data->nslope,ASTEP);
	gsl_spline_init(gcosmo->spline_curv,data->a,data->curv,ASTEP);

	return(0);
}
/* ---  --- */


/* --- double rscale [scale at which the power spectrum becomes unity, at time a] --- */
double rscale(double a)
{
	int status, iter=0, max_iter=100;
	const gsl_root_fsolver_type *T;
	gsl_root_fsolver *s;
	double rscale,x_lo = 0.0,x_hi = 10.0;
	gsl_function F;

	F.function = &aux_rscale;
	F.params = &a;

	T = gsl_root_fsolver_brent;
	s = gsl_root_fsolver_alloc(T);
	gsl_root_fsolver_set (s, &F, x_lo, x_hi);

	// printf("Iter start\n");

	do
	{
		iter++;
		// printf("Iter = %i\n", iter);
		status = gsl_root_fsolver_iterate(s);
		rscale = gsl_root_fsolver_root(s);
		x_lo = gsl_root_fsolver_x_lower(s);
		x_hi = gsl_root_fsolver_x_upper(s);
		status = gsl_root_test_interval(x_lo,x_hi,0,epsrel);
	} while (status == GSL_CONTINUE && iter < max_iter);
	// printf("Iter end\n");

	gsl_root_fsolver_free(s);
	// printf("rscale = %e\n",rscale);

	return(rscale);
}
/* --- end of function rscale --- */


/* --- function aux_rscale [root finding: smoothed cdm spectrum is unity] --- */
double aux_rscale(double rscale,void *params)
{
	double dp,result;
	double a = *(double *)params;

	dp = gsl_spline_eval(gcosmo->spline_dplus,a,gcosmo->acc_dplus);
	result = 1.0 - gsl_pow_int(dp,2) * sigma2(rscale);

	return(result);
}
/* --- end of function aux_rscale --- */


/* --- function sigma2 --- */
double sigma2(double rscale)
{
	double result,aux,error;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;
	F.function = &aux_sigma2;
	F.params = &rscale;
	gsl_integration_qagiu(&F,0,eps,eps,NEVAL,w,&aux,&error);
	result = aux / 2.0 / pi2;

	gsl_integration_workspace_free(w);

	return(result);
}
/* --- end of function sigma2 --- */


/* --- function aux_sigma2 --- */
double aux_sigma2(double k,void *params)
{
	double result,window,aux,rscale;

	rscale = *(double *)params;

	aux = rscale * k;
	window = exp(-gsl_pow_int(aux,2));
	result = cdm_spectrum_slope(k) * gsl_pow_int(k,2) * window;

	return(result);
}
/* --- end of function aux_sigma2 --- */


/* --- function dsigma2 --- */
double dsigma2(double rscale)
{
	double result,aux,error;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	F.function = &aux_dsigma2;
	F.params = &rscale;

	gsl_integration_qagiu(&F,0.0,eps,eps,NEVAL,w,&aux,&error);

	result = aux / pi2;

	gsl_integration_workspace_free(w);

	return(result);
}
/* --- end of function dsigma2 --- */


/* --- function aux_dsigma2 --- */
double aux_dsigma2(double k,void *params)
{
	double result,window,aux,rscale;

	rscale = *(double *)params;

	aux = rscale * k;
	window = -aux * exp(-gsl_pow_int(aux,2));
	result = cdm_spectrum_slope(k) * gsl_pow_int(k,3) * window;

	return(result);
}
/* --- end of function aux_dsigma2 --- */


/* --- function dsigma2 --- */
double ddsigma2(double rscale)
{
	double result,aux,error;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc(NEVAL);
	gsl_function F;

	F.function = &aux_ddsigma2;
	F.params = &rscale;

	gsl_integration_qagiu(&F,0.0,eps,eps,NEVAL,w,&aux,&error);


	result = aux / pi2;

	gsl_integration_workspace_free(w);

	return(result);
}
/* --- end of function dsigma2 --- */


/* --- function aux_ddsigma2 --- */
double aux_ddsigma2(double k,void *params)
{
	double result,window,aux,rscale;

	rscale = *(double *)params;

	aux = gsl_pow_int(rscale * k,2);
	window = exp(-aux) * (2.0 * aux - 1.0);
	result = cdm_spectrum_slope(k) * gsl_pow_int(k,4) * window;

	return(result);
}
/* --- end of function aux_ddsigma2 --- */


/* --- function cdm_nonlinear [nonlinear cdm spectrum by Smith et al., MNRAS 341, 1311 (2003)] --- */
double cdm_nonlinear(double k, double a, double dp)
{

	double an,bn,cn,y;
	double alpha,beta,gamma,mu,nu;
	double omega,delta_q,delta_h,delta;
	double c,n,n2,n3,n4;
	double aux,result;
	double prefac = 0;
	double exteta = 1;
	double extmu = 1;
	double mattertofield;

	if(a < ATRANSITION)
	{
		// linear CDM spectrum at early times, a<0.1
		result = cdm_spectrum(k);
	}	

	else
	{
	
		if(tconf->flag_extPk==on&&k>0.0005&&k<5.0)
		{
			exteta = gsl_spline_eval(gcosmo->spline_exteta[tconf->whichalpha][tconf->alphapm], k, gcosmo->acc_exteta[tconf->whichalpha][tconf->alphapm]);
			extmu = gsl_spline_eval(gcosmo->spline_extmu[tconf->whichalpha][tconf->alphapm], k, gcosmo->acc_extmu[tconf->whichalpha][tconf->alphapm]);
			mattertofield = gsl_pow_int(k,-4)*gsl_pow_int((3*gcosmo->omega_m)/(2*a*spirou_dhubble),2);
			
			switch(tconf->whichBardeen)
			{
				case BardeenPhi:
				prefac = gsl_pow_int(exteta,2);
				break;
				case BardeenPsi:
				prefac = gsl_pow_int(extmu,2)*gsl_pow_int(exteta,2);
				break;
				case BardeenWeyl:
				prefac = 0.25*gsl_pow_int(exteta*(extmu+1),2);
				break;
				case BardeenTugi:
				prefac = 0.5*extmu*(exteta*(mu+1));
				break;
			}
			prefac *= gsl_spline2d_eval(gcosmo->spline_extdplus[tconf->whichalpha][tconf->alphapm], k, a, gcosmo->acc_extdplus_k[tconf->whichalpha][tconf->alphapm], gcosmo->acc_extdplus_a[tconf->whichalpha][tconf->alphapm]);
			//printf("k = %f \n", k);
			result = prefac * gsl_spline_eval(gcosmo->spline_extPk[tconf->whichalpha][tconf->alphapm], k, gcosmo->acc_extPk[tconf->whichalpha][tconf->alphapm]);
			//if(result < 1) result = 0.0;
		}
		
		else
		{
			//printf("we're using the internal pk, k = %f \n", k);
			// coefficients, with time dependence
			n = gsl_spline_eval(gcosmo->spline_nslope,a,gcosmo->acc_nslope);
			c = gsl_spline_eval(gcosmo->spline_curv,a,gcosmo->acc_curv);
			y = k / gsl_spline_eval(gcosmo->spline_ksigma,a,gcosmo->acc_ksigma);

			n2 = gsl_pow_int(n,2);
			n3 = gsl_pow_int(n,3);
			n4 = gsl_pow_int(n,4);

			an = pow(10.0,1.4861 + 1.83693 * n + 1.67618 * n2 + 0.7940 * n3 + 0.1670756 * n4 - 0.620695 * c);
			bn = pow(10.0,0.9463 + 0.9466 * n + 0.3084 * n2 - 0.9400 * c);
			cn = pow(10.0,-0.2807 + 0.6669 * n + 0.3214 * n2 - 0.0793 * c);

			alpha = 1.38848 + 0.3701 * n - 0.1452 * n2;
			beta = 0.8291 + 0.9854 * n + 0.3401 * n2;
			gamma = 0.86485 + 0.2989 * n + 0.1631 * c;

			mu = pow(10.0,-3.54419 + 0.19086 * n);
			nu = pow(10.0,0.95897 + 1.2857 * n);

			// omega(a), for adiabatic fluids
			omega = gcosmo->omega_m / gsl_pow_int(a,3) / gsl_pow_int(hubble(a),2);

			// spectral computations
			delta = gsl_pow_int(k,3) * cdm_spectrum(k) / 2.0 / pi2 * gsl_pow_int(dp,2);

			delta_q = delta * pow(1.0 + delta,beta) / (1.0 + alpha * delta)
				* exp(-0.25 * y - 0.125 * gsl_pow_int(y,2));

			aux = an * pow(y,3.0 * pow(omega,-0.0307))
				/ (1.0 + bn * pow(y,pow(omega,-0.0585)) + pow(cn * pow(omega,0.0743) * y,3.0 - gamma));

			delta_h = aux / (1.0 + mu / y + nu / gsl_pow_int(y,2));

			aux = delta_q + delta_h;
			result = 2.0 * pi2 * aux / gsl_pow_int(k,3) / gsl_pow_int(dp,2);
			//printf("k = %f, Pk = %f \n", k, result);
		}

	return(result);
	}
}
/* --- end of function cdm_nonlinear --- */


// --- function cdm_nonlinear_transfer [returns the square of the nonlinear transfer function] ---
double cdm_nonlinear_transfer(double k,double a,double dp)
{
	double result;
	tconf->whichBardeen=BardeenWeyl;
	result = cdm_nonlinear(k,a,dp) / cdm_spectrum(k);

	return(result);
}
// ---  ---


/* --- function cdm_spectrum_slope --- */
double cdm_spectrum_slope(double k)
{
	double result;
	double aux;
	switch (tconf->baryon_transfer)
	{
		case off:
		aux = cdm_transfer(k);
		break;
		case on:
		aux = baryon_transfer(k);
		break;
	}
	result = k * gsl_pow_int(gcosmo->anorm * aux, 2);
	return(result);
}
/* ---  --- */
/* --- function chistep [log equidistant stepping in distance chi] --- */
double chistep(int i)
{
	double result,delta;

	delta = pow(CMAX / CMIN,1.0 / ((double)(CSTEP - 1)));;
	result = CMIN * gsl_pow_int(delta,(double)i);

	return(result);
}
/* --- end of function chistep --- */
