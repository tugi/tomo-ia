// Potato level input and output
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Output nicer: PIDs and creating dirs
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

// Math and parallelisation
#include <omp.h>
#include <math.h>

// GSL
#include <gsl/gsl_math.h>
#include <gsl/gsl_const_num.h>
#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_spline2d.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_statistics.h>

// tomo specific stuff:
// (!note! the order is fucking important)

// Configuration, #defines etc.
#include <tomo_config.h>

// Structures, such as DATA, COSMOLOGY etc. 
#include <tomo_struc.h> 

// Header files for functions
#include <cosmology.h>  
#include <lensing.h>
#include <correlation.h>
#include <alignment.h>
#include <bias.h>
#include <output.h>
#include <input.h>
