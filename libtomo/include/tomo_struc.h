// --- structures ---
struct TUGI_CONFIG
{
	char outputdir[MAXBUF];
	char mode_string[MAXBUF];
	char morph_string[MAXBUF];
	char survey_string[MAXBUF];
	char Pktype_string[MAXBUF];
	int myuid; // just to make sure

	int mode;
	int morphology;
	int surveytype;
	int nonlinearPk;
	int baryon_transfer;

	int whichalpha;
	int alphapm;
	int whichBardeen;
	
	int flag_gravslip;
	int flag_crossspectra;
	int flag_GIs;
	int flag_separation;
	int flag_extPk;

	char mode_c[MAXBUF];
	char morphology_c[MAXBUF];
	char surveytype_c[MAXBUF];
	char nonlinear_c[MAXBUF];
	char baryon_transfer_c[MAXBUF];
};

struct SCONFIG
{
	int a,b;
	int i,j;
	double l,k;
	double theta;
};

struct ZETAPARAM
{
	int n;
	double r;
	double k;
};

struct LPARAM
{
	double chi,chiprime;
	double theta, l, k, r;
	int index;
	int i,j,a,b,n;
};

struct COSMOLOGY
{
	double omega_m;			// cosmological parameters
	double omega_q;
	double omega_b;
	double h;
	double w,wprime;
	double n_s;
	double sigma8;
	double rsmooth;			// 8 Mpc for Sigma_8
	double rhocrit0;
	double rsmoothscale;		// Gaussian Cutoff for P(k)

	double rsmoothscale_ellipse;
	double rsmoothscale_spiral;

	double anorm;			//Workaround for the lib

	double zmean,beta,pgal_prenorm;		// observation: EUCLID
	double f_sky,nmean;
	double ellipticity;

	// Alignment parameters
	double tugi_align;
	double tugi_hooke;

	// Gravitational Slip parameters, use Planck XIV conventions of eta = Phi / Psi
	double tugi_eta, tugi_mu;
	double bardeen_phi, bardeen_psi; //Bardeen potentials

	//seperation misclass probs
	double sepIA_pbb, sepIA_prr, sepIA_prb, sepIA_pbr;

	gsl_interp_accel *acc_z2;	// moments II
	gsl_spline *spline_z2;
	gsl_interp_accel *acc_z3;
	gsl_spline *spline_z3;
	gsl_interp_accel *acc_z4;
	gsl_spline *spline_z4;

	gsl_interp_accel *acc_z2p;	// moments GI
	gsl_spline *spline_z2p;
	gsl_interp_accel *acc_z3p;
	gsl_spline *spline_z3p;
	gsl_interp_accel *acc_z4p;
	gsl_spline *spline_z4p;

	gsl_interp_accel *acc_pp;	// ellipticity correlation functions II
	gsl_spline 		*spline_pp;
	gsl_interp_accel *acc_cc;
	gsl_spline 		*spline_cc;

	gsl_interp_accel *acc_ss;
	gsl_spline 		*spline_ss;
	gsl_interp_accel *acc_sp;
	gsl_spline 		*spline_sp;

	gsl_interp_accel *acc_gi_pp[NBIN];	// ellipticity correlation functions GI
	gsl_spline 	*spline_gi_pp[NBIN];
	gsl_interp_accel *acc_gi_cc[NBIN];
	gsl_spline 	*spline_gi_cc[NBIN];

	gsl_interp_accel *acc_gi_ss[NBIN];
	gsl_spline 	*spline_gi_ss[NBIN];
	gsl_interp_accel *acc_gi_sp[NBIN];
	gsl_spline 	*spline_gi_sp[NBIN];


	gsl_interp_accel *acc_dplus;	// accelerators for growth
	gsl_spline *spline_dplus;
	gsl_interp_accel *acc_a;	// a(chi) inversion
	gsl_spline *spline_a;

	gsl_interp_accel *acc_ksigma;	// nonlinear CDM spectrum
	gsl_spline *spline_ksigma;
	gsl_interp_accel *acc_nslope;
	gsl_spline *spline_nslope;
	gsl_interp_accel *acc_curv;
	gsl_spline *spline_curv;

	gsl_interp_accel *acc_Cl[2][NBIN][NBIN];
	gsl_spline 	*spline_Cl[2][NBIN][NBIN];
	
	gsl_interp_accel *acc_extPk[2][3];
	gsl_spline 	*spline_extPk[2][3];
	gsl_interp_accel *acc_exteta[2][3];
	gsl_spline 	*spline_exteta[2][3];
	gsl_interp_accel *acc_extmu[2][3];
	gsl_spline 	*spline_extmu[2][3];
	
	gsl_interp_accel *acc_extdplus_a[2][3];
	gsl_interp_accel *acc_extdplus_k[2][3];
	gsl_spline2d 	*spline_extdplus[2][3];


	double lmax;
};

struct DATA
{
	struct COSMOLOGY *cosmology;

	double dp[ASTEP];		// tables for growth function and a(chi)
	double a[ASTEP];
	double chi[ASTEP];

	double tomo[NBIN+1];	// table for tomography bin in chi
	double tomo_z[NBIN+1];	// table for tomography bin in z

	double ksigma[ASTEP];		// nonlinear CDM spectrum
	double nslope[ASTEP];
	double curv[ASTEP];

	double c_noise[LMAX][NBIN][NBIN];	// cached noisy spectra for the covariances
	double noise_sep[LMAX][NBIN2][NBIN2];	// noise for separation
	double signal_sep[LMAX][NBIN2][NBIN2];	// noise for separation

	double ck_s2n[LMAX];			// s2n-ratio of the convergence spectrum
	double dck_s2n[LMAX];			// s2n-ratio of the convergence spectrum

	double ce_s2n[LMAX];			// s2n-ratios of the ellipticity E- and B-mode spectra
	double cb_s2n[LMAX];
	double cs_s2n[LMAX];			// <ss> alone
	double cc_s2n[LMAX];			// <cc> alone
	double cse_s2n[LMAX];			// combination of <ee> and <ss>
	double csec_s2n[LMAX];			// combination of <ee>, <ss> and <cc>

	double epp[CSTEP],ecc[CSTEP];			// 3d-correlation functions and moments
	double r[CSTEP];

	double zeta2[CSTEP],zeta3[CSTEP],zeta4[CSTEP];
	double zetaprime2[CSTEP],zetaprime3[CSTEP],zetaprime4[CSTEP];

	double theta[TSTEP];
	double ell[LSTEP_INT];

	double epsilon_pp[TSTEP],epsilon_cc[TSTEP];	// angular correlation functions II
	double epsilon_sp[TSTEP],epsilon_ss[TSTEP];	
	
	double epsilon_gi_pp[NBIN][TSTEP], epsilon_gi_cc[NBIN][TSTEP];	// angular correlation functions GI
	double epsilon_gi_sp[NBIN][TSTEP], epsilon_gi_ss[NBIN][TSTEP];	// angular correlation functions GI

	double gamma[TSTEP][NBIN][NBIN];		// lensing correlation function

	double ce[LMAX][NBIN];				// II ellipticity spectra
	double cb[LMAX][NBIN];
	double cs[LMAX][NBIN];
	double cc[LMAX][NBIN];

	double ceS[LMAX][NBIN];				// II ellipticity spectra
	double cbS[LMAX][NBIN];
	double csS[LMAX][NBIN];
	double ccS[LMAX][NBIN];

	double ceE[LMAX][NBIN];				// II ellipticity spectra
	double cbE[LMAX][NBIN];
	double csE[LMAX][NBIN];
	double ccE[LMAX][NBIN];

	double ce_gi[LMAX][NBIN][NBIN];				// GI ellipticity spectra
	double cb_gi[LMAX][NBIN][NBIN];
	double cs_gi[LMAX][NBIN][NBIN];				// GI ellipticity crossspectra
	double cc_gi[LMAX][NBIN][NBIN];

	double ce_giS[LMAX][NBIN][NBIN];				// GI ellipticity spectra
	double cb_giS[LMAX][NBIN][NBIN];
	double cs_giS[LMAX][NBIN][NBIN];				// GI ellipticity crossspectra
	double cc_giS[LMAX][NBIN][NBIN];

	double ce_giE[LMAX][NBIN][NBIN];				// GI ellipticity spectra
	double cb_giE[LMAX][NBIN][NBIN];
	double cs_giE[LMAX][NBIN][NBIN];				// GI ellipticity crossspectra
	double cc_giE[LMAX][NBIN][NBIN];

	double cv[LMAX][NBIN][NBIN];			// fiducial spectrum
	double X[LMAX][NBIN][NBIN];			// X-spectrum for mode_dchisq
	double Chi2t[LMAX];
	double Chi2f[LMAX];
	double Dchisq[LMAX];
	double deltaDchisq[LMAX];

	double cu[LMAX][NBIN][NBIN][NFISHER];		// derivative construction and Fisher matrix
	double cl[LMAX][NBIN][NBIN][NFISHER];
	double dc[LMAX][NBIN][NBIN][NFISHER];
	gsl_matrix *fish;

	double cuu[LMAX][NBIN][NBIN][NFISHER][NFISHER];	// construction of second derivatives, matrices for bias
	double cul[LMAX][NBIN][NBIN][NFISHER][NFISHER];
	double cll[LMAX][NBIN][NBIN][NFISHER][NFISHER];
	double d2c[LMAX][NBIN][NBIN][NFISHER][NFISHER];

	gsl_matrix *gext;			// extended Fisher matrix, cumulative in i
	gsl_vector *avec;			// a-vector
	gsl_vector *bias;			// parameter estimation bias

	double ccoefficient[LMAX][NBIN][NBIN];	// correlation coefficient between weak lensing bins

	double c_noise_p_ia[LMAX][NBIN][NBIN];	// cached noisy spectra + IA
	double spectral_moment[SPECTRAL_MOMENT_MAX+1][2][NBIN][NBIN];	// Spectral Moments \sigma[j][a] where a=0 no alignment, a=1 alignment
	double C_sep_rot[LMAX][2*NBIN][2*NBIN]; // Rotated C_separation
};

// --- global variables ---
struct COSMOLOGY *gcosmo;
struct DATA *gdata;
struct TUGI_CONFIG *tconf;
