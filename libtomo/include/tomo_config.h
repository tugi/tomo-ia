// tomo_config.h
// Fiducial Cosmology:
// #define conf_Omega_m 0.32342//0.245	// Omega Matter (Flat Comsology implied. Omega_Lamda = 1-Omega_M)
// #define conf_Omega_B 0.0449//0.0449		// baryonic density, for shape parameter
// #define conf_sigma8 0.834//0.809		// fluctuation amplitude
// #define conf_ns 0.962//1.0			// spectral index
// #define conf_h 0.674//0.71			// hubble parameter, for shape parameter
// #define conf_w -1.0//-1.0		// de eos parameter
// #define conf_wprime 0.0//0.0		// de eos running

#define conf_Omega_m 0.32//0.245	// Omega Matter (Flat Comsology implied. Omega_Lamda = 1-Omega_M)
#define conf_Omega_B 0.045//0.0449		// baryonic density, for shape parameter
#define conf_sigma8 0.83//0.809		// fluctuation amplitude
#define conf_ns 0.96//1.0			// spectral index
#define conf_h 0.68//0.71			// hubble parameter, for shape parameter
#define conf_w -1.0//-1.0		// de eos parameter
#define conf_wprime 0.0//0.0		// de eos running


// --- definitions ---
#define pi M_PI
#define pi2 (pi * pi)
#define twopi (2.0 * M_PI)
#define eps 1.0e-6
#define smalleps 1.0e-7
#define epsrel eps
#define epsabs 1.0e-16
#define NEVAL 9000

#define spirou_clight GSL_CONST_MKSA_SPEED_OF_LIGHT
#define spirou_parsec GSL_CONST_MKSA_PARSEC
#define spirou_kparsec (1.0e3 * spirou_parsec)
#define spirou_mparsec (1.0e6 * spirou_parsec)
#define spirou_gparsec (1.0e9 * spirou_parsec)
#define spirou_hubble (1.0e5 / spirou_mparsec)
#define spirou_dhubble (spirou_clight / spirou_hubble / spirou_mparsec)
#define spirou_thubble (1.0 / spirou_hubble / 3600.0 / 24.0 / 365.25 / 1e9)

#define tugi_G GSL_CONST_MKSA_GRAVITATIONAL_CONSTANT

#define spirou_rad2deg (360.0 / 2.0 / pi)
#define spirou_deg2rad (1.0 / spirou_rad2deg)
#define spirou_rad2min (spirou_rad2deg * 60.0)
#define spirou_min2rad (1.0 / spirou_rad2min)
#define spirou_rad2sec (spirou_rad2deg * 3600.0)
#define spirou_sec2rad (1.0 / spirou_rad2sec)
#define spirou_rhocrit (1.4e11)

#define NBIN externalNBIN		// 1: no tomography, n: n-bin tomography
#define NBIN2 (2 * NBIN)	// twice the bin number, for large covariance matrices in cross-measurements

#define SPECTRAL_MOMENT_MAX 1

#define ASTEP 250		// increase to 200
#define AMIN 0.01
#define AMAX 1.0

#define CHIMIN 3e1
#define CHIMAX 5e3
#define CHISTEP 300

#define LMAX 3000		// ultimately: l=1e4 for integrations, and l=3e3 for intrinsic alignments
#define LMAX_INT 10000		// ultimately: l=1e4 for integrations, and l=3e3 for intrinsic alignments
#define LMIN 10
#define LSTEP 2000
#define LSTEP_INT 5000

#define KMIN_INT 0
#define KMAX_INT 3

#define DCHI_spirals 10.0		// overlap of the two dchi-integrations for the Limber-equations
#define DCHI_ellipticals 30.0		// overlap of the two dchi-integrations for the Limber-equations

#define CMIN 0.01
#define CMAX 2.0e3
#define CSTEP 1000

#define TSTEP 500
#define TMIN (0.01 * spirou_min2rad) // for splines 
#define TMAX (4200 * spirou_min2rad)
#define TMIN_INT (0.02 * spirou_min2rad) // fourier integration limits
#define TMAX_INT (4000 * spirou_min2rad) // obviously keep Dsplines > Dfourier limits


//#define TMIN (0.01 * spirou_min2rad)
//#define TMAX (1.0e3 * spirou_min2rad) Alte Defs
//#define TSTEP 100

#define index_pp 0		// flag for <e+e+> and <exex>
#define index_cc 1
#define index_sp 2
#define index_ss 3

#define flag_ii_emode 300		// flags for integration
#define flag_ii_bmode 301
#define flag_gi_emode 302
#define flag_gi_bmode 303
#define flag_ii_smode 304
#define flag_ii_cmode 305
#define flag_gi_smode 306
#define flag_gi_cmode 307

#define flag_ii_pp 401
#define flag_ii_cc 402
#define flag_ii_sp 403
#define flag_ii_ss 404
#define flag_gi_pp 405
#define flag_gi_cc 406
#define flag_gi_sp 407
#define flag_gi_ss 408

#define flag_zeta 500

#define on 0
#define off 1
#define true 0
#define false 1

#define alphaB 0
#define alphaM 1
#define N_k 3000
#define N_kdp 1000
#define N_a 7

#define minus 0
#define neutral 1
#define plus 2
#define BardeenPhi 0
#define BardeenPsi 1
#define BardeenWeyl 2
#define BardeenTugi 3

// Morphologies
#define ellipse 10
#define spiral 20
#define both 30

// Surveys
#define euclid 100
#define cfhtlens 101
#define sunglass 102
#define robert_test 103

#define mode_lensing 0

#define mode_ebmode 1

#define mode_ellipticity 2
#define mode_tomography 3
#define mode_fisher 4
#define mode_bias 5

#define mode_gravslip 6
#define mode_hooke 7

#define mode_derivs 8
#define mode_moments 9

#define mode_sep_s2n 10
#define mode_sep_biases 11
#define mode_sep_dchisq 12
#define mode_sep_aDfisher 13

#define mode_victor 14

#define type_local 0
#define type_equil 1
#define type_ortho 2
#define type_sform 3

#define inside 0
#define outside 1

#define spirou_shape 0.3	// shape noise
// Spiral model parameters
#define spirou_alpha 0.25//0.5	// disk thickness
#define spirou_align 0.25	// alignment parameter
// Elliptical model parameters
#define cdm_halo_mass 1000.0 // in 1e10 M_Sol
#define hooke externalHooke //9.5e5
// Spiral Fraction
#define tugi_q 0.70   // spiral fraction ~70%

//Nullstellen bei q=0.7: ++: 1.9756880756880757, --: 0.40499179222694703
#define tugi_alpha externalAlpha //1.9756880756880757//0.40499179222694703//1.97563102367963//2.3561944902
//lensing max bei ++ bei 0.40489186189186194

#define tugi_a (tugi_q*tugi_q) // spirals squared
#define tugi_b (tugi_q*(1.0-tugi_q)) // spirals ellipticals mixed
#define tugi_c ((1.0-tugi_q)*(1.0-tugi_q)) // ellipticals squared

#define sep_pbb 1.0
#define sep_prb (1.0-sep_pbb)

#define sep_prr 1.0
#define sep_pbr (1.0-sep_prr)

#define ATRANSITION 0.1

#define NFISHER_START 0
#define NFISHER_END 1

// Put NFISHER to 8: 5 LCDM + 1 Alignment + 2 GravSlips
#define NFISHER 2//(NFISHER_START-NFISHER_END+1)

/*
#define tugi_fisher_omegaM 0
#define tugi_fisher_sigma8 1
#define tugi_fisher_eos 2
#define tugi_fisher_eta 3
#define tugi_fisher_mu 4

#define tugi_fisher_align 999
#define tugi_fisher_Hubble 888
#define tugi_fisher_ns 777
*/

// THE ORIGINAL CONFIGURATION OF FISHERS, DON'T DELETE!

#define tugi_fisher_alphaM 0
#define tugi_fisher_alphaB 1

#define tugi_fisher_omegaM 999
#define tugi_fisher_sigma8 888
#define tugi_fisher_Hubble 2
#define tugi_fisher_ns 3
#define tugi_fisher_eos 4

#define tugi_fisher_align 5
#define tugi_fisher_eta 6
#define tugi_fisher_mu 7
#define tugi_fisher_IAa 8
#define tugi_fisher_IAD 9
 
#define tugi_alphaB 0.1         // absolute values of the alphas in hiclass
#define tugi_alphaM 0.2
#define tugi_delta_alphaB 0.05  // by this relative amount they are changed between neutral, plus and minus
#define tugi_delta_alphaM 0.05

#define eps_derivative 0.025

#define config_filename "config.cfg"
#define MAXBUF 128
#define TUGI_DELIM ":"

