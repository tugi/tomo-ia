// --- prototypes ---
int COMPUTE_LENSING_SPECTRA(struct DATA *data);
int COMPUTE_LENSING_CORRELATION(struct DATA *data);
int COMPUTE_LENSING_CCOEFFICIENTS(struct DATA *data);
double COMPUTE_C_separation_rot(struct DATA *data, double alpha);
// --- splitting up galaxy sample ---
int SPLIT_UP_GALAXY_SAMPLE(struct DATA *data);
double p_galaxy_inverse(double frac);
double aux_p_galaxy_inverse(double z,void *params);
double p_galaxy_cumulative(double z);


// --- s2n computation ---
int COMPUTE_S2N_SPECTRUM(struct DATA *data);
int COMPUTE_S2N_ELLIPTICITY(struct DATA *data);
int COMPUTE_S2N_CROSS(struct DATA *data);
int COMPUTE_S2N_SPECTRUM_SEPARATION(struct DATA *data);

// --- spectral moments ---
int COMPUTE_CNOISE_IA(struct DATA *data);
int PREPARE_SPECTRAL_SPLINES(struct DATA *data);
int COMPUTE_SPECTRAL_MOMENTS(struct DATA *data);
double spectral_moment_j(int j, int i, int a, int b);
double aux_spectral_moment_j(double l, void *param);

// lensing correlation function
double gamma_lensing(double theta,int a,int b);
double aux_dgamma(double l,void *param);
double spirou_cut(double chi, double mu, double DCHI);

// Stepping
double chistep(int i);
double lstep(int i);
double tstep(int i);


// --- spectra ---
double tomo_spectrum(struct SCONFIG *sconfig);
double aux_tomo_spectrum(double chi,void *param);
double tomo_noise(double l,int a,int b);
double tomo_kappa(double l,int a,int b);
double tomo_kappa_wIAs(double l, int a, int b, struct DATA *data);

double C_separation_adapter(struct DATA *data, int l, int a, int b, double alpha);
double S_separation_adapter(struct DATA *data, int l, int a, int b, double alpha);
double CnoIA_separation_adapter(struct DATA *data, int l, int a, int b, double alpha);
double SnoIA_separation_adapter(struct DATA *data, int l, int a, int b, double alpha);
double N_separation_adapter(int l, int a, int b, double alpha);

double POPULATE_SEPARATION_NOISE(struct DATA *data);
double POPULATE_SEPARATION_SIGNAL(struct DATA *data);
double Sbb_separation(struct DATA *data, int l, int a, int b);
double Sbr_separation(struct DATA *data, int l, int a, int b);
double Srr_separation(struct DATA *data, int l, int a, int b);

double SnoIAbb_separation(struct DATA *data, int l, int a, int b);
double SnoIAbr_separation(struct DATA *data, int l, int a, int b);
double SnoIArr_separation(struct DATA *data, int l, int a, int b);

double Nbb_separation(int l, int a, int b);
double Nrr_separation(int l, int a, int b);

// --- weak lensing ---
double tomo_efficiency(double chi,double a,double dp,int i);
double tomo_weighting(double chi,int i);
double aux_weighting(double chiprime,void *params);
double kronecker(int a,int b);

// --- GI Bastards ---
double tomo_efficiency_gi(int i);
double aux_tomo_efficiency_gi(double chi, void *param);

// --- primary functions ---
double map_multipole(double xx,double lmax);
double map_polar_angle(double xx);