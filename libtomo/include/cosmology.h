// --- main functions ---
int INIT_COSMOLOGY(struct DATA *data);
int PREPARE_NEW_COSMOLOGY(struct DATA *data);
int RECALC_COSMOLOGY(struct DATA *data);
int COMPUTE_EVOLUTION(struct DATA *data);
int COMPUTE_COMOVING_DISTANCE(struct DATA *data);
int ALLOCATE_INTERPOLATION_MEMORY(struct DATA *data);
int FREE_INTERPOLATION_MEMORY(struct DATA *data);
int SET_FIDUCIAL_COSMOLOGY(struct DATA *data);

// --- stepping ---
double astep(int i);

// --- cosmometry ---
double a2z(double a);
double z2a(double z);
double a2com(double a);
double aux_dcom(double a,void *params);

double hubble(double a);
double aux_dhubble(double a,void *params);

double deceleration(double a);
double dhubble(double a);
double omega(double a);


// --- growth ---
int d_plus_function(double t, const double y[], double f[],void *params);
double aux_d_plus(double a,double *result_d_plus,double *result_d_plus_prime);
double d_plus(double a);

// --- dark energy model ---
double w_de(double a);
double p_galaxy(double z);

// --- cdm-spectrum ---
double cdm_spectrum(double k);
double cdm_transfer(double k);
double sigma8();
double aux_dsigma8(double k,void *params);
double cdm_potential(double k);
double cdm_smooth(double k);
double baryon_transfer(double k);


// nonlinear CDM-spectrum
int COMPUTE_CDM_PARAMETERS(struct DATA *data);
double cdm_spectrum_slope(double k);
double cdm_nonlinear(double k,double a,double dp);
double cdm_nonlinear_transfer(double k,double a,double dp);
double rscale(double a);
double aux_rscale(double rscale,void *params);
double sigma2(double rscale);
double aux_sigma2(double k,void *params);
double dsigma2(double rscale);
double aux_dsigma2(double k,void *params);
double ddsigma2(double rscale);
double aux_ddsigma2(double k,void *params);

