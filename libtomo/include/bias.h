// auxiliary functions
double spirou_trace(gsl_matrix *matrix);
int comp(const void * a, const void * b);

// lensing fisher matrix
int COMPUTE_FIDUCIAL_SPECTRUM(struct DATA *data);
int COMPUTE_X_SPECTRUM(struct DATA *data);
int COMPUTE_SPECTRUM_P(struct DATA *data);
int COMPUTE_SPECTRUM_M(struct DATA *data);
int COMPUTE_SPECTRUM_DERIVATIVE(struct DATA *data);
int CONSTRUCT_FISHER_MATRIX(struct DATA *data);

// lensing bias formalism
int COMPUTE_SPECTRUM_PP(struct DATA *data);
int COMPUTE_SPECTRUM_PM(struct DATA *data);
int COMPUTE_SPECTRUM_MM(struct DATA *data);
int COMPUTE_HESSIAN_DERIVATIVE(struct DATA *data);
int CONSTRUCT_GMATRIX(struct DATA *data);
int CONSTRUCT_AVECTOR(struct DATA *data);
int COMPUTE_ESTIMATION_BIAS(struct DATA *data);

//Abkuerzungen
int FisherParam_switch_minus(int iFisher);
int FisherParam_switch_minus_revert(int iFisher);
int FisherParam_switch_plus(int iFisher);
int FisherParam_switch_plus_revert(int iFisher);
double FisherParam_switch_delta(int iFisher);
