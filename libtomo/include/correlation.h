// correlation.h

// compute ellipticity spectra
int COMPUTE_ZETAS_AND_ELL_SPECTRA(struct DATA *data);
int COMPUTE_BOTH_ELLIPTICITY_SPECTRA(struct DATA *data);
int COMPUTE_ELLIPTICITY_SPECTRA(struct DATA *data);
int COMPUTE_ELLIPTICITY_CMODE(struct DATA *data);
int COMPUTE_ANGULAR_CORRELATIONS(struct DATA *data,int i);

// 2d ellipticity correlations
// -- II --
double correlation_ii_pp(double theta,int i);
double correlation_ii_cc(double theta,int i);
double correlation_ii_sp(double theta,int i);
double correlation_ii_ss(double theta,int i);
double aux_correlation_ii_dchiprime(double chiprime,void *param);
double aux_correlation_ii_dchi(double chi,void *param);

// -- GI --
double correlation_gi_pp(double theta,int i,int j);
double correlation_gi_cc(double theta,int i,int j);
double correlation_gi_ss(double theta,int i,int j);
double correlation_gi_sp(double theta,int i,int j);
double aux_correlation_gi_dchiprime(double chiprime,void *param);
double aux_correlation_gi_dchi(double chi,void *param);

// --- Integration Helpers --
double tugi_integrate_cquad_k(int flag_function, double r, int n);
double aux_zeta_ext(double k, void *param);


// --- Spectra ---
double tugi_integrate_cquad_lij(int flag_function, int l, int i, int j);
double tugi_integrate_cquad_tij(int flag_function, double theta, int i, int j);

// - II Alignments -
double ii_emode(int l,int i);
double ii_bmode(int l,int i);
double ii_smode(int l,int i);
double ii_cmode(int l,int i);
double aux_ii_dsmode(double theta,void *param);
double aux_ii_dcmode(double theta,void *param);
double aux_ii_demode(double theta,void *param);
double aux_ii_dbmode(double theta,void *param);

// - GI Alignments -
double gi_emode(int l,int i,int j);
double gi_bmode(int l,int i,int j);
double aux_gi_demode(double theta,void *param);
double aux_gi_dbmode(double theta,void *param);
double gi_smode(int l,int i,int j);
double gi_cmode(int l,int i,int j);
double aux_gi_dsmode(double theta,void *param);
double aux_gi_dcmode(double theta,void *param);