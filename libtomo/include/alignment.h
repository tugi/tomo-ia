// alignment.h

// zeta-functions
double spirou_zeta(double r,int n);
double aux_spirou_zeta(double k, void *param);
double tugi_zetaprime(double r,int n);
double aux_tugi_zetaprime(double k, void *param);
double spirou_zeta_norm();
double aux_spirou_zeta_norm(double k, void *param);

// 3d ellipticity
double ellipticity_pp(double r, double alpha);
double ellipticity_cc(double r, double alpha);
double ellipticity_sp(double r, double alpha);
double ellipticity_ss(double r, double alpha);

int COMPUTE_ZETA_COEFFICIENTS(struct DATA *data);
int COMPUTE_ZETAPRIME_COEFFICIENTS(struct DATA *data);
