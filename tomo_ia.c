// weak lensing spectrum tomography
// intrinsic ellipticity spectra for spiral and elliptical galaxies
// cross-correlations between intrinsic alignments and weak lensing
//
// by Bjoern Malte Schaefer, GSFP/Heidelberg, bjoern.malte.schaefer@uni-heidelberg.de

// gcc-mp-4.8 -o tomo tomo.c -L. -L/sw/lib -I/sw/include -lgsl -lgslcblas -lcuba -lm -fopenmp
// gcc -o tomo_ia tomo_ia.c -L./libtomo/lib -I./libtomo/inlcude -I./libcuba/include -L./libcuba/lib -lcuba -lm -fopenmp

// OpenMP parallelisation of intensive l- and bin-looping

// include libtomo
#include <tomo.h>

// --- main ---
int main(int argc, char *argv[])
{
 
	struct TUGI_CONFIG *tugi_config;
	tugi_config = (struct TUGI_CONFIG *)calloc(1,sizeof(struct TUGI_CONFIG));
	tconf = tugi_config;

	// Hard coded stuff (for now)
	tconf->baryon_transfer = off;
	tconf->flag_crossspectra = off; // Speed up for paper
	tconf->flag_extPk = off;  // Pk internal for all but Victors stuff
	struct DATA *data;
	data = (struct DATA *)calloc(1,sizeof(struct DATA));
	gdata = data;
	data->cosmology = (struct COSMOLOGY *)calloc(1,sizeof(struct COSMOLOGY));
	gcosmo = data->cosmology;

	CONFIG_TOMO(argv);
	PRINT_TOMO_HEADER();

	INIT_COSMOLOGY(data);
	PREPARE_NEW_COSMOLOGY(data);
	
	PREPARE_OUTPUT(data);
	WRITE_CONFIG2DISK(data);
	
	SPLIT_UP_GALAXY_SAMPLE(data);

	switch(tconf->mode)
	{
		case mode_lensing:				// lensing s2n-ratio
			COMPUTE_S2N_SPECTRUM(data);
			SAVE_SPECTRUM_SIGNIFICANCE2DISK(data);
			break;

		case mode_ebmode:				// ellipticity s2n-ratios
			COMPUTE_ZETAS_AND_ELL_SPECTRA(data);
			SAVE_ELLIPTICITY_SPECTRA2DISK(data);

			if(tconf->flag_crossspectra==on)
			{
				COMPUTE_ELLIPTICITY_CMODE(data);
				SAVE_ELLIPTICITY_CMODE2DISK(data);
				COMPUTE_S2N_CROSS(data);
			}
			COMPUTE_S2N_ELLIPTICITY(data);		
			SAVE_SIGNIFICANCE2DISK(data);
			break;
			
		case mode_ellipticity:				// ellipticity spectra
			COMPUTE_ZETAS_AND_ELL_SPECTRA(data);
			SAVE_ELLIPTICITY_SPECTRA2DISK(data);

			if(tconf->flag_crossspectra==on)
			{
				COMPUTE_ELLIPTICITY_CMODE(data);
				SAVE_ELLIPTICITY_CMODE2DISK(data);
			}
			break;

		case mode_tomography:
			COMPUTE_LENSING_SPECTRA(data);
			SAVE_LENSING_SPECTRA2DISK(data);
			COMPUTE_LENSING_CORRELATION(data);
			SAVE_LENSING_CORRELATION2DISK(data);
			COMPUTE_LENSING_CCOEFFICIENTS(data);
			SAVE_LENSING_CCOEFFICIENTS2DISK(data);
			break;

		case mode_fisher:				// Fisher matrix
			COMPUTE_FIDUCIAL_SPECTRUM(data);
			COMPUTE_SPECTRUM_P(data);
			COMPUTE_SPECTRUM_M(data);
			COMPUTE_SPECTRUM_DERIVATIVE(data);// <-- here!
			CONSTRUCT_FISHER_MATRIX(data);
			SAVE_FISHER_MATRIX2DISK(data);
			break;

		case mode_bias:					// bias computation
			COMPUTE_ZETAS_AND_ELL_SPECTRA(data);
			COMPUTE_FIDUCIAL_SPECTRUM(data);

			COMPUTE_SPECTRUM_P(data);
			COMPUTE_SPECTRUM_M(data);
			COMPUTE_SPECTRUM_DERIVATIVE(data);

			COMPUTE_SPECTRUM_PP(data);
			COMPUTE_SPECTRUM_PM(data);
			COMPUTE_SPECTRUM_MM(data);
			COMPUTE_HESSIAN_DERIVATIVE(data);

			CONSTRUCT_GMATRIX(data);
			CONSTRUCT_AVECTOR(data);

			COMPUTE_ESTIMATION_BIAS(data);
			SAVE_BIAS2DISK(data);
			break;

		case mode_gravslip:					// Fisher Matrix with slip
			COMPUTE_ZETAS_AND_ELL_SPECTRA(data);
			COMPUTE_FIDUCIAL_SPECTRUM(data);
			
			COMPUTE_SPECTRUM_P(data);
			COMPUTE_SPECTRUM_M(data);
			COMPUTE_SPECTRUM_DERIVATIVE(data);

			CONSTRUCT_FISHER_MATRIX(data);
			SAVE_FISHER_MATRIX2DISK(data);			
			break;

		case mode_hooke:				// ellipticity s2n-ratios
			COMPUTE_ZETAS_AND_ELL_SPECTRA(data);
			SAVE_ELLIPTICITY_SPECTRA2DISK(data);			
			COMPUTE_S2N_ELLIPTICITY(data);
			SAVE_SIGNIFICANCE2DISK(data);

		case mode_derivs:				// d log C / d theta_i
			COMPUTE_ZETAS_AND_ELL_SPECTRA(data);
			COMPUTE_FIDUCIAL_SPECTRUM(data);
			COMPUTE_SPECTRUM_P(data);
			COMPUTE_SPECTRUM_M(data);
			COMPUTE_SPECTRUM_DERIVATIVE(data);
			break;

		case mode_moments:				// spectral moments sigma_0 and sigma_1 with and without IAs
			COMPUTE_BOTH_ELLIPTICITY_SPECTRA(data);
			COMPUTE_LENSING_SPECTRA(data);
			PREPARE_SPECTRAL_SPLINES(data);
			COMPUTE_SPECTRAL_MOMENTS(data);
			SAVE_SPECTRAL_MOMENTS2DISK(data);
			break;

		case mode_sep_s2n:			// ellipticity s2n-ratios for separation mode
			COMPUTE_BOTH_ELLIPTICITY_SPECTRA(data);
			SAVE_ELLIPTICITY_SPECTRA2DISK(data);
			COMPUTE_S2N_SPECTRUM_SEPARATION(data);
			SAVE_SIGNIFICANCE2DISK_SEPARATION(data);
			break;

		case mode_sep_biases:			// ellipticity s2n-ratios for separation mode
			COMPUTE_BOTH_ELLIPTICITY_SPECTRA(data);		
			POPULATE_SEPARATION_SIGNAL(data);
			POPULATE_SEPARATION_NOISE(data);

			COMPUTE_FIDUCIAL_SPECTRUM(data);

			COMPUTE_SPECTRUM_P(data);
			COMPUTE_SPECTRUM_M(data);
			COMPUTE_SPECTRUM_DERIVATIVE(data);

			CONSTRUCT_FISHER_MATRIX(data);
			SAVE_FISHER_MATRIX2DISK(data);
			
			COMPUTE_SPECTRUM_PP(data);
			COMPUTE_SPECTRUM_PM(data);
			COMPUTE_SPECTRUM_MM(data);
			COMPUTE_HESSIAN_DERIVATIVE(data);

			CONSTRUCT_GMATRIX(data);
			CONSTRUCT_AVECTOR(data);

			COMPUTE_ESTIMATION_BIAS(data);
			SAVE_BIAS2DISK(data);
			break;

		case mode_sep_dchisq: // delta chi squared between C++ (S+N p's = 1) and X++ (S+N p's 0.1/0.9).
			COMPUTE_BOTH_ELLIPTICITY_SPECTRA(data);
			POPULATE_SEPARATION_NOISE(data);
			COMPUTE_FIDUCIAL_SPECTRUM(data);
			COMPUTE_X_SPECTRUM(data);
			SAVE_ELLIPTICITY_C_SEPERATION(data);

			CALCULATE_DCHISQ_SEP(data);
			SAVE_DCHI22DISK(data);
			break;
			
		case mode_sep_aDfisher:					
			COMPUTE_BOTH_ELLIPTICITY_SPECTRA(data);			
			POPULATE_SEPARATION_NOISE(data);
			COMPUTE_FIDUCIAL_SPECTRUM(data);
			
			COMPUTE_SPECTRUM_P(data);
			COMPUTE_SPECTRUM_M(data);
			COMPUTE_SPECTRUM_DERIVATIVE(data);

			CONSTRUCT_FISHER_MATRIX(data);
			SAVE_FISHER_MATRIX2DISK(data);			
			break;

		case mode_victor:
			tconf->whichBardeen = BardeenWeyl;
			tconf->flag_extPk = on;
			tconf->whichalpha = alphaB;
			tconf->alphapm = neutral;

			READIN_EXTPK(data);				
			READIN_EXTDPLUS(data);				
			COMPUTE_ZETAS_AND_ELL_SPECTRA(data);
			COMPUTE_FIDUCIAL_SPECTRUM(data);

			COMPUTE_SPECTRUM_P(data);
			COMPUTE_SPECTRUM_M(data);
			COMPUTE_SPECTRUM_DERIVATIVE(data);

			CONSTRUCT_FISHER_MATRIX(data);
			SAVE_FISHER_MATRIX2DISK(data);	
			break;
	}

	free(data);
	free(tugi_config);

	printf("============================================================\n");
	exit(0);
}
